﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using ProdStart.SOAP;
using ProdStart.L2LIF0025_QAS;
using System.Configuration;
using System.Diagnostics;
using MatInput;
using Core;

namespace ProdStart
{
    public partial class frmProdStart : Form
    {
        CommonCode commonFunc = new CommonCode();
        QueryResult qr = new QueryResult();
        int SelCnt = 0;
        private string ScanBarCode = string.Empty; // 스캔 바코드
        private StringBuilder TempScanBarcode = new StringBuilder();    // 임시 키보드값, 바코드위해사용 (Enter) 시 초기화
        private string sCompId = ConfigurationSettings.AppSettings["CompId"];

        public frmProdStart()
        {
            InitializeComponent();
        }
        
        public void frmProdStart_Load(object sender, EventArgs e)
        {
#if DEBUG
            if (!CMyFunc.SoapConnection("DEV"))
            {
                Application.Exit();
                return;
            }
#else
            if (!CMyFunc.SoapConnection("RUN"))
            {
                Application.Exit();
                return;
            }

#endif

            // 작업자 조회해서 콤보박스에 추가
            BindCboWorker();

            //작업순번, 코드 설정
            commonFunc.WrkStep = ConfigurationSettings.AppSettings["WrkStep"];
            commonFunc.WrkName = ConfigurationSettings.AppSettings["WrkName"];
            //commonFunc.PartTypeName = ConfigurationSettings.AppSettings["WrkName"];            
        }
        
        private void BindCboWorker()
        {
            DataTable dtWorker = new DataTable();
            SelCnt = qr.GetData_Worker(ref dtWorker, sCompId);
            if (SelCnt < 1)
            {
                return;
            }

            cboWorker.DataSource = dtWorker;
            cboWorker.ValueMember = "WORKER_INFO_SEQ";
            cboWorker.DisplayMember = "WORKER";


            DataRow dr = dtWorker.NewRow();
            dr["WORKER_INFO_SEQ"] = 0;
            dr["WORKER"] = "선택";

            dtWorker.Rows.InsertAt(dr, 0);
            cboWorker.SelectedIndex = 0;
        }

        private void SetControl()
        {
            // 스캔한 지시번호로 제조지시 정보 조회
            DataTable dtOrderInfo = new DataTable("Default");
            SelCnt = qr.GetData_IF_Manufactuing(ref dtOrderInfo, txtProdOrderNo.Text);
            if (SelCnt < 1)
            {
                MessageBox.Show("스캔한 지시번호로 제조지시 정보 조회 실패");
                txtProdOrderNo.Clear();
                return;
            }
            else if(!string.IsNullOrEmpty(Convert.ToString(dtOrderInfo.Rows[0]["StartTime"])))
            {
                MessageBox.Show("이미 시작한 지시입니다. ");
                txtProdOrderNo.Clear();
                return;
            }

            // SOAP방식으로 WSDL파일 읽어옴
            string CompId = sCompId;
            string ItemCode = Convert.ToString(dtOrderInfo.Rows[0]["ManufactureiTem"]);
            string Version = string.Empty;


            //테스트
            //if("REAL".Equals(commonFunc.Status))
            //{
                //SendSoapL2LIF0025_QAS(CompId, ItemCode, ref Version);
            //}
            try
            {
                SendSoapL2LIF0025_QAS(CompId, ItemCode, ref Version);
            }catch(Exception e)
            {
            }

            // 스캔한 지시번호로 조회한 제조지시 정보 표시
            lblProdNo.Text          = Convert.ToString(dtOrderInfo.Rows[0]["ManufactureNo"]);
            lblItemCode.Text        = ItemCode;
            lblProdOrderQty.Text    = double.Parse(Convert.ToString(dtOrderInfo.Rows[0]["ManufactureQTY"])).ToString();
            lblVersion.Text         = "Ver " + Version;
            lblTankID.Text          = ""; //Convert.ToString(dtOrderInfo.Rows[0]["MixerNo"]);
            lblItemName.Text        = Convert.ToString(dtOrderInfo.Rows[0]["ManufactureName"]);
            lblProdStartDate.Text   = Convert.ToString(dtOrderInfo.Rows[0]["StartTime"]);


            // 처방에서 인터페이스로 받은 '주의사항'항목 표시
            DataTable dtRTGETC = new DataTable("Default");
            SelCnt = qr.GetData_RTGETC(ref dtRTGETC, CompId, ItemCode, Version);
            if (SelCnt < 1)
            {
                MessageBox.Show("주의사항 조회 실패");
                return;
            }

            // 주의사항 항목 CheckedListBox Item 추가
            clbPrecautions.Items.Clear();
            string[] Precautions = dtRTGETC.Rows[0]["RTGETC"].ToString().Split('\n');
            foreach(string precautions in Precautions)
            {
                string[] precaution = precautions.Split(new string[] { "|^|" }, StringSplitOptions.None);
                clbPrecautions.Items.Add(precaution[0]);
            }
        }

        private void clbPrecautions_SelectedIndexChanged(object sender, EventArgs e)
        {
            //주의사항 항목이 모두 Checked 되야 ‘생산시작‘ 버튼이 활성화
            if (clbPrecautions.CheckedItems.Count == clbPrecautions.Items.Count)
            {
                btnProdStart.Enabled = true;
            }
            else
            {
                btnProdStart.Enabled = false;
            }
        }

        private void btnProdStart_Click(object sender, EventArgs e)
        {
            // 작업자 선택안했을 경우 메세지로 알려줌
            if (cboWorker.SelectedIndex == 0)
            {
                MessageBox.Show("작업자를 선택해주세요. ");
                return;
            }

            string startTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            // 생산시작 일시 DB 적재
            qr.UpdateData_IF_Manufacturing(startTime, txtProdOrderNo.Text);
            qr.InsertData_T_FACT_UPTIME(sCompId, txtProdOrderNo.Text, lblProdNo.Text, lblItemCode.Text, lblItemName.Text, lblTankID.Text, startTime);

            Program.cmx.SetTagVal("REPORT.시작1", 1); //S->1
            Program.cmx.SetTagVal("REPORT.생산1.지시번호", txtProdOrderNo.Text);
            Program.cmx.SetTagVal("REPORT.VER", lblVersion.Text);
            Program.cmx.SetTagVal("REPORT.생산1.제조번호", lblProdNo.Text);
            Program.cmx.SetTagVal("REPORT.생산1.제조품목", lblItemCode.Text);
            Program.cmx.SetTagVal("REPORT.생산1.제조품명", lblItemName.Text);
            Program.cmx.SetTagVal("REPORT.생산1.제조량", lblProdOrderQty.Text);
            Program.cmx.SetTagVal("REPORT.생산1.시작일시2", lblProdStartDate.Text);
            Program.cmx.SetTagVal("작업자.작업자_N1", cboWorker.Text);

            if (commonFunc.WrkName.StartsWith("PT_"))
            {
                this.Hide();             
                Process.Start(Application.StartupPath + "/MatInput.exe").Refresh();
                this.Close();
            }
            //this.Close();
        }


        private void frmProdStart_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtProdNo_KeyPress(sender, e);
        }
        
        private void txtProdNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            // 키 이벤트 처리
            if (!commonFunc.ScanKeyPress(e.KeyChar, ref TempScanBarcode, ref txtProdOrderNo))
            {
                e.Handled = true;
                return;
            }

            SetControl();
        }

        
        private bool SendSoapL2LIF0025_QAS(string pCompId, string pRoutNo, ref string pVersion)
        {
            CSoapL2LIF0025_QAS _SendData = new CSoapL2LIF0025_QAS();
            L2LIF0025_WEB newData = new L2LIF0025_WEB();

            newData.IF_RD_ROUTING_OPER_NEW_R = new L2LIF0025_WEBIF_RD_ROUTING_OPER_NEW_R();
            newData.IF_RD_ROUTING_OPER_NEW_R.COMPID = pCompId;
            newData.IF_RD_ROUTING_OPER_NEW_R.ROUTNO = pRoutNo;

            _SendData.m_Req.MT_L2LIF0025_WEB = newData;
            _SendData.StartCSoapL2LIF0025_QAS();


            //전송하는 수를 넣어야함..
            int SendCount = 1;

            L2LIF0025_WEB_responseIF_RD_ROUTING_OPER_NEW_R2[] SAPDATA = new L2LIF0025_WEB_responseIF_RD_ROUTING_OPER_NEW_R2[SendCount];

            string errMsg = string.Empty;
            string strReturn = _SendData.Reflash(_SendData.m_Req, ref SAPDATA, ref errMsg);

            pVersion = ((string.IsNullOrEmpty(SAPDATA[0].VERSION)) ? "0" : SAPDATA[0].VERSION);

            // 이미 인터페이스 한게 있는 경우에는 테이블에 INSERT 하지 않음
            qr.GetCount_IF_RD_ROUTING_OPER_NEW_R(pCompId, pRoutNo, pVersion);
            if (Convert.ToInt32(qr.Bdb.dtMain.Rows[0]["CNT"]) > 0)
            {
                return true;
            }

            // 인터페이스 테이블(IF_RD_ROUTING_OPER_NEW_R)에 INSERT
            string[][] pL2LIF0025_WEB_responseIF_RD_ROUTING_OPER_NEW_R2 = new string[SAPDATA.Length][];
            int i = 0;
            foreach (L2LIF0025_WEB_responseIF_RD_ROUTING_OPER_NEW_R2 sapData in SAPDATA)
            {
                string[] arrayL2 = new string[25];

                arrayL2[0] = sapData.SEQKEY;
                arrayL2[1] = sapData.SPCOMP;
                arrayL2[2] = sapData.NODTYP;
                arrayL2[3] = sapData.NODEID;
                arrayL2[4] = sapData.OPRCOD;
                arrayL2[5] = sapData.OPTIND;
                arrayL2[6] = sapData.OPRUOT;  
                arrayL2[7] = ((string.IsNullOrEmpty(sapData.OPTIME)) ? "0" : sapData.OPTIME);
                arrayL2[8] = ((string.IsNullOrEmpty(sapData.OPTEMP)) ? "0" : sapData.OPTEMP);
                arrayL2[9] = ((string.IsNullOrEmpty(sapData.OPRPMH)) ? "0" : sapData.OPRPMH);
                arrayL2[10] = ((string.IsNullOrEmpty(sapData.OPRPMH_R)) ? "0" : sapData.OPRPMH_R);
                arrayL2[11] = ((string.IsNullOrEmpty(sapData.OPRPMP)) ? "0" : sapData.OPRPMP);
                arrayL2[12] = ((string.IsNullOrEmpty(sapData.OPRPMS)) ? "0" : sapData.OPRPMS);
                arrayL2[13] = ((string.IsNullOrEmpty(sapData.OPVACU)) ? "0" : sapData.OPVACU);
                arrayL2[14] = ((string.IsNullOrEmpty(sapData.WTEMP)) ? "0" : sapData.WTEMP);
                arrayL2[15] = ((string.IsNullOrEmpty(sapData.WRPMD)) ? "0" : sapData.WRPMD);
                arrayL2[16] = ((string.IsNullOrEmpty(sapData.OTEMP)) ? "0" : sapData.OTEMP);
                arrayL2[17] = ((string.IsNullOrEmpty(sapData.ORPMP)) ? "0" : sapData.ORPMP);
                arrayL2[18] = sapData.OPSINS;
                arrayL2[19] = sapData.OPSINSDSC;
                arrayL2[20] = sapData.RTGETC;
                arrayL2[21] = sapData.ADDEMP;
                arrayL2[22] = sapData.ADDTIM;
                arrayL2[23] = sapData.LOGEMP;
                arrayL2[24] = sapData.LOGTIM;

                pL2LIF0025_WEB_responseIF_RD_ROUTING_OPER_NEW_R2[i++] = arrayL2;
            }
            
            _SendData.EndSoapL2LIF0025_QAS();
            return qr.InsertData_L2LIF0025_WEB_responseIF_RD_ROUTING_OPER_NEW_R2(pL2LIF0025_WEB_responseIF_RD_ROUTING_OPER_NEW_R2, pCompId, pRoutNo, pVersion);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label10_Click(object sender, EventArgs e)
        {
            qr.TestStart(txtProdOrderNo.Text);
        }
    }
}
