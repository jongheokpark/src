﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;

namespace ProdStart.SOAP
{
    class CSoap
    {
        internal static string CSUserName;
        internal static string CSPassword;
        
        private string AndAdressPortL2LIF0025_QAS = "HTTP_Port";
        internal static string CSEndpintAddress_L2LIF0025_QAS;

        private string ReqStartDay;
        private string ReqEndDay;

        public CSoap()
        {
            DateTime now = DateTime.Now;
            ReqStartDay = ReqEndDay = string.Format("{0}", now.ToString("yyyyMMdd"));
        }

        public string GetCSL2LIF0025_QASEndpintAddress()
        {
            return AndAdressPortL2LIF0025_QAS;
        }
        
        #region _SET
        public void SetCSUserName(string _UserName)
        {
            CSUserName = _UserName;
        }

        public void SetCSPassword(string _CSPassword)
        {
            CSPassword = _CSPassword;
        }

        public void SetSatrtDay(string SatarDay)
        {
            ReqStartDay = SatarDay;
        }
        public void SetEndDay(string EndDay)
        {
            ReqEndDay = EndDay;
        }

        #endregion _SET

        #region _GET
        public string GetCSUserName()
        {
            return CSUserName;
        }

        public string GetCSPassword()
        {
            return CSPassword;
        }
        public BasicHttpBinding GetCredentialsBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = System.ServiceModel.BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport.ClientCredentialType = System.ServiceModel.HttpClientCredentialType.Basic;
            binding.MaxBufferPoolSize = 65536 * 2;
            binding.MaxReceivedMessageSize = 65536 * 2;
            return binding;
        }

        public string GetSatrtDay()
        {

            return ReqStartDay;
        }
        public string GetEndDay()
        {

            return ReqEndDay;
        }
        #endregion _GET
    }
}
