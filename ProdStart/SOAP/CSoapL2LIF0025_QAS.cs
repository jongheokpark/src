﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProdStart.L2LIF0025_QAS;
using System.ServiceModel;
using System.Windows.Forms;

namespace ProdStart.SOAP
{
    class CSoapL2LIF0025_QAS
    {
        private CSoap mCSoap;
        private L2LIF0025_WEB_SOClient m_Client;
        public L2LIF0025_WEB_SORequest m_Req;
        public L2LIF0025_WEB_SOResponse m_Res = new L2LIF0025_WEB_SOResponse();

        public CSoapL2LIF0025_QAS()
        {
            mCSoap = new CSoap();

            m_Client = new L2LIF0025_WEB_SOClient(mCSoap.GetCSL2LIF0025_QASEndpintAddress());
            m_Client.Endpoint.Address = new EndpointAddress(CSoap.CSEndpintAddress_L2LIF0025_QAS);
            m_Client.Endpoint.Binding = mCSoap.GetCredentialsBinding();
            m_Client.ClientCredentials.UserName.UserName = mCSoap.GetCSUserName();
            m_Client.ClientCredentials.UserName.Password = mCSoap.GetCSPassword();
            m_Req = new L2LIF0025_WEB_SORequest();
            m_Req.MT_L2LIF0025_WEB = new L2LIF0025_WEB();
            m_Res = new L2LIF0025_WEB_SOResponse();
        }

        public void SetSatrDay(string _SetDay)
        {
            mCSoap.SetSatrtDay(_SetDay);
        }
        public void SetEndDay(string _setDay)
        {
            mCSoap.SetEndDay(_setDay);
        }

        public void StartCSoapL2LIF0025_QAS()
        {
            try
            {
                m_Client.Open();
            }
            catch (Exception e)
            {
                string error = e.Message;
                if (e.InnerException != null)
                {
                    MessageBox.Show(e.InnerException.Message);
                    error = e.InnerException.Message;
                }
                else
                {
                    MessageBox.Show(e.Message);
                }
            }
        }

        public void EndSoapL2LIF0025_QAS()
        {
            m_Client.Close();
        }

        public string Reflash(L2LIF0025_WEB_SORequest _WMSDATA, ref L2LIF0025_WEB_responseIF_RD_ROUTING_OPER_NEW_R2[] _SAPDATA, ref string ErrMsg)
        {
            string strReturn = "";//OK
            if (m_Client.State != CommunicationState.Opened)
            {
                m_Client.Open();
            }

            try
            {
                _SAPDATA = m_Client.L2LIF0025_WEB_SO(_WMSDATA.MT_L2LIF0025_WEB);
            }
            catch (Exception e)
            {
                string error = e.Message;
                if (e.InnerException != null)
                {
                    MessageBox.Show(e.InnerException.Message);
                    error = e.InnerException.Message;
                }
                else
                {
                    MessageBox.Show(e.Message);
                }
                strReturn = error;
            }
            return strReturn;
        }
    }
}
