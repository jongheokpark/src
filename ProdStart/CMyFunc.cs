﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProdStart.SOAP;
using System.Windows.Forms;
using System.Data;
using System.Data.Common;
using System.Configuration;

namespace ProdStart
{
    class CMyFunc
    {
        /// <summary>
        /// SOAP 연결정보 mapping
        /// </summary>
        /// <param name="SOAPServer"></param>
        internal static bool SoapConnection(string SOAPServer)
        {
            try
            {
                if (SOAPServer == "DEV")
                {
                    CSoap.CSUserName = ConfigurationSettings.AppSettings["CSUserName"];
                    CSoap.CSPassword = ConfigurationSettings.AppSettings["CSPassword"];

                    CSoap.CSEndpintAddress_L2LIF0025_QAS = "http://cpq001.cosmax.co.kr:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=WEB_Q&receiverParty=&receiverService=&interface=L2LIF0025_WEB_SO&interfaceNamespace=http%3A%2F%2Fcosmax.co.kr%2FL2L%2FWEB";
                }
                else
                {
                    CSoap.CSUserName = ConfigurationSettings.AppSettings["CSUserName"];
                    CSoap.CSPassword = ConfigurationSettings.AppSettings["CSPassword"];

                    CSoap.CSEndpintAddress_L2LIF0025_QAS = "http://cpq001.cosmax.co.kr:50000/XISOAPAdapter/MessageServlet?senderParty=&senderService=WEB_Q&receiverParty=&receiverService=&interface=L2LIF0025_WEB_SO&interfaceNamespace=http%3A%2F%2Fcosmax.co.kr%2FL2L%2FWEB";
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

    }
}
