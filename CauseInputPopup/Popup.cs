﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;
using System.Configuration;

namespace CauseInputPopup
{
    public partial class Popup : Form
    {
        QueryResult qr = new QueryResult();
        string CodeSeq = string.Empty; //0 -> 일시정지, 1 -> 정지
        string codeName = string.Empty;

        public Popup()
        {
            InitializeComponent();
        }

        public void Popup_Load(object sender, EventArgs e)
        {
            SetControl();
        }

        #region Method
        private void SetControl()
        {
            lblOrderNo.Text = Program.cmx.GetTagVal("REPORT.생산1.지시번호") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.지시번호").ToString();
            lblOperator.Text = Program.cmx.GetTagVal("작업자.작업자_N1") == null ? "" : Program.cmx.GetTagVal("작업자.작업자_N1").ToString();

            GetCauseData();
        }

        private void GetCauseData()
        {
            DataTable dt = new DataTable();
            CodeSeq = Program.cmx.GetTagVal("MAIN.STOP_COMMENT") == null ? "" : Program.cmx.GetTagVal("MAIN.STOP_COMMENT").ToString();

            //0 -> 일시정지, 1 -> 정지
            if ("0".Equals(CodeSeq))
            {
                codeName = "PAUSE_REASON";
            }else if ("1".Equals(CodeSeq))
            {
                codeName = "STOP_REASON";
            }

            qr.GetCuaseData(ref dt, codeName);

            if (dt != null && dt.Rows.Count > 0)
            {
                cmbCause.Items.Add(string.Empty);
                foreach(DataRow dr in dt.Rows)
                {
                    cmbCause.Items.Add(dr.ItemArray[0]);
                }
            }
        }
        #endregion

        #region Button
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(cmbCause.Text))
            {
                MessageBox.Show(this, "사유 입력은 필수입니다.");
                return;
            }

            string sCompId = ConfigurationSettings.AppSettings["CompId"];
            string wrkCode = Program.cmx.GetTagVal("CMD.STEP") == null ? "1" : Program.cmx.GetTagVal("CMD.STEP").ToString();
            string Version = Program.cmx.GetTagVal("REPORT.VER") == null ? "0" : Program.cmx.GetTagVal("REPORT.VER").ToString();
            string itemName = Program.cmx.GetTagVal("REPORT.생산1.제조품목") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조품목").ToString();
            string mixerId = Program.cmx.GetTagVal("REPORT.MIXER_ID") == null ? "" : Program.cmx.GetTagVal("REPORT.MIXER_ID").ToString();

            //작업 일시 중지
            if ("0".Equals(CodeSeq))
            {
                Program.cmx.SetTagVal("MAIN.PAUSE", "1");
            }
            //자동화 중지
            else if ("1".Equals(CodeSeq))
            {
                Program.cmx.SetTagVal("CMD.OPRCOD", "Z");
            }

            qr.SetCauseData(sCompId, lblOrderNo.Text, mixerId, lblOperator.Text, codeName, cmbCause.Text, itemName, Version.Replace("Ver", "").Trim(), wrkCode);

            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
