﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using FarPoint.Win.Spread;
using Core;
using System.Configuration;
using System.Diagnostics;

namespace MatInput
{
    public partial class frmMatInput : Form
    {
        #region 전역변수
        public string partTypeName = string.Empty;
        public string wrkCode = string.Empty;
        public string wrkName = string.Empty;

        public string OrdNo = string.Empty;
        public string Ver = string.Empty;
        public string ProdNo = string.Empty;
        public string TankId = string.Empty;
        public string ProdCd = string.Empty;
        public string ProdNm = string.Empty;
        public string ProdQty = string.Empty;
        public string ProdStartDate = string.Empty;
        public string UserId = string.Empty;
        private string ScanBarCode = string.Empty; // 스캔 바코드
        private string sFactory = ConfigurationSettings.AppSettings["CompId"];
        private string SeqKey = string.Empty;
        private StringBuilder TempScanBarcode = new StringBuilder();    // 임시 키보드값, 바코드위해사용 (Enter) 시 초기화
        DataTable dt = new DataTable();
        CommonCode commonFunc = new CommonCode();
        QueryResult qr = new QueryResult();
        Service query = new Service();
        
        #endregion

        #region 생성자
        public frmMatInput()
        {
            InitializeComponent();
        }

        enum spColumn
        {
            INPUT_SEQ,
            MAT_CODE,
            MAT_NAME,
            MAT_QTY,
            BARCODE,
            CHECKED,
            WRKSEQ
        }

        #endregion
        #region Event
        private void btnOK_Click(object sender, EventArgs e)
        {
            string TempStep = commonFunc.AddWrkStep(wrkCode).ToString(); //Step +1
            string TempCode = string.Empty;
            string nextStep = string.Empty;
            string chkMHB = string.Empty;

            DataTable dt = new DataTable();
            qr.GetDataForWrkStep(ref dt, TempStep, ProdCd, sFactory);
            if (dt != null && dt.Rows.Count > 0)
            {
                //원료 투입 시 호모역방향 진행 시 중지 신호보내기
                string sStatus = Program.cmx.GetTagVal("CMD.처리") == null ? "" : Program.cmx.GetTagVal("CMD.처리").ToString();
                if (sStatus.Contains("CMD : MHB"))
                {
                    //메인 운전 정지
                    Program.cmx.SetTagVal("MAIN.RECIPE_START", "0");
                    commonFunc.Delay(300);
                }

                //현재 Step Code
                TempCode = dt.Rows[0]["OPRCOD"].ToString();

                //다음 Step Code확인
                if (TempCode.StartsWith("PT_"))
                {
                    while (TempCode.StartsWith("PT_"))
                    {
                        TempStep = commonFunc.AddWrkStep(TempStep).ToString();
                        qr.GetDataForWrkStep(ref dt, TempStep, ProdCd, sFactory);

                        TempCode = dt.Rows[0]["OPRCOD"].ToString();
                    }
                }
                else if (TempCode.StartsWith("CHK"))
                {
                    Program.cmx.SetTagVal("CMD.STEP", TempStep);
                    this.Hide();
                    Process.Start(Application.StartupPath + "/ProcChk.exe");
                    this.Close();
                    return;
                }
                else
                {
                    //추후 로직 필요 시 추가 예정
                }

                //메인 장비 세팅 값 때문에 다음 Step전 변수에 입력
                commonFunc.OpTime = double.Parse(dt.Rows[0]["OPTIME"].ToString()).ToString();
                commonFunc.OpTemp = double.Parse(dt.Rows[0]["OPTEMP"].ToString()).ToString();
                commonFunc.OpRpmH = double.Parse(dt.Rows[0]["OPRPMH"].ToString()).ToString();
                commonFunc.OpRpmH_R = double.Parse(dt.Rows[0]["OPRPMH_R"].ToString()).ToString();
                commonFunc.OpRpmP = double.Parse(dt.Rows[0]["OPRPMP"].ToString()).ToString();
                commonFunc.OpRpmS = double.Parse(dt.Rows[0]["OPRPMS"].ToString()).ToString();
                commonFunc.wTemp = double.Parse(dt.Rows[0]["WTEMP"].ToString()).ToString();
                commonFunc.wRpmD = double.Parse(dt.Rows[0]["WRPMD"].ToString()).ToString();
                commonFunc.oTemp = double.Parse(dt.Rows[0]["OTEMP"].ToString()).ToString();
                commonFunc.oRpmP = double.Parse(dt.Rows[0]["ORPMP"].ToString()).ToString();

                //메인 장비의 경우 특이 케이스라고 생각해서 위의 if문과 별개로 구분 해놓음.
                if (TempCode.StartsWith("M") && !"MHB".Equals(TempCode))
                {
                    nextStep = commonFunc.AddWrkStep(TempStep).ToString();
                    qr.GetDataForWrkStep(ref dt, nextStep, ProdCd, sFactory);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string nextStepCode = dt.Rows[0]["OPRCOD"].ToString();
                        TempCode += "," + nextStepCode;

                        while (nextStepCode.StartsWith("M"))
                        {
                            commonFunc.OpTime += ","+ double.Parse(dt.Rows[0]["OPTIME"].ToString()).ToString();
                            commonFunc.OpTemp += "," + double.Parse(dt.Rows[0]["OPTEMP"].ToString()).ToString();
                            commonFunc.OpRpmH += "," + double.Parse(dt.Rows[0]["OPRPMH"].ToString()).ToString();
                            commonFunc.OpRpmH_R += "," + double.Parse(dt.Rows[0]["OPRPMH_R"].ToString()).ToString();
                            commonFunc.OpRpmP += "," + double.Parse(dt.Rows[0]["OPRPMP"].ToString()).ToString();
                            commonFunc.OpRpmS += "," + double.Parse(dt.Rows[0]["OPRPMS"].ToString()).ToString();
                            commonFunc.wTemp += "," + double.Parse(dt.Rows[0]["WTEMP"].ToString()).ToString();
                            commonFunc.wRpmD += "," + double.Parse(dt.Rows[0]["WRPMD"].ToString()).ToString();
                            commonFunc.oTemp += "," + double.Parse(dt.Rows[0]["OTEMP"].ToString()).ToString();
                            commonFunc.oRpmP += "," + double.Parse(dt.Rows[0]["ORPMP"].ToString()).ToString();

                            nextStep = commonFunc.AddWrkStep(nextStep).ToString();
                            qr.GetDataForWrkStep(ref dt, nextStep, ProdCd, sFactory);

                            if (dt != null && dt.Rows.Count > 0)
                            {
                                if ("MHB".Equals(nextStepCode))
                                {
                                    chkMHB = dt.Rows[0]["OPRCOD"].ToString();
                                    break;
                                }
                                else
                                {
                                    nextStepCode = dt.Rows[0]["OPRCOD"].ToString();
                                    TempCode += "," + nextStepCode;
                                }
                            }
                            else
                            {
                                nextStepCode = string.Empty;
                            }

                        }
                    }
                }

                //CMD 세팅
                Program.cmx.SetTagVal("CMD.OPTIME", commonFunc.OpTime);
                Program.cmx.SetTagVal("CMD.OPTEMP", commonFunc.OpTemp);
                Program.cmx.SetTagVal("CMD.OPRPMH", commonFunc.OpRpmH);
                Program.cmx.SetTagVal("CMD.OPRPMH_R", commonFunc.OpRpmH_R);
                Program.cmx.SetTagVal("CMD.OPRPMP", commonFunc.OpRpmP);
                Program.cmx.SetTagVal("CMD.OPRPMS", commonFunc.OpRpmS);
                Program.cmx.SetTagVal("CMD.OPRPMV", commonFunc.OpVacu);
                Program.cmx.SetTagVal("CMD.WTEMP", commonFunc.wTemp);
                Program.cmx.SetTagVal("CMD.WRPMD", commonFunc.wRpmD);
                Program.cmx.SetTagVal("CMD.OTEMP", commonFunc.oTemp);
                Program.cmx.SetTagVal("CMD.ORPMP", commonFunc.oRpmP);

                //세팅값이 설정되어야 메인머신 운전이 가능하다고 해서 세팅 후에 Step, 현재 코드 세팅
                Program.cmx.SetTagVal("CMD.STEP", TempStep);
                Program.cmx.SetTagVal("CMD.OPRCOD", TempCode);

                //다음 Step OPRCOD
                //작업 후에 팝업 시 Code에 ","가 들어가도록 되어 있으므로 oprcod2에는 데이터 입력하지 않음
                if (!TempCode.Contains(","))
                {
                    TempStep = commonFunc.AddWrkStep(TempStep).ToString();
                    qr.GetDataForWrkStep(ref dt, TempStep, ProdCd, sFactory);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        Program.cmx.SetTagVal("CMD.OPRCOD2", dt.Rows[0]["OPRCOD"].ToString());
                    }
                }
                else if (TempCode.Contains(",MHB"))
                {
                    string gbnCode = string.Empty;
                    int count = TempCode.Count(o => o == ',');
                    for (int i = 0; i < count; i++)
                    {
                        gbnCode += ",";
                    }
                    Program.cmx.SetTagVal("CMD.OPRCOD2", gbnCode + chkMHB);
                }

                if (TempCode.StartsWith("W"))
                {
                    //수상 운전 시작
                    Program.cmx.SetTagVal("WATER.AGITATOR_OFF", "0");
                    Program.cmx.SetTagVal("WATER.AGITATOR_ON", "1");
                }
                else if (TempCode.StartsWith("O"))
                {
                    //유상 운전 시작
                    Program.cmx.SetTagVal("OIL.AGITATOR_OFF", "0");
                    Program.cmx.SetTagVal("OIL.AGITATOR_ON", "1");
                }

                this.Close();
            }
        }
        private void frmMatInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            txtBarCode_KeyPress(sender, e);
        }
        #endregion
        public void frmMatInput_Load(object sender, EventArgs e)
        {
            SetGrid();
            SetControl();

            txtTankId.Focus();
        }

        #region Method
        private void SetGrid()
        {
            //Sheet
            commonFunc.InitSheet(ref spBeforeMat_Sheet1, 0, spBeforeMat_Sheet1.ColumnCount);
            commonFunc.SetColHeader(spBeforeMat_Sheet1, typeof(spColumn));
            //spBeforeMat_Sheet1.Columns[(int)spColumn.BARCODE].Visible = false;
            spBeforeMat_Sheet1.Columns[(int)spColumn.CHECKED].Visible = false;
            spBeforeMat_Sheet1.Columns[(int)spColumn.WRKSEQ].Visible = false;

            commonFunc.InitSheet(ref spAfterMat_Sheet1, 0, spAfterMat_Sheet1.ColumnCount);
            commonFunc.SetColHeader(spAfterMat_Sheet1, typeof(spColumn));
            //spAfterMat_Sheet1.Columns[(int)spColumn.BARCODE].Visible = false;
            //spAfterMat_Sheet1.Columns[(int)spColumn.CHECKED].Visible = false;
            //spAfterMat_Sheet1.Columns[(int)spColumn.WRKSEQ].Visible = true;
        }

        private void SetControl()
        {
            //상단부분 데이터 세팅
            wrkCode = Program.cmx.GetTagVal("CMD.STEP") == null ? "1" : Program.cmx.GetTagVal("CMD.STEP").ToString();
            lblOrderNo.Text = Program.cmx.GetTagVal("REPORT.생산1.지시번호") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.지시번호").ToString();
            lblVersion.Text = Program.cmx.GetTagVal("REPORT.VER") == null ? "" : Program.cmx.GetTagVal("REPORT.VER").ToString();
            lblProdNo.Text = Program.cmx.GetTagVal("REPORT.생산1.제조번호") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조번호").ToString();
            txtTankId.Text = "";//Program.cmx.GetTagVal("REPORT.생산1.지시번호") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.지시번호").ToString();
            lblItemCode.Text = Program.cmx.GetTagVal("REPORT.생산1.제조품목") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조품목").ToString();
            lblItemName.Text = Program.cmx.GetTagVal("REPORT.생산1.제조품명") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조품명").ToString();
            lblProdOrderQty.Text = Program.cmx.GetTagVal("REPORT.생산1.제조량") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조량").ToString();
            lblProdStartDate.Text = Program.cmx.GetTagVal("REPORT.생산1.시작일시2") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.시작일시2").ToString();
            lblWorker.Text = Program.cmx.GetTagVal("작업자.작업자_N1") == null ? "" : Program.cmx.GetTagVal("작업자.작업자_N1").ToString();

            //투입 전 데이터 가져오기
            ProdCd = lblItemCode.Text;
            qr.GetDataForWrkStep(ref dt, wrkCode, ProdCd, sFactory);
            if (dt != null && dt.Rows.Count > 0)
            {
                SeqKey = dt.Rows[0]["SEQKEY"].ToString();
                partTypeName = dt.Rows[0]["OPRCOD"].ToString();

                //수상 or 유상 체크
                if ("PT_W".Equals(partTypeName))
                {
                    lblPartName.Text = "수상";
                }
                else if ("PT_O".Equals(partTypeName))
                {
                    lblPartName.Text = "유상";
                }
                else if(partTypeName.Contains("_M_") || partTypeName.Contains("PM"))
                {
                    lblPartName.Text = "메인";
                }
            }
            GetMaterialData();
        }

        private int CheckBeforeMatData()
        {
            //바코드 유무 체크 및 몇 번째 row인지 체크
            SheetView sv = spBeforeMat_Sheet1;
            string barcodeData = txtBarCode.Text;
            int cnt = -1;

            for (int i = 0; i < sv.RowCount; i++)
            {
                if (barcodeData.Equals(sv.Cells[i, (int)spColumn.BARCODE].Text))
                {
                    cnt = i;
                    break;
                }
            }

            return cnt;
        }
        private void GetMaterialData()
        {
            SheetView sv = spBeforeMat_Sheet1;
            SheetView sv2 = spAfterMat_Sheet1;
            DataTable dt = new DataTable();
            int row = 0;
            int row2 = 0;

            sv.RowCount = 0;
            sv2.RowCount = 0;
            //수상케이스로 인해 WATER를 따로 예외시켜놓음.
            if (wrkCode == "1" && SeqKey == "01")
            {
                qr.GetMaterialData(ref dt, lblOrderNo.Text, wrkCode, ProdCd, sFactory, SeqKey, "WATER");
            }
            else
            {
                qr.GetMaterialData(ref dt, lblOrderNo.Text, wrkCode, ProdCd, sFactory, SeqKey);
            }

            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ("Checked".Equals(dt.Rows[i]["Checked"].ToString()))
                    {
                        sv2.RowCount += 1;
                        sv2.Rows[row2].Font = new Font("맑은 고딕", 15F);
                        sv2.Rows[row2].Height = 35;
                        sv2.Cells[row2, (int)spColumn.INPUT_SEQ].Text = dt.Rows[i]["INPUT_SEQ"].ToString();
                        sv2.Cells[row2, (int)spColumn.MAT_CODE].Text = dt.Rows[i]["SPCOMP"].ToString();
                        sv2.Cells[row2, (int)spColumn.MAT_NAME].Text = dt.Rows[i]["MaterialName"].ToString();
                        sv2.Cells[row2, (int)spColumn.MAT_NAME].Font = new Font("맑은 고딕", 11F);
                        sv2.Cells[row2, (int)spColumn.MAT_QTY].Text = dt.Rows[i]["ScaleCapacity"].ToString();
                        sv2.Cells[row2, (int)spColumn.BARCODE].Text = dt.Rows[i]["ScaleBarCode"].ToString();
                        sv2.Cells[row2, (int)spColumn.CHECKED].Text = dt.Rows[i]["Checked"].ToString();
                        sv2.Cells[row2, (int)spColumn.WRKSEQ].Text = dt.Rows[i]["LineNumber"].ToString();
                        row2++;
                    }
                    else
                    {
                        sv.RowCount += 1;
                        sv.Rows[row].Font = new Font("맑은 고딕", 15F);
                        sv.Rows[row].Height = 35;
                        sv.Cells[row, (int)spColumn.INPUT_SEQ].Text = dt.Rows[i]["INPUT_SEQ"].ToString();
                        sv.Cells[row, (int)spColumn.MAT_CODE].Text = dt.Rows[i]["SPCOMP"].ToString();
                        sv.Cells[row, (int)spColumn.MAT_NAME].Text = dt.Rows[i]["MaterialName"].ToString();
                        sv.Cells[row, (int)spColumn.MAT_NAME].Font = new Font("맑은 고딕", 11F);
                        sv.Cells[row, (int)spColumn.MAT_QTY].Text = dt.Rows[i]["ScaleCapacity"].ToString();
                        sv.Cells[row, (int)spColumn.BARCODE].Text = dt.Rows[i]["ScaleBarCode"].ToString();
                        sv.Cells[row, (int)spColumn.CHECKED].Text = dt.Rows[i]["Checked"].ToString();
                        row++;
                    }
                }
            }

            //투입전 데이터는 0이고 투입 후 데이터가 있는경우 버튼 활성화
            if (sv.Rows.Count == 0 && sv2.Rows.Count > 0)
            {
                btnOK.Enabled = true;
            }
        }


        #endregion

        private void txtBarCode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //원료바코드 찍기 전에 투입탱크 먼저 찍어야함
                if (string.IsNullOrEmpty(txtTankId.Text))
                {
                    MessageBox.Show("투입탱크가 지정되지 않았습니다.\r\n투입탱크를 지정해 주시기 바랍니다.!!!");
                    txtBarCode.Text = string.Empty;
                    txtTankId.Focus();
                    txtTankId.SelectionStart = 0;
                    txtTankId.SelectionLength = txtTankId.Text.Length;
                    return;
                }

                int sRow = CheckBeforeMatData();
                if (sRow == -1)
                {
                    MessageBox.Show(this, "현재 바코드가 투입 전의 바코드 데이터에 없습니다.");
                    txtBarCode.Text = string.Empty;
                    txtBarCode.Focus();
                    return;
                }

                SheetView sv = spBeforeMat_Sheet1;
                SheetView sv2 = spAfterMat_Sheet1;
                int AfterRowCnt = sv2.RowCount;
                string ScaleBarcode = string.Empty;
                int workSeqNumber = qr.GetWorkSeqNumber(lblOrderNo.Text);

                List<int> BarcodeCnt = new List<int>();

                for (int i = 0; i < sv.RowCount; i++)
                {
                    string RDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    string useWeight = sv.Cells[i, (int)spColumn.MAT_QTY].Text;
                    string MCode = sv.Cells[i, (int)spColumn.MAT_CODE].Text;
                    ScaleBarcode = sv.Cells[i, (int)spColumn.BARCODE].Text;

                    if (ScaleBarcode.Equals(txtBarCode.Text) && string.IsNullOrEmpty(sv.Cells[i, (int)spColumn.CHECKED].Text))
                    {
                        //    OrderQty = double.Parse(sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text) > 0.0 ?
                        //                double.Parse(sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text) / 1000 : 0.0;
                        //    INPUT_SEQNO = int.Parse(sv.Cells[i, (int)spNotCheckedNoCol.SEQ].Text); //순번

                        //원료 데이터에 Checked 추가
                        int succCnt = qr.UpdateData_IF_Scale(txtBarCode.Text, txtTankId.Text, RDate, lblOrderNo.Text,
                                                                useWeight, MCode, workSeqNumber.ToString());

                        workSeqNumber++; //Max값 Update 후 +1
                    }
                }

                GetMaterialData();

                //투입 전 바코드 Sheet가 0 Row인 경우 확인버튼 활성화
                if (sv.RowCount == 0)
                {
                    btnOK.Enabled = true;
                }
            }
        }

        private void txtBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            // 키 이벤트 처리
            if (!commonFunc.ScanKeyPress(e.KeyChar, ref TempScanBarcode, ref txtBarCode))
            {
                e.Handled = true;
                return;
            }
        }

        private void spBeforeMat_CellDoubleClick(object sender, CellClickEventArgs e)
        {
            //로컬 테스트 용도
            SheetView sv = spBeforeMat_Sheet1;
            SheetView sv2 = spAfterMat_Sheet1;
            int AfterRowCnt = sv2.RowCount;
            string ScaleBarcode = string.Empty;
            int workSeqNumber = qr.GetWorkSeqNumber(lblOrderNo.Text);
            List<int> BarcodeCnt = new List<int>();

            for (int i = 0; i < sv.RowCount; i++)
            {
                string RDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string useWeight = sv.Cells[i, (int)spColumn.MAT_QTY].Text;
                string MCode = sv.Cells[i, (int)spColumn.MAT_CODE].Text;
                ScaleBarcode = sv.Cells[i, (int)spColumn.BARCODE].Text;
                
                if (ScaleBarcode.Equals(txtBarCode.Text) && string.IsNullOrEmpty(sv.Cells[i, (int)spColumn.CHECKED].Text))
                {
                //    OrderQty = double.Parse(sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text) > 0.0 ?
                //                double.Parse(sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text) / 1000 : 0.0;
                //    INPUT_SEQNO = int.Parse(sv.Cells[i, (int)spNotCheckedNoCol.SEQ].Text); //순번

                    //원료 데이터에 Checked 추가
                    int succCnt = qr.UpdateData_IF_Scale(txtBarCode.Text, txtTankId.Text, RDate, lblOrderNo.Text,
                                                            useWeight, MCode, workSeqNumber.ToString());

                    if(succCnt == 1)
                    {
                        //투입 후 Sheet로 넘겨주기
                        sv2.RowCount++; //투입 후 Row 추가
                        sv2.Cells[AfterRowCnt, (int)spColumn.INPUT_SEQ].Text = sv.Cells[i, (int)spColumn.INPUT_SEQ].Text;
                        sv2.Cells[AfterRowCnt, (int)spColumn.MAT_CODE].Text = sv.Cells[i, (int)spColumn.MAT_CODE].Text;
                        sv2.Cells[AfterRowCnt, (int)spColumn.MAT_NAME].Text = sv.Cells[i, (int)spColumn.MAT_NAME].Text;
                        sv2.Cells[AfterRowCnt, (int)spColumn.MAT_QTY].Text = sv.Cells[i, (int)spColumn.MAT_QTY].Text;
                        sv2.Cells[AfterRowCnt, (int)spColumn.BARCODE].Text = sv.Cells[i, (int)spColumn.BARCODE].Text;
                        sv2.Cells[AfterRowCnt, (int)spColumn.CHECKED].Text = "Checked";

                        sv2.Rows[AfterRowCnt].Font = new Font("맑은 고딕", 15F);
                        sv2.Rows[AfterRowCnt].Height = 35;
                        AfterRowCnt++;
                        workSeqNumber++;
                        BarcodeCnt.Add(i); //바코드 찍은 원료 갯수(ScaleBarcode 데이터 확인)
                    }
                }
            }

            BarcodeCnt.Sort();
            for (int j = BarcodeCnt.Count() - 1; j >= 0; j--)
            {
                sv.Rows.Remove(BarcodeCnt[j], 1);
            }

            //투입 전 바코드 Sheet가 0 Row인 경우 확인버튼 활성화
            if (sv.RowCount == 0)
            {
                btnOK.Enabled = true;
            }
        }

        private void spBeforeMat_CellClick(object sender, CellClickEventArgs e)
        {
            //로컬 테스트 용도
            //SheetView sv = spBeforeMat.ActiveSheet;
            //txtBarCode.Text = sv.Cells[sv.ActiveRowIndex, (int)spColumn.BARCODE].Text;
        }

        private void txtTankId_KeyPress(object sender, KeyPressEventArgs e)
        {
            // 키 이벤트 처리
            if (!commonFunc.ScanKeyPress(e.KeyChar, ref TempScanBarcode, ref txtTankId))
            {
                e.Handled = true;
                return;
            }
        }

        private void txtTankId_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string partName = string.Empty;
                string CodeData = txtTankId.Text.Trim();
                string MixerId = Program.cmx.GetTagVal("REPORT.MIXER_ID") == null ? "" : Program.cmx.GetTagVal("REPORT.MIXER_ID").ToString();
                if (CodeData.Length != 5)
                {
                    MessageBox.Show(this, "투입탱크 바코드를 확인 하시기 바랍니다.!!!!");
                    txtTankId.Text = string.Empty;
                    txtTankId.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(MixerId))
                {
                    MessageBox.Show(this, "믹서 ID 정보가 없습니다.!!!!");
                }

                switch (lblPartName.Text)
                {
                    case "수상":
                        partName = "W";
                        break;
                    case "유상":
                        partName = "O";
                        break;
                    case "메인":
                        partName = "M";
                        break;
                }

                int successCnt = qr.ChkTankId(CodeData, MixerId, partName);
                if(successCnt == 0)
                {
                    MessageBox.Show(this, "탱크 ID를 다시 확인해주세요.");
                    return;
                }

                txtBarCode.Focus();
            }
        }
    }
}
