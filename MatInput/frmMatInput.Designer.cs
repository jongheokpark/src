﻿using Core;

namespace MatInput
{
    partial class frmMatInput
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            FarPoint.Win.Spread.DefaultFocusIndicatorRenderer defaultFocusIndicatorRenderer1 = new FarPoint.Win.Spread.DefaultFocusIndicatorRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer1 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer2 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer3 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer4 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            this.lblInputMsg = new System.Windows.Forms.Label();
            this.btnOK = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtTankId = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblVersion = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblProdNo = new System.Windows.Forms.Label();
            this.lblProdOrderQty = new System.Windows.Forms.Label();
            this.lblItemCode = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblItemName = new System.Windows.Forms.Label();
            this.lblProdStartDate = new System.Windows.Forms.Label();
            this.lblOrderNo = new System.Windows.Forms.Label();
            this.lblWorker = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtBarCode = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblPartName = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.spBeforeMat = new Core.shSpread();
            this.spBeforeMat_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.spAfterMat = new Core.shSpread();
            this.spAfterMat_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spBeforeMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spBeforeMat_Sheet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAfterMat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAfterMat_Sheet1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblInputMsg
            // 
            this.lblInputMsg.AutoSize = true;
            this.lblInputMsg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.lblInputMsg.Font = new System.Drawing.Font("맑은 고딕", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblInputMsg.ForeColor = System.Drawing.Color.Black;
            this.lblInputMsg.Location = new System.Drawing.Point(330, 0);
            this.lblInputMsg.Name = "lblInputMsg";
            this.lblInputMsg.Size = new System.Drawing.Size(259, 40);
            this.lblInputMsg.TabIndex = 416;
            this.lblInputMsg.Text = "원료를 투입하세요";
            this.lblInputMsg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.btnOK.Enabled = false;
            this.btnOK.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold);
            this.btnOK.ForeColor = System.Drawing.Color.Black;
            this.btnOK.Location = new System.Drawing.Point(448, 3);
            this.btnOK.Margin = new System.Windows.Forms.Padding(0);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(135, 49);
            this.btnOK.TabIndex = 417;
            this.btnOK.Text = "확인";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(520, 39);
            this.label3.TabIndex = 419;
            this.label3.Text = "투입 전";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(101)))), ((int)(((byte)(34)))));
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(503, 39);
            this.label8.TabIndex = 420;
            this.label8.Text = "투입 후";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1024, 189);
            this.panel1.TabIndex = 422;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.06147F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.86329F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.81874F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.18298F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 94F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 380F));
            this.tableLayoutPanel1.Controls.Add(this.txtTankId, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblVersion, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblProdNo, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblProdOrderQty, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblItemCode, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label12, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label9, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblItemName, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblProdStartDate, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblOrderNo, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblWorker, 3, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(17, 19);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(990, 156);
            this.tableLayoutPanel1.TabIndex = 415;
            // 
            // txtTankId
            // 
            this.txtTankId.BackColor = System.Drawing.Color.White;
            this.txtTankId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTankId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTankId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTankId.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtTankId.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtTankId.Location = new System.Drawing.Point(610, 4);
            this.txtTankId.Name = "txtTankId";
            this.txtTankId.Size = new System.Drawing.Size(376, 32);
            this.txtTankId.TabIndex = 434;
            this.txtTankId.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTankId_KeyDown);
            this.txtTankId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTankId_KeyPress);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(1, 115);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 40);
            this.label1.TabIndex = 443;
            this.label1.Text = "제조량";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVersion
            // 
            this.lblVersion.BackColor = System.Drawing.Color.White;
            this.lblVersion.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblVersion.ForeColor = System.Drawing.Color.Black;
            this.lblVersion.Location = new System.Drawing.Point(391, 1);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(117, 36);
            this.lblVersion.TabIndex = 436;
            this.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(297, 1);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 37);
            this.label2.TabIndex = 415;
            this.label2.Text = "버전";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblProdNo
            // 
            this.lblProdNo.BackColor = System.Drawing.Color.White;
            this.lblProdNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProdNo.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblProdNo.ForeColor = System.Drawing.Color.Black;
            this.lblProdNo.Location = new System.Drawing.Point(102, 39);
            this.lblProdNo.Name = "lblProdNo";
            this.lblProdNo.Size = new System.Drawing.Size(191, 37);
            this.lblProdNo.TabIndex = 418;
            this.lblProdNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProdOrderQty
            // 
            this.lblProdOrderQty.BackColor = System.Drawing.Color.White;
            this.lblProdOrderQty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProdOrderQty.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblProdOrderQty.ForeColor = System.Drawing.Color.Black;
            this.lblProdOrderQty.Location = new System.Drawing.Point(102, 115);
            this.lblProdOrderQty.Name = "lblProdOrderQty";
            this.lblProdOrderQty.Size = new System.Drawing.Size(191, 40);
            this.lblProdOrderQty.TabIndex = 438;
            this.lblProdOrderQty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblItemCode
            // 
            this.lblItemCode.BackColor = System.Drawing.Color.White;
            this.lblItemCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblItemCode.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblItemCode.ForeColor = System.Drawing.Color.Black;
            this.lblItemCode.Location = new System.Drawing.Point(102, 77);
            this.lblItemCode.Name = "lblItemCode";
            this.lblItemCode.Size = new System.Drawing.Size(191, 37);
            this.lblItemCode.TabIndex = 437;
            this.lblItemCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(1, 39);
            this.label11.Margin = new System.Windows.Forms.Padding(0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 37);
            this.label11.TabIndex = 423;
            this.label11.Text = "제조번호";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(1, 77);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 37);
            this.label6.TabIndex = 434;
            this.label6.Text = "제조품목";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(1, 1);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 37);
            this.label4.TabIndex = 414;
            this.label4.Text = "지시번호";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(297, 39);
            this.label12.Margin = new System.Windows.Forms.Padding(0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 37);
            this.label12.TabIndex = 424;
            this.label12.Text = "제조품명";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(297, 77);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 37);
            this.label9.TabIndex = 421;
            this.label9.Text = "제조일자";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(297, 115);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 40);
            this.label7.TabIndex = 435;
            this.label7.Text = "작업자";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(512, 1);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 37);
            this.label5.TabIndex = 441;
            this.label5.Text = "탱크ID";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblItemName
            // 
            this.lblItemName.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.lblItemName, 3);
            this.lblItemName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblItemName.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblItemName.ForeColor = System.Drawing.Color.Black;
            this.lblItemName.Location = new System.Drawing.Point(391, 39);
            this.lblItemName.Name = "lblItemName";
            this.lblItemName.Size = new System.Drawing.Size(595, 37);
            this.lblItemName.TabIndex = 439;
            this.lblItemName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblProdStartDate
            // 
            this.lblProdStartDate.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.lblProdStartDate, 3);
            this.lblProdStartDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblProdStartDate.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblProdStartDate.ForeColor = System.Drawing.Color.Black;
            this.lblProdStartDate.Location = new System.Drawing.Point(391, 77);
            this.lblProdStartDate.Name = "lblProdStartDate";
            this.lblProdStartDate.Size = new System.Drawing.Size(595, 37);
            this.lblProdStartDate.TabIndex = 440;
            this.lblProdStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOrderNo
            // 
            this.lblOrderNo.BackColor = System.Drawing.Color.White;
            this.lblOrderNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOrderNo.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblOrderNo.ForeColor = System.Drawing.Color.Black;
            this.lblOrderNo.Location = new System.Drawing.Point(102, 1);
            this.lblOrderNo.Name = "lblOrderNo";
            this.lblOrderNo.Size = new System.Drawing.Size(191, 37);
            this.lblOrderNo.TabIndex = 418;
            this.lblOrderNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWorker
            // 
            this.lblWorker.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.lblWorker, 3);
            this.lblWorker.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblWorker.ForeColor = System.Drawing.Color.Black;
            this.lblWorker.Location = new System.Drawing.Point(391, 115);
            this.lblWorker.Name = "lblWorker";
            this.lblWorker.Size = new System.Drawing.Size(507, 40);
            this.lblWorker.TabIndex = 440;
            this.lblWorker.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.panel2.Controls.Add(this.btnOK);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 666);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1024, 55);
            this.panel2.TabIndex = 423;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.panel3.Controls.Add(this.txtBarCode);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.lblPartName);
            this.panel3.Controls.Add(this.lblInputMsg);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 189);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1024, 49);
            this.panel3.TabIndex = 424;
            // 
            // txtBarCode
            // 
            this.txtBarCode.BackColor = System.Drawing.Color.White;
            this.txtBarCode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtBarCode.Font = new System.Drawing.Font("맑은 고딕", 20.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtBarCode.Location = new System.Drawing.Point(786, 0);
            this.txtBarCode.Name = "txtBarCode";
            this.txtBarCode.Size = new System.Drawing.Size(221, 36);
            this.txtBarCode.TabIndex = 417;
            this.txtBarCode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtBarCode_KeyDown);
            this.txtBarCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarCode_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(688, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 30);
            this.label10.TabIndex = 416;
            this.label10.Text = "Barcode";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPartName
            // 
            this.lblPartName.AutoSize = true;
            this.lblPartName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
            this.lblPartName.Font = new System.Drawing.Font("맑은 고딕", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblPartName.ForeColor = System.Drawing.Color.Black;
            this.lblPartName.Location = new System.Drawing.Point(260, 0);
            this.lblPartName.Name = "lblPartName";
            this.lblPartName.Size = new System.Drawing.Size(75, 40);
            this.lblPartName.TabIndex = 416;
            this.lblPartName.Text = "수상";
            this.lblPartName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 238);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.spBeforeMat);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.spAfterMat);
            this.splitContainer1.Panel2.Controls.Add(this.label8);
            this.splitContainer1.Size = new System.Drawing.Size(1024, 428);
            this.splitContainer1.SplitterDistance = 520;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 425;
            // 
            // spBeforeMat
            // 
            this.spBeforeMat.AccessibleDescription = "spBeforeMat, Sheet1, Row 0, Column 0, ";
            this.spBeforeMat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spBeforeMat.FocusRenderer = defaultFocusIndicatorRenderer1;
            this.spBeforeMat.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.spBeforeMat.HorizontalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spBeforeMat.HorizontalScrollBar.Name = "";
            this.spBeforeMat.HorizontalScrollBar.Renderer = defaultScrollBarRenderer1;
            this.spBeforeMat.HorizontalScrollBar.TabIndex = 22;
            this.spBeforeMat.Location = new System.Drawing.Point(0, 39);
            this.spBeforeMat.Name = "spBeforeMat";
            this.spBeforeMat.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spBeforeMat_Sheet1});
            this.spBeforeMat.Size = new System.Drawing.Size(520, 389);
            this.spBeforeMat.Skin = FarPoint.Win.Spread.DefaultSpreadSkins.Classic;
            this.spBeforeMat.TabIndex = 420;
            this.spBeforeMat.VerticalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spBeforeMat.VerticalScrollBar.Name = "";
            this.spBeforeMat.VerticalScrollBar.Renderer = defaultScrollBarRenderer2;
            this.spBeforeMat.VerticalScrollBar.TabIndex = 23;
            this.spBeforeMat.VisualStyles = FarPoint.Win.VisualStyles.Off;
            this.spBeforeMat.CellClick += new FarPoint.Win.Spread.CellClickEventHandler(this.spBeforeMat_CellClick);
            this.spBeforeMat.CellDoubleClick += new FarPoint.Win.Spread.CellClickEventHandler(this.spBeforeMat_CellDoubleClick);
            // 
            // spBeforeMat_Sheet1
            // 
            this.spBeforeMat_Sheet1.Reset();
            this.spBeforeMat_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spBeforeMat_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spBeforeMat_Sheet1.ColumnCount = 7;
            this.spBeforeMat_Sheet1.RowHeader.ColumnCount = 0;
            this.spBeforeMat_Sheet1.ColumnFooter.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spBeforeMat_Sheet1.ColumnFooter.DefaultStyle.Parent = "HeaderDefault";
            this.spBeforeMat_Sheet1.ColumnFooterSheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spBeforeMat_Sheet1.ColumnFooterSheetCornerStyle.Parent = "CornerDefault";
            this.spBeforeMat_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "순번";
            this.spBeforeMat_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "원료번호";
            this.spBeforeMat_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "원료명";
            this.spBeforeMat_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "투입량";
            this.spBeforeMat_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "바코드";
            this.spBeforeMat_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "CHECKED";
            this.spBeforeMat_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "Sequence";
            this.spBeforeMat_Sheet1.ColumnHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spBeforeMat_Sheet1.ColumnHeader.DefaultStyle.Parent = "HeaderDefault";
            this.spBeforeMat_Sheet1.Columns.Get(0).Label = "순번";
            this.spBeforeMat_Sheet1.Columns.Get(0).Width = 50F;
            this.spBeforeMat_Sheet1.Columns.Get(1).Label = "원료번호";
            this.spBeforeMat_Sheet1.Columns.Get(1).Width = 90F;
            this.spBeforeMat_Sheet1.Columns.Get(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
            this.spBeforeMat_Sheet1.Columns.Get(2).Label = "원료명";
            this.spBeforeMat_Sheet1.Columns.Get(2).Width = 280F;
            this.spBeforeMat_Sheet1.Columns.Get(3).Label = "투입량";
            this.spBeforeMat_Sheet1.Columns.Get(3).Width = 90F;
            this.spBeforeMat_Sheet1.Columns.Get(4).Label = "바코드";
            this.spBeforeMat_Sheet1.Columns.Get(4).Width = 110F;
            this.spBeforeMat_Sheet1.Columns.Get(5).Label = "CHECKED";
            this.spBeforeMat_Sheet1.Columns.Get(5).Width = 87F;
            this.spBeforeMat_Sheet1.Columns.Get(6).Label = "Sequence";
            this.spBeforeMat_Sheet1.Columns.Get(6).Width = 90F;
            this.spBeforeMat_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spBeforeMat_Sheet1.RowHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spBeforeMat_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderDefault";
            this.spBeforeMat_Sheet1.SheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spBeforeMat_Sheet1.SheetCornerStyle.Parent = "CornerDefault";
            this.spBeforeMat_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // spAfterMat
            // 
            this.spAfterMat.AccessibleDescription = "spAfterMat, Sheet1, Row 0, Column 0, ";
            this.spAfterMat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spAfterMat.FocusRenderer = defaultFocusIndicatorRenderer1;
            this.spAfterMat.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.spAfterMat.HorizontalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spAfterMat.HorizontalScrollBar.Name = "";
            this.spAfterMat.HorizontalScrollBar.Renderer = defaultScrollBarRenderer3;
            this.spAfterMat.HorizontalScrollBar.TabIndex = 24;
            this.spAfterMat.Location = new System.Drawing.Point(0, 39);
            this.spAfterMat.Name = "spAfterMat";
            this.spAfterMat.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spAfterMat_Sheet1});
            this.spAfterMat.Size = new System.Drawing.Size(503, 389);
            this.spAfterMat.Skin = FarPoint.Win.Spread.DefaultSpreadSkins.Classic;
            this.spAfterMat.TabIndex = 421;
            this.spAfterMat.VerticalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spAfterMat.VerticalScrollBar.Name = "";
            this.spAfterMat.VerticalScrollBar.Renderer = defaultScrollBarRenderer4;
            this.spAfterMat.VerticalScrollBar.TabIndex = 25;
            this.spAfterMat.VisualStyles = FarPoint.Win.VisualStyles.Off;
            // 
            // spAfterMat_Sheet1
            // 
            this.spAfterMat_Sheet1.Reset();
            this.spAfterMat_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spAfterMat_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spAfterMat_Sheet1.ColumnCount = 7;
            this.spAfterMat_Sheet1.RowHeader.ColumnCount = 0;
            this.spAfterMat_Sheet1.ColumnFooter.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spAfterMat_Sheet1.ColumnFooter.DefaultStyle.Parent = "HeaderDefault";
            this.spAfterMat_Sheet1.ColumnFooterSheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spAfterMat_Sheet1.ColumnFooterSheetCornerStyle.Parent = "CornerDefault";
            this.spAfterMat_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "순번";
            this.spAfterMat_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "원료번호";
            this.spAfterMat_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "원료명";
            this.spAfterMat_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "투입량";
            this.spAfterMat_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "바코드";
            this.spAfterMat_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "CHECKED";
            this.spAfterMat_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "Sequence";
            this.spAfterMat_Sheet1.ColumnHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spAfterMat_Sheet1.ColumnHeader.DefaultStyle.Parent = "HeaderDefault";
            this.spAfterMat_Sheet1.Columns.Get(0).Label = "순번";
            this.spAfterMat_Sheet1.Columns.Get(0).Width = 50F;
            this.spAfterMat_Sheet1.Columns.Get(1).Label = "원료번호";
            this.spAfterMat_Sheet1.Columns.Get(1).Width = 90F;
            this.spAfterMat_Sheet1.Columns.Get(2).Font = new System.Drawing.Font("맑은 고딕", 11F);
            this.spAfterMat_Sheet1.Columns.Get(2).HorizontalAlignment = FarPoint.Win.Spread.CellHorizontalAlignment.Left;
            this.spAfterMat_Sheet1.Columns.Get(2).Label = "원료명";
            this.spAfterMat_Sheet1.Columns.Get(2).Width = 280F;
            this.spAfterMat_Sheet1.Columns.Get(3).Label = "투입량";
            this.spAfterMat_Sheet1.Columns.Get(3).Width = 90F;
            this.spAfterMat_Sheet1.Columns.Get(4).Label = "바코드";
            this.spAfterMat_Sheet1.Columns.Get(4).Width = 110F;
            this.spAfterMat_Sheet1.Columns.Get(5).Label = "CHECKED";
            this.spAfterMat_Sheet1.Columns.Get(5).Width = 97F;
            this.spAfterMat_Sheet1.Columns.Get(6).Label = "Sequence";
            this.spAfterMat_Sheet1.Columns.Get(6).Width = 90F;
            this.spAfterMat_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spAfterMat_Sheet1.RowHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spAfterMat_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderDefault";
            this.spAfterMat_Sheet1.SheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spAfterMat_Sheet1.SheetCornerStyle.Parent = "CornerDefault";
            this.spAfterMat_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // frmMatInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.ClientSize = new System.Drawing.Size(1024, 721);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMatInput";
            this.Text = "MatInput";
            this.Load += new System.EventHandler(this.frmMatInput_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmMatInput_KeyPress);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spBeforeMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spBeforeMat_Sheet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAfterMat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spAfterMat_Sheet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        internal System.Windows.Forms.Label lblInputMsg;
        internal System.Windows.Forms.Button btnOK;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private shSpread spBeforeMat;
        private FarPoint.Win.Spread.SheetView spBeforeMat_Sheet1;
        private shSpread spAfterMat;
        private FarPoint.Win.Spread.SheetView spAfterMat_Sheet1;
        internal System.Windows.Forms.Label lblPartName;
        private System.Windows.Forms.TextBox txtBarCode;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        internal System.Windows.Forms.Label label1;
        internal System.Windows.Forms.Label lblVersion;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label lblProdNo;
        internal System.Windows.Forms.Label lblProdOrderQty;
        internal System.Windows.Forms.Label lblItemCode;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label label12;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label lblItemName;
        internal System.Windows.Forms.Label lblProdStartDate;
        internal System.Windows.Forms.Label lblWorker;
        internal System.Windows.Forms.Label lblOrderNo;
        internal System.Windows.Forms.TextBox txtTankId;
        internal System.Windows.Forms.Label label10;
    }
}

