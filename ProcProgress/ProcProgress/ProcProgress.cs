﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;
using System.Configuration;
using FarPoint.Win.Spread;

namespace ProcProgress
{
    public partial class ProcProgress : Form
    {
        #region 전역변수
        CommonCode commonFunc = new CommonCode();
        QueryResult qr = new QueryResult();
        #endregion
        #region 생성자
        public ProcProgress()
        {
            InitializeComponent();
        }
        enum spFromMatColumn
        {
            INPUT_SEQ,
            MAT_CODE,
            MAT_NAME
        }
        enum spToMatColumn
        {
            INPUT_SEQ,
            MAT_CODE,
            MAT_NAME,
            LINE_NUMBER
        }
        #endregion
        #region Form
        public void ProcProgress_Load(object sender, EventArgs e)
        {
            SetGrid();
            SetControl();
        }
        #endregion

        #region Method
        private void SetGrid()
        {
            //Sheet
            commonFunc.InitSheet(ref spFromMat_Sheet1, 0, spFromMat_Sheet1.ColumnCount);
            commonFunc.SetColHeader(spFromMat_Sheet1, typeof(spFromMatColumn));
            spFromMat_Sheet1.Columns.Get((int)spFromMatColumn.MAT_NAME).HorizontalAlignment = CellHorizontalAlignment.Left;

            commonFunc.InitSheet(ref spToMat_Sheet1, 0, spToMat_Sheet1.ColumnCount);
            commonFunc.SetColHeader(spToMat_Sheet1, typeof(spToMatColumn));
            spToMat_Sheet1.Columns.Get((int)spToMatColumn.MAT_NAME).HorizontalAlignment = CellHorizontalAlignment.Left;
        }
        private void SetControl()
        {
            //상단부분 데이터 세팅
            //wrkCode = Program.cmx.GetTagVal("CMD.STEP") == null ? "1" : Program.cmx.GetTagVal("CMD.STEP").ToString();
            lblOrderNo.Text = Program.cmx.GetTagVal("REPORT.생산1.지시번호") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.지시번호").ToString();
            lblVersion.Text = Program.cmx.GetTagVal("REPORT.VER") == null ? "" : Program.cmx.GetTagVal("REPORT.VER").ToString();
            lblProdNo.Text = Program.cmx.GetTagVal("REPORT.생산1.제조번호") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조번호").ToString();
            lblTankID.Text = "";//Program.cmx.GetTagVal("REPORT.MIXER_ID") == null ? "" : Program.cmx.GetTagVal("REPORT.MIXER_ID").ToString();
            lblItemCode.Text = Program.cmx.GetTagVal("REPORT.생산1.제조품목") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조품목").ToString();
            lblItemName.Text = Program.cmx.GetTagVal("REPORT.생산1.제조품명") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조품명").ToString();
            lblProdOrderQty.Text = Program.cmx.GetTagVal("REPORT.생산1.제조량") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조량").ToString();
            lblProdStartDate.Text = Program.cmx.GetTagVal("REPORT.생산1.시작일시2") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.시작일시2").ToString();
            lblWorker.Text = Program.cmx.GetTagVal("작업자.작업자_N1") == null ? "" : Program.cmx.GetTagVal("작업자.작업자_N1").ToString();

            string sCompId = ConfigurationSettings.AppSettings["CompId"];
            //Tab1 원료
            DataTable dt = new DataTable();
            int retVal = qr.GetMaterialData(ref dt, lblOrderNo.Text, "", lblItemCode.Text, sCompId, "", "PROCPROGRESS");
            if(retVal < 0)
            {
                MessageBox.Show("원료 조회 실패");
            }
            SheetView sv = spFromMat_Sheet1;
            SheetView sv2 = spToMat_Sheet1;

            int row = 0;
            int row2 = 0;

            sv.RowCount = 0;
            sv2.RowCount = 0;
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ("Checked".Equals(dt.Rows[i]["Checked"].ToString()))
                    {
                        //투입 후 원료 표시
                        sv2.RowCount += 1;
                        sv2.Rows[row2].Font = new Font("맑은 고딕", 15F);
                        sv2.Rows[row2].Height = 35;
                        sv2.Cells[row2, (int)spToMatColumn.LINE_NUMBER].Text = dt.Rows[i]["LINENUMBER"].ToString();
                        sv2.Cells[row2, (int)spToMatColumn.INPUT_SEQ].Text = dt.Rows[i]["INPUT_SEQ"].ToString();
                        sv2.Cells[row2, (int)spToMatColumn.MAT_CODE].Text = dt.Rows[i]["SPCOMP"].ToString();
                        sv2.Cells[row2, (int)spToMatColumn.MAT_NAME].Text = dt.Rows[i]["MaterialName"].ToString();
                        sv2.Cells[row2, (int)spToMatColumn.MAT_NAME].Font = new Font("맑은 고딕", 11F);
                        row2++;
                    }
                    else
                    {
                        //투입 전 원료 표시
                        sv.RowCount += 1;
                        sv.Rows[row].Font = new Font("맑은 고딕", 15F);
                        sv.Rows[row].Height = 35;
                        sv.Cells[row, (int)spFromMatColumn.INPUT_SEQ].Text = dt.Rows[i]["INPUT_SEQ"].ToString();
                        sv.Cells[row, (int)spFromMatColumn.MAT_CODE].Text = dt.Rows[i]["SPCOMP"].ToString();
                        sv.Cells[row, (int)spFromMatColumn.MAT_NAME].Text = dt.Rows[i]["MaterialName"].ToString();
                        sv.Cells[row, (int)spFromMatColumn.MAT_NAME].Font = new Font("맑은 고딕", 11F);
                        row++;
                    }
                }
            }
            

            //현재까지의 원료 투입률(%) 표시 (지시의 원료 수 : 현재까지 투입된 원료 수)
            int inputPercent = 0;
            if (sv2.RowCount > 0)
            {
                inputPercent = sv2.RowCount * 100 / (sv.RowCount + sv2.RowCount);
            }
            lblInputPercent.Text = inputPercent + "%";

            //Tab2 알람
            retVal = qr.GetAlarm(ref dt, sCompId, lblOrderNo.Text, lblProdNo.Text, lblItemCode.Text, lblTankID.Text, lblProdStartDate.Text);
            if (retVal < 0)
            {
                MessageBox.Show("알람 조회 실패");
            }
            foreach (DataRow dr in dt.Rows)
            {
                txtAlarm.AppendText(dr["ALARM_TEXT"].ToString());
                txtAlarm.AppendText(Environment.NewLine);
            }

            //Tab3 특이사항
            string yesCase = "|^|Y";
            string noCase = "|^|N";
            dt = new DataTable();
            retVal = qr.GetComment(ref dt, lblItemCode.Text, lblVersion.Text.Replace("Ver", "").Trim());
            //qr.GetComment(ref dt, "3SLC00008111", "0"); //테스트용
            if (retVal < 0)
            {
                MessageBox.Show("특이사항 조회 실패");
            }
            txtComment.Text = "";
            if (dt != null && dt.Rows.Count > 0)
            {
                string sComment = dt.Rows[0]["RTGETC"].ToString();
                string[] vComment = sComment.Split('\n');

                for (int i = 0; i < vComment.Count(); i++)
                {
                    if (vComment[i].Contains(yesCase))
                    {
                        string chgText = vComment[i].Replace(yesCase, "");
                        int chgTextLength = chgText.Length;
                        int textLength = txtComment.Text.Length;
                        txtComment.AppendText(chgText);
                        txtComment.SelectionStart = textLength;
                        txtComment.SelectionLength = chgTextLength;
                        txtComment.SelectionColor = Color.Red;
                    }
                    else
                    {
                        string chgText = vComment[i].Replace(noCase, "");
                        int chgTextLength = chgText.Length;
                        int textLength = txtComment.Text.Length;
                        txtComment.AppendText(chgText);
                        txtComment.SelectionStart = textLength;
                        txtComment.SelectionLength = chgTextLength;
                        txtComment.SelectionColor = Color.Black;
                    }
                    txtComment.AppendText(Environment.NewLine);
                }
            }
        }
        #endregion

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
