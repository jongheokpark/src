﻿using Core;

namespace OrderMatInput
{
    partial class Main
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            FarPoint.Win.Spread.DefaultFocusIndicatorRenderer defaultFocusIndicatorRenderer1 = new FarPoint.Win.Spread.DefaultFocusIndicatorRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer1 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer2 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer3 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer4 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer5 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer6 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer7 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer8 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer9 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer10 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer11 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer12 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer13 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            FarPoint.Win.Spread.DefaultScrollBarRenderer defaultScrollBarRenderer14 = new FarPoint.Win.Spread.DefaultScrollBarRenderer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btnClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblManufactureName_1st = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.lblManufactureiTem_1st = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.btnOrderNo_1 = new System.Windows.Forms.Button();
            this.lblManufactureNo_1st = new System.Windows.Forms.Label();
            this.TextBox1 = new System.Windows.Forms.TextBox();
            this.lblOrderNo_1st = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.Label30 = new System.Windows.Forms.Label();
            this.spOrderNo = new Core.shSpread();
            this.spOrderNo_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblUnCheckedCNT = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.TextBox4 = new System.Windows.Forms.TextBox();
            this.Button4 = new System.Windows.Forms.Button();
            this.TextBox2 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.lblBarCode_1st = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.spCheckedNo = new Core.shSpread();
            this.spCheckedNo_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.spNotCheckedNo = new Core.shSpread();
            this.spNotCheckedNo_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lblManufactureName_2nd = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblManufactureiTem_2nd = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnOrderNo_2 = new System.Windows.Forms.Button();
            this.lblManufactureNo_2nd = new System.Windows.Forms.Label();
            this.TextBox6 = new System.Windows.Forms.TextBox();
            this.lblOrderNo_2nd = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.spOrderNo_2 = new Core.shSpread();
            this.spOrderNo_2_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lblUnCheckedCNT2 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.TextBox5 = new System.Windows.Forms.TextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.TextBox3 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.lblBarCode_2nd = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.spCheckedNo_2 = new Core.shSpread();
            this.spCheckedNo_2_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.spNotCheckedNo_2 = new Core.shSpread();
            this.spNotCheckedNo_2_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.spSpread = new Core.shSpread();
            this.spSpread_Sheet1 = new FarPoint.Win.Spread.SheetView();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblPWBarCode_2nd = new System.Windows.Forms.Label();
            this.lblPWBarCode_1st = new System.Windows.Forms.Label();
            this.lblETHANOLBarCode_2nd = new System.Windows.Forms.Label();
            this.lblETHANOLBarCode_1st = new System.Windows.Forms.Label();
            this.lblSILICONBarCode_2nd = new System.Windows.Forms.Label();
            this.lblSILICONBarCode_1st = new System.Windows.Forms.Label();
            this.lblLILYBarCode_2nd = new System.Windows.Forms.Label();
            this.lblLILYBarCode_1st = new System.Windows.Forms.Label();
            this.lbl13BGBarCode_2nd = new System.Windows.Forms.Label();
            this.lbl13BGBarCode_1st = new System.Windows.Forms.Label();
            this.lblGlycerineBarCode_2nd = new System.Windows.Forms.Label();
            this.lblGlycerineBarCode_1st = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spOrderNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spOrderNo_Sheet1)).BeginInit();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spCheckedNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spCheckedNo_Sheet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spNotCheckedNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spNotCheckedNo_Sheet1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spOrderNo_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spOrderNo_2_Sheet1)).BeginInit();
            this.panel8.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spCheckedNo_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spCheckedNo_2_Sheet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spNotCheckedNo_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spNotCheckedNo_2_Sheet1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spSpread)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spSpread_Sheet1)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.dateTimePicker1);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(982, 44);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(352, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 18);
            this.button1.TabIndex = 4;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(237, 12);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(109, 21);
            this.dateTimePicker1.TabIndex = 3;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("돋움", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnClose.Location = new System.Drawing.Point(844, 3);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(135, 38);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "닫기";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("돋움", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(99, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "1000(7)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("돋움", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(19, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "장치이름 :";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 44);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(982, 564);
            this.panel2.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(982, 564);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Click += new System.EventHandler(this.tabControl1_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(974, 538);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "생산1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.spOrderNo, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.43609F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.46617F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.18797F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(968, 532);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblManufactureName_1st);
            this.panel3.Controls.Add(this.Label13);
            this.panel3.Controls.Add(this.lblManufactureiTem_1st);
            this.panel3.Controls.Add(this.Label5);
            this.panel3.Controls.Add(this.btnOrderNo_1);
            this.panel3.Controls.Add(this.lblManufactureNo_1st);
            this.panel3.Controls.Add(this.TextBox1);
            this.panel3.Controls.Add(this.lblOrderNo_1st);
            this.panel3.Controls.Add(this.label31);
            this.panel3.Controls.Add(this.Label30);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(962, 69);
            this.panel3.TabIndex = 0;
            // 
            // lblManufactureName_1st
            // 
            this.lblManufactureName_1st.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblManufactureName_1st.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManufactureName_1st.Location = new System.Drawing.Point(539, 45);
            this.lblManufactureName_1st.Name = "lblManufactureName_1st";
            this.lblManufactureName_1st.Size = new System.Drawing.Size(302, 19);
            this.lblManufactureName_1st.TabIndex = 63;
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.Location = new System.Drawing.Point(459, 50);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(85, 14);
            this.Label13.TabIndex = 62;
            this.Label13.Text = "제조이름 : ";
            // 
            // lblManufactureiTem_1st
            // 
            this.lblManufactureiTem_1st.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblManufactureiTem_1st.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManufactureiTem_1st.Location = new System.Drawing.Point(104, 45);
            this.lblManufactureiTem_1st.Name = "lblManufactureiTem_1st";
            this.lblManufactureiTem_1st.Size = new System.Drawing.Size(302, 19);
            this.lblManufactureiTem_1st.TabIndex = 61;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(23, 50);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(85, 14);
            this.Label5.TabIndex = 60;
            this.Label5.Text = "제조품목 : ";
            // 
            // btnOrderNo_1
            // 
            this.btnOrderNo_1.Font = new System.Drawing.Font("돋움", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrderNo_1.Location = new System.Drawing.Point(26, 3);
            this.btnOrderNo_1.Name = "btnOrderNo_1";
            this.btnOrderNo_1.Size = new System.Drawing.Size(73, 33);
            this.btnOrderNo_1.TabIndex = 55;
            this.btnOrderNo_1.Text = "지시번호";
            this.btnOrderNo_1.UseVisualStyleBackColor = true;
            this.btnOrderNo_1.Click += new System.EventHandler(this.btnOrderNo_1_Click);
            // 
            // lblManufactureNo_1st
            // 
            this.lblManufactureNo_1st.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblManufactureNo_1st.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManufactureNo_1st.Location = new System.Drawing.Point(669, 13);
            this.lblManufactureNo_1st.Name = "lblManufactureNo_1st";
            this.lblManufactureNo_1st.Size = new System.Drawing.Size(172, 19);
            this.lblManufactureNo_1st.TabIndex = 59;
            // 
            // TextBox1
            // 
            this.TextBox1.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox1.Location = new System.Drawing.Point(116, 7);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.Size = new System.Drawing.Size(190, 25);
            this.TextBox1.TabIndex = 54;
            this.TextBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox1_KeyDown);
            this.TextBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox1_KeyPress);
            // 
            // lblOrderNo_1st
            // 
            this.lblOrderNo_1st.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblOrderNo_1st.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderNo_1st.Location = new System.Drawing.Point(406, 13);
            this.lblOrderNo_1st.Name = "lblOrderNo_1st";
            this.lblOrderNo_1st.Size = new System.Drawing.Size(172, 19);
            this.lblOrderNo_1st.TabIndex = 58;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(593, 13);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(85, 14);
            this.label31.TabIndex = 57;
            this.label31.Text = "제조번호 : ";
            // 
            // Label30
            // 
            this.Label30.AutoSize = true;
            this.Label30.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label30.Location = new System.Drawing.Point(330, 13);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(85, 14);
            this.Label30.TabIndex = 56;
            this.Label30.Text = "지시번호 : ";
            // 
            // spOrderNo
            // 
            this.spOrderNo.AccessibleDescription = "spOrderNo, Sheet1, Row 0, Column 0, ";
            this.spOrderNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spOrderNo.FocusRenderer = defaultFocusIndicatorRenderer1;
            this.spOrderNo.HorizontalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spOrderNo.HorizontalScrollBar.Name = "";
            this.spOrderNo.HorizontalScrollBar.Renderer = defaultScrollBarRenderer1;
            this.spOrderNo.HorizontalScrollBar.TabIndex = 6;
            this.spOrderNo.Location = new System.Drawing.Point(3, 78);
            this.spOrderNo.Name = "spOrderNo";
            this.spOrderNo.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spOrderNo_Sheet1});
            this.spOrderNo.Size = new System.Drawing.Size(962, 123);
            this.spOrderNo.Skin = FarPoint.Win.Spread.DefaultSpreadSkins.Classic;
            this.spOrderNo.TabIndex = 1;
            this.spOrderNo.VerticalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spOrderNo.VerticalScrollBar.Name = "";
            this.spOrderNo.VerticalScrollBar.Renderer = defaultScrollBarRenderer2;
            this.spOrderNo.VerticalScrollBar.TabIndex = 7;
            this.spOrderNo.VisualStyles = FarPoint.Win.VisualStyles.Off;
            this.spOrderNo.Click += new System.EventHandler(this.spOrderNo_Click);
            // 
            // spOrderNo_Sheet1
            // 
            this.spOrderNo_Sheet1.Reset();
            this.spOrderNo_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spOrderNo_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spOrderNo_Sheet1.ColumnCount = 11;
            this.spOrderNo_Sheet1.RowHeader.ColumnCount = 0;
            this.spOrderNo_Sheet1.ColumnFooter.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spOrderNo_Sheet1.ColumnFooter.DefaultStyle.Parent = "HeaderDefault";
            this.spOrderNo_Sheet1.ColumnFooterSheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spOrderNo_Sheet1.ColumnFooterSheetCornerStyle.Parent = "CornerDefault";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "지시번호";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "제조번호";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "BulkName";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "제조품목";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "제조실";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "제조명";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "제조량";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 7).Value = "제조일시";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 8).Value = "사용설비";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 9).Value = "Check";
            this.spOrderNo_Sheet1.ColumnHeader.Cells.Get(0, 10).Value = "사용자";
            this.spOrderNo_Sheet1.ColumnHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spOrderNo_Sheet1.ColumnHeader.DefaultStyle.Parent = "HeaderDefault";
            this.spOrderNo_Sheet1.Columns.Get(0).Label = "지시번호";
            this.spOrderNo_Sheet1.Columns.Get(0).Width = 122F;
            this.spOrderNo_Sheet1.Columns.Get(1).Label = "제조번호";
            this.spOrderNo_Sheet1.Columns.Get(1).Width = 100F;
            this.spOrderNo_Sheet1.Columns.Get(2).Label = "BulkName";
            this.spOrderNo_Sheet1.Columns.Get(2).Width = 127F;
            this.spOrderNo_Sheet1.Columns.Get(3).Label = "제조품목";
            this.spOrderNo_Sheet1.Columns.Get(3).Width = 100F;
            this.spOrderNo_Sheet1.Columns.Get(4).Label = "제조실";
            this.spOrderNo_Sheet1.Columns.Get(4).Width = 90F;
            this.spOrderNo_Sheet1.Columns.Get(5).Label = "제조명";
            this.spOrderNo_Sheet1.Columns.Get(5).Width = 300F;
            this.spOrderNo_Sheet1.Columns.Get(6).Label = "제조량";
            this.spOrderNo_Sheet1.Columns.Get(6).Width = 100F;
            this.spOrderNo_Sheet1.Columns.Get(7).Label = "제조일시";
            this.spOrderNo_Sheet1.Columns.Get(7).Width = 200F;
            this.spOrderNo_Sheet1.Columns.Get(8).Label = "사용설비";
            this.spOrderNo_Sheet1.Columns.Get(8).Width = 100F;
            this.spOrderNo_Sheet1.Columns.Get(9).Label = "Check";
            this.spOrderNo_Sheet1.Columns.Get(9).Width = 80F;
            this.spOrderNo_Sheet1.Columns.Get(10).Label = "사용자";
            this.spOrderNo_Sheet1.Columns.Get(10).Width = 80F;
            this.spOrderNo_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spOrderNo_Sheet1.RowHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spOrderNo_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderDefault";
            this.spOrderNo_Sheet1.SheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spOrderNo_Sheet1.SheetCornerStyle.Parent = "CornerDefault";
            this.spOrderNo_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblUnCheckedCNT);
            this.panel4.Controls.Add(this.Label12);
            this.panel4.Controls.Add(this.TextBox4);
            this.panel4.Controls.Add(this.Button4);
            this.panel4.Controls.Add(this.TextBox2);
            this.panel4.Controls.Add(this.button2);
            this.panel4.Controls.Add(this.lblBarCode_1st);
            this.panel4.Controls.Add(this.Label11);
            this.panel4.Controls.Add(this.Label10);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 207);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(962, 54);
            this.panel4.TabIndex = 2;
            // 
            // lblUnCheckedCNT
            // 
            this.lblUnCheckedCNT.AutoSize = true;
            this.lblUnCheckedCNT.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnCheckedCNT.Location = new System.Drawing.Point(110, 3);
            this.lblUnCheckedCNT.Name = "lblUnCheckedCNT";
            this.lblUnCheckedCNT.Size = new System.Drawing.Size(13, 14);
            this.lblUnCheckedCNT.TabIndex = 70;
            this.lblUnCheckedCNT.Text = "0";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(9, 3);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(95, 14);
            this.Label12.TabIndex = 69;
            this.Label12.Text = "Check안된수량 : ";
            // 
            // TextBox4
            // 
            this.TextBox4.Location = new System.Drawing.Point(103, 24);
            this.TextBox4.Name = "TextBox4";
            this.TextBox4.Size = new System.Drawing.Size(167, 21);
            this.TextBox4.TabIndex = 68;
            this.TextBox4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox4_KeyDown);
            this.TextBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox4_KeyPress);
            // 
            // Button4
            // 
            this.Button4.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button4.Location = new System.Drawing.Point(6, 22);
            this.Button4.Name = "Button4";
            this.Button4.Size = new System.Drawing.Size(91, 30);
            this.Button4.TabIndex = 67;
            this.Button4.Text = "투입탱크";
            this.Button4.UseVisualStyleBackColor = true;
            this.Button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // TextBox2
            // 
            this.TextBox2.Location = new System.Drawing.Point(479, 24);
            this.TextBox2.Name = "TextBox2";
            this.TextBox2.Size = new System.Drawing.Size(210, 21);
            this.TextBox2.TabIndex = 63;
            this.TextBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox2_KeyDown);
            this.TextBox2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox2_KeyPress);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(309, 22);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(98, 30);
            this.button2.TabIndex = 66;
            this.button2.Text = "원료바코드";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // lblBarCode_1st
            // 
            this.lblBarCode_1st.AutoSize = true;
            this.lblBarCode_1st.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblBarCode_1st.Location = new System.Drawing.Point(702, 28);
            this.lblBarCode_1st.Name = "lblBarCode_1st";
            this.lblBarCode_1st.Size = new System.Drawing.Size(71, 14);
            this.lblBarCode_1st.TabIndex = 65;
            this.lblBarCode_1st.Text = "BarCode";
            this.lblBarCode_1st.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Label11.Location = new System.Drawing.Point(413, 27);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(70, 14);
            this.Label11.TabIndex = 64;
            this.Label11.Text = "바코드 : ";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Microsoft YaHei", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.ForeColor = System.Drawing.Color.Red;
            this.Label10.Location = new System.Drawing.Point(828, 15);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(101, 30);
            this.Label10.TabIndex = 62;
            this.Label10.Text = "Label10";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.61746F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.38254F));
            this.tableLayoutPanel2.Controls.Add(this.spCheckedNo, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.spNotCheckedNo, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 267);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(962, 262);
            this.tableLayoutPanel2.TabIndex = 3;
            // 
            // spCheckedNo
            // 
            this.spCheckedNo.AccessibleDescription = "spCheckedNo, Sheet1, Row 0, Column 0, ";
            this.spCheckedNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spCheckedNo.FocusRenderer = defaultFocusIndicatorRenderer1;
            this.spCheckedNo.HorizontalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spCheckedNo.HorizontalScrollBar.Name = "";
            this.spCheckedNo.HorizontalScrollBar.Renderer = defaultScrollBarRenderer3;
            this.spCheckedNo.HorizontalScrollBar.TabIndex = 4;
            this.spCheckedNo.Location = new System.Drawing.Point(614, 3);
            this.spCheckedNo.Name = "spCheckedNo";
            this.spCheckedNo.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spCheckedNo_Sheet1});
            this.spCheckedNo.Size = new System.Drawing.Size(345, 256);
            this.spCheckedNo.Skin = FarPoint.Win.Spread.DefaultSpreadSkins.Classic;
            this.spCheckedNo.TabIndex = 3;
            this.spCheckedNo.VerticalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spCheckedNo.VerticalScrollBar.Name = "";
            this.spCheckedNo.VerticalScrollBar.Renderer = defaultScrollBarRenderer4;
            this.spCheckedNo.VerticalScrollBar.TabIndex = 5;
            this.spCheckedNo.VisualStyles = FarPoint.Win.VisualStyles.Off;
            // 
            // spCheckedNo_Sheet1
            // 
            this.spCheckedNo_Sheet1.Reset();
            this.spCheckedNo_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spCheckedNo_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spCheckedNo_Sheet1.ColumnCount = 12;
            this.spCheckedNo_Sheet1.RowHeader.ColumnCount = 0;
            this.spCheckedNo_Sheet1.ColumnFooter.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spCheckedNo_Sheet1.ColumnFooter.DefaultStyle.Parent = "HeaderDefault";
            this.spCheckedNo_Sheet1.ColumnFooterSheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spCheckedNo_Sheet1.ColumnFooterSheetCornerStyle.Parent = "CornerDefault";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "제조번호";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "지시번호";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "순번";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "Scale바코드";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "제품코드";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "원료명";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "지시량";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 7).Value = "사용량";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 8).Value = "Tank 선택";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 9).Value = "바코드";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 10).Value = "확인";
            this.spCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 11).Value = "등록일시";
            this.spCheckedNo_Sheet1.ColumnHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spCheckedNo_Sheet1.ColumnHeader.DefaultStyle.Parent = "HeaderDefault";
            this.spCheckedNo_Sheet1.Columns.Get(0).Label = "제조번호";
            this.spCheckedNo_Sheet1.Columns.Get(0).Width = 122F;
            this.spCheckedNo_Sheet1.Columns.Get(1).Label = "지시번호";
            this.spCheckedNo_Sheet1.Columns.Get(1).Width = 100F;
            this.spCheckedNo_Sheet1.Columns.Get(2).Label = "순번";
            this.spCheckedNo_Sheet1.Columns.Get(2).Width = 80F;
            this.spCheckedNo_Sheet1.Columns.Get(3).Label = "Scale바코드";
            this.spCheckedNo_Sheet1.Columns.Get(3).Width = 100F;
            this.spCheckedNo_Sheet1.Columns.Get(4).Label = "제품코드";
            this.spCheckedNo_Sheet1.Columns.Get(4).Width = 90F;
            this.spCheckedNo_Sheet1.Columns.Get(5).Label = "원료명";
            this.spCheckedNo_Sheet1.Columns.Get(5).Width = 250F;
            this.spCheckedNo_Sheet1.Columns.Get(6).Label = "지시량";
            this.spCheckedNo_Sheet1.Columns.Get(6).Width = 100F;
            this.spCheckedNo_Sheet1.Columns.Get(7).Label = "사용량";
            this.spCheckedNo_Sheet1.Columns.Get(7).Width = 100F;
            this.spCheckedNo_Sheet1.Columns.Get(8).Label = "Tank 선택";
            this.spCheckedNo_Sheet1.Columns.Get(8).Width = 100F;
            this.spCheckedNo_Sheet1.Columns.Get(9).Label = "바코드";
            this.spCheckedNo_Sheet1.Columns.Get(9).Width = 80F;
            this.spCheckedNo_Sheet1.Columns.Get(10).Label = "확인";
            this.spCheckedNo_Sheet1.Columns.Get(10).Width = 80F;
            this.spCheckedNo_Sheet1.Columns.Get(11).Label = "등록일시";
            this.spCheckedNo_Sheet1.Columns.Get(11).Width = 200F;
            this.spCheckedNo_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spCheckedNo_Sheet1.RowHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spCheckedNo_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderDefault";
            this.spCheckedNo_Sheet1.SheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spCheckedNo_Sheet1.SheetCornerStyle.Parent = "CornerDefault";
            this.spCheckedNo_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // spNotCheckedNo
            // 
            this.spNotCheckedNo.AccessibleDescription = "spNotCheckedNo, Sheet1, Row 0, Column 0, ";
            this.spNotCheckedNo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spNotCheckedNo.FocusRenderer = defaultFocusIndicatorRenderer1;
            this.spNotCheckedNo.HorizontalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spNotCheckedNo.HorizontalScrollBar.Name = "";
            this.spNotCheckedNo.HorizontalScrollBar.Renderer = defaultScrollBarRenderer5;
            this.spNotCheckedNo.HorizontalScrollBar.TabIndex = 4;
            this.spNotCheckedNo.Location = new System.Drawing.Point(3, 3);
            this.spNotCheckedNo.Name = "spNotCheckedNo";
            this.spNotCheckedNo.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spNotCheckedNo_Sheet1});
            this.spNotCheckedNo.Size = new System.Drawing.Size(605, 256);
            this.spNotCheckedNo.Skin = FarPoint.Win.Spread.DefaultSpreadSkins.Classic;
            this.spNotCheckedNo.TabIndex = 2;
            this.spNotCheckedNo.VerticalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spNotCheckedNo.VerticalScrollBar.Name = "";
            this.spNotCheckedNo.VerticalScrollBar.Renderer = defaultScrollBarRenderer6;
            this.spNotCheckedNo.VerticalScrollBar.TabIndex = 5;
            this.spNotCheckedNo.VisualStyles = FarPoint.Win.VisualStyles.Off;
            this.spNotCheckedNo.Click += new System.EventHandler(this.spNotCheckedNo_Click);
            // 
            // spNotCheckedNo_Sheet1
            // 
            this.spNotCheckedNo_Sheet1.Reset();
            this.spNotCheckedNo_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spNotCheckedNo_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spNotCheckedNo_Sheet1.ColumnCount = 12;
            this.spNotCheckedNo_Sheet1.RowHeader.ColumnCount = 0;
            this.spNotCheckedNo_Sheet1.ColumnFooter.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spNotCheckedNo_Sheet1.ColumnFooter.DefaultStyle.Parent = "HeaderDefault";
            this.spNotCheckedNo_Sheet1.ColumnFooterSheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spNotCheckedNo_Sheet1.ColumnFooterSheetCornerStyle.Parent = "CornerDefault";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "제조번호";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "지시번호";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "순번";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "Scale바코드";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "제품코드";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "원료명";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "지시량";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 7).Value = "사용량";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 8).Value = "Tank 선택";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 9).Value = "바코드";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 10).Value = "확인";
            this.spNotCheckedNo_Sheet1.ColumnHeader.Cells.Get(0, 11).Value = "등록일시";
            this.spNotCheckedNo_Sheet1.ColumnHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spNotCheckedNo_Sheet1.ColumnHeader.DefaultStyle.Parent = "HeaderDefault";
            this.spNotCheckedNo_Sheet1.Columns.Get(0).Label = "제조번호";
            this.spNotCheckedNo_Sheet1.Columns.Get(0).Width = 122F;
            this.spNotCheckedNo_Sheet1.Columns.Get(1).Label = "지시번호";
            this.spNotCheckedNo_Sheet1.Columns.Get(1).Width = 100F;
            this.spNotCheckedNo_Sheet1.Columns.Get(2).Label = "순번";
            this.spNotCheckedNo_Sheet1.Columns.Get(2).Width = 80F;
            this.spNotCheckedNo_Sheet1.Columns.Get(3).Label = "Scale바코드";
            this.spNotCheckedNo_Sheet1.Columns.Get(3).Width = 100F;
            this.spNotCheckedNo_Sheet1.Columns.Get(4).Label = "제품코드";
            this.spNotCheckedNo_Sheet1.Columns.Get(4).Width = 90F;
            this.spNotCheckedNo_Sheet1.Columns.Get(5).Label = "원료명";
            this.spNotCheckedNo_Sheet1.Columns.Get(5).Width = 250F;
            this.spNotCheckedNo_Sheet1.Columns.Get(6).Label = "지시량";
            this.spNotCheckedNo_Sheet1.Columns.Get(6).Width = 100F;
            this.spNotCheckedNo_Sheet1.Columns.Get(7).Label = "사용량";
            this.spNotCheckedNo_Sheet1.Columns.Get(7).Width = 100F;
            this.spNotCheckedNo_Sheet1.Columns.Get(8).Label = "Tank 선택";
            this.spNotCheckedNo_Sheet1.Columns.Get(8).Width = 100F;
            this.spNotCheckedNo_Sheet1.Columns.Get(9).Label = "바코드";
            this.spNotCheckedNo_Sheet1.Columns.Get(9).Width = 80F;
            this.spNotCheckedNo_Sheet1.Columns.Get(10).Label = "확인";
            this.spNotCheckedNo_Sheet1.Columns.Get(10).Width = 80F;
            this.spNotCheckedNo_Sheet1.Columns.Get(11).Label = "등록일시";
            this.spNotCheckedNo_Sheet1.Columns.Get(11).Width = 200F;
            this.spNotCheckedNo_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spNotCheckedNo_Sheet1.RowHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spNotCheckedNo_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderDefault";
            this.spNotCheckedNo_Sheet1.SheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spNotCheckedNo_Sheet1.SheetCornerStyle.Parent = "CornerDefault";
            this.spNotCheckedNo_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(974, 538);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "생산2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.panel7, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.spOrderNo_2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel8, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 3);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.43609F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.46617F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.18797F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(968, 532);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.lblManufactureName_2nd);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.lblManufactureiTem_2nd);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.btnOrderNo_2);
            this.panel7.Controls.Add(this.lblManufactureNo_2nd);
            this.panel7.Controls.Add(this.TextBox6);
            this.panel7.Controls.Add(this.lblOrderNo_2nd);
            this.panel7.Controls.Add(this.label14);
            this.panel7.Controls.Add(this.label15);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(962, 69);
            this.panel7.TabIndex = 0;
            // 
            // lblManufactureName_2nd
            // 
            this.lblManufactureName_2nd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblManufactureName_2nd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManufactureName_2nd.Location = new System.Drawing.Point(539, 45);
            this.lblManufactureName_2nd.Name = "lblManufactureName_2nd";
            this.lblManufactureName_2nd.Size = new System.Drawing.Size(302, 19);
            this.lblManufactureName_2nd.TabIndex = 63;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(459, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 14);
            this.label4.TabIndex = 62;
            this.label4.Text = "제조이름 : ";
            // 
            // lblManufactureiTem_2nd
            // 
            this.lblManufactureiTem_2nd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblManufactureiTem_2nd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManufactureiTem_2nd.Location = new System.Drawing.Point(104, 45);
            this.lblManufactureiTem_2nd.Name = "lblManufactureiTem_2nd";
            this.lblManufactureiTem_2nd.Size = new System.Drawing.Size(302, 19);
            this.lblManufactureiTem_2nd.TabIndex = 61;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(23, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 14);
            this.label7.TabIndex = 60;
            this.label7.Text = "제조품목 : ";
            // 
            // btnOrderNo_2
            // 
            this.btnOrderNo_2.Font = new System.Drawing.Font("돋움", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrderNo_2.Location = new System.Drawing.Point(26, 3);
            this.btnOrderNo_2.Name = "btnOrderNo_2";
            this.btnOrderNo_2.Size = new System.Drawing.Size(73, 33);
            this.btnOrderNo_2.TabIndex = 55;
            this.btnOrderNo_2.Text = "지시번호";
            this.btnOrderNo_2.UseVisualStyleBackColor = true;
            this.btnOrderNo_2.Click += new System.EventHandler(this.btnOrderNo_2_Click);
            // 
            // lblManufactureNo_2nd
            // 
            this.lblManufactureNo_2nd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblManufactureNo_2nd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblManufactureNo_2nd.Location = new System.Drawing.Point(669, 13);
            this.lblManufactureNo_2nd.Name = "lblManufactureNo_2nd";
            this.lblManufactureNo_2nd.Size = new System.Drawing.Size(172, 19);
            this.lblManufactureNo_2nd.TabIndex = 59;
            // 
            // TextBox6
            // 
            this.TextBox6.Font = new System.Drawing.Font("Microsoft YaHei", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox6.Location = new System.Drawing.Point(116, 7);
            this.TextBox6.Name = "TextBox6";
            this.TextBox6.Size = new System.Drawing.Size(190, 25);
            this.TextBox6.TabIndex = 54;
            this.TextBox6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox6_KeyDown);
            this.TextBox6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox6_KeyPress);
            // 
            // lblOrderNo_2nd
            // 
            this.lblOrderNo_2nd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.lblOrderNo_2nd.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrderNo_2nd.Location = new System.Drawing.Point(406, 13);
            this.lblOrderNo_2nd.Name = "lblOrderNo_2nd";
            this.lblOrderNo_2nd.Size = new System.Drawing.Size(172, 19);
            this.lblOrderNo_2nd.TabIndex = 58;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(593, 13);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 14);
            this.label14.TabIndex = 57;
            this.label14.Text = "제조번호 : ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(330, 13);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 14);
            this.label15.TabIndex = 56;
            this.label15.Text = "지시번호 : ";
            // 
            // spOrderNo_2
            // 
            this.spOrderNo_2.AccessibleDescription = "spOrderNo_2, Sheet1, Row 0, Column 0, ";
            this.spOrderNo_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spOrderNo_2.FocusRenderer = defaultFocusIndicatorRenderer1;
            this.spOrderNo_2.HorizontalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spOrderNo_2.HorizontalScrollBar.Name = "";
            this.spOrderNo_2.HorizontalScrollBar.Renderer = defaultScrollBarRenderer7;
            this.spOrderNo_2.HorizontalScrollBar.TabIndex = 6;
            this.spOrderNo_2.Location = new System.Drawing.Point(3, 78);
            this.spOrderNo_2.Name = "spOrderNo_2";
            this.spOrderNo_2.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spOrderNo_2_Sheet1});
            this.spOrderNo_2.Size = new System.Drawing.Size(962, 123);
            this.spOrderNo_2.Skin = FarPoint.Win.Spread.DefaultSpreadSkins.Classic;
            this.spOrderNo_2.TabIndex = 1;
            this.spOrderNo_2.VerticalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spOrderNo_2.VerticalScrollBar.Name = "";
            this.spOrderNo_2.VerticalScrollBar.Renderer = defaultScrollBarRenderer8;
            this.spOrderNo_2.VerticalScrollBar.TabIndex = 7;
            this.spOrderNo_2.VisualStyles = FarPoint.Win.VisualStyles.Off;
            this.spOrderNo_2.Click += new System.EventHandler(this.spOrderNo_2_Click);
            // 
            // spOrderNo_2_Sheet1
            // 
            this.spOrderNo_2_Sheet1.Reset();
            this.spOrderNo_2_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spOrderNo_2_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spOrderNo_2_Sheet1.ColumnCount = 11;
            this.spOrderNo_2_Sheet1.RowHeader.ColumnCount = 0;
            this.spOrderNo_2_Sheet1.ColumnFooter.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spOrderNo_2_Sheet1.ColumnFooter.DefaultStyle.Parent = "HeaderDefault";
            this.spOrderNo_2_Sheet1.ColumnFooterSheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spOrderNo_2_Sheet1.ColumnFooterSheetCornerStyle.Parent = "CornerDefault";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "지시번호";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "제조번호";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "BulkName";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "제조품목";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "제조실";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "제조명";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "제조량";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 7).Value = "제조일시";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 8).Value = "사용설비";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 9).Value = "Check";
            this.spOrderNo_2_Sheet1.ColumnHeader.Cells.Get(0, 10).Value = "사용자";
            this.spOrderNo_2_Sheet1.ColumnHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spOrderNo_2_Sheet1.ColumnHeader.DefaultStyle.Parent = "HeaderDefault";
            this.spOrderNo_2_Sheet1.Columns.Get(0).Label = "지시번호";
            this.spOrderNo_2_Sheet1.Columns.Get(0).Width = 122F;
            this.spOrderNo_2_Sheet1.Columns.Get(1).Label = "제조번호";
            this.spOrderNo_2_Sheet1.Columns.Get(1).Width = 100F;
            this.spOrderNo_2_Sheet1.Columns.Get(2).Label = "BulkName";
            this.spOrderNo_2_Sheet1.Columns.Get(2).Width = 127F;
            this.spOrderNo_2_Sheet1.Columns.Get(3).Label = "제조품목";
            this.spOrderNo_2_Sheet1.Columns.Get(3).Width = 100F;
            this.spOrderNo_2_Sheet1.Columns.Get(4).Label = "제조실";
            this.spOrderNo_2_Sheet1.Columns.Get(4).Width = 90F;
            this.spOrderNo_2_Sheet1.Columns.Get(5).Label = "제조명";
            this.spOrderNo_2_Sheet1.Columns.Get(5).Width = 300F;
            this.spOrderNo_2_Sheet1.Columns.Get(6).Label = "제조량";
            this.spOrderNo_2_Sheet1.Columns.Get(6).Width = 100F;
            this.spOrderNo_2_Sheet1.Columns.Get(7).Label = "제조일시";
            this.spOrderNo_2_Sheet1.Columns.Get(7).Width = 200F;
            this.spOrderNo_2_Sheet1.Columns.Get(8).Label = "사용설비";
            this.spOrderNo_2_Sheet1.Columns.Get(8).Width = 100F;
            this.spOrderNo_2_Sheet1.Columns.Get(9).Label = "Check";
            this.spOrderNo_2_Sheet1.Columns.Get(9).Width = 80F;
            this.spOrderNo_2_Sheet1.Columns.Get(10).Label = "사용자";
            this.spOrderNo_2_Sheet1.Columns.Get(10).Width = 80F;
            this.spOrderNo_2_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spOrderNo_2_Sheet1.RowHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spOrderNo_2_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderDefault";
            this.spOrderNo_2_Sheet1.SheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spOrderNo_2_Sheet1.SheetCornerStyle.Parent = "CornerDefault";
            this.spOrderNo_2_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.lblUnCheckedCNT2);
            this.panel8.Controls.Add(this.label17);
            this.panel8.Controls.Add(this.TextBox5);
            this.panel8.Controls.Add(this.button5);
            this.panel8.Controls.Add(this.TextBox3);
            this.panel8.Controls.Add(this.button6);
            this.panel8.Controls.Add(this.lblBarCode_2nd);
            this.panel8.Controls.Add(this.label19);
            this.panel8.Controls.Add(this.label26);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(3, 207);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(962, 54);
            this.panel8.TabIndex = 2;
            // 
            // lblUnCheckedCNT2
            // 
            this.lblUnCheckedCNT2.AutoSize = true;
            this.lblUnCheckedCNT2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnCheckedCNT2.Location = new System.Drawing.Point(110, 3);
            this.lblUnCheckedCNT2.Name = "lblUnCheckedCNT2";
            this.lblUnCheckedCNT2.Size = new System.Drawing.Size(13, 14);
            this.lblUnCheckedCNT2.TabIndex = 70;
            this.lblUnCheckedCNT2.Text = "0";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(9, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(95, 14);
            this.label17.TabIndex = 69;
            this.label17.Text = "Check안된수량 : ";
            // 
            // TextBox5
            // 
            this.TextBox5.Location = new System.Drawing.Point(103, 24);
            this.TextBox5.Name = "TextBox5";
            this.TextBox5.Size = new System.Drawing.Size(167, 21);
            this.TextBox5.TabIndex = 68;
            this.TextBox5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox5_KeyDown);
            this.TextBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox5_KeyPress);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(6, 22);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(91, 30);
            this.button5.TabIndex = 67;
            this.button5.Text = "투입탱크";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(479, 24);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.Size = new System.Drawing.Size(210, 21);
            this.TextBox3.TabIndex = 63;
            this.TextBox3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBox3_KeyDown);
            this.TextBox3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextBox3_KeyPress);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(309, 22);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(98, 30);
            this.button6.TabIndex = 66;
            this.button6.Text = "원료바코드";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // lblBarCode_2nd
            // 
            this.lblBarCode_2nd.AutoSize = true;
            this.lblBarCode_2nd.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblBarCode_2nd.Location = new System.Drawing.Point(702, 28);
            this.lblBarCode_2nd.Name = "lblBarCode_2nd";
            this.lblBarCode_2nd.Size = new System.Drawing.Size(71, 14);
            this.lblBarCode_2nd.TabIndex = 65;
            this.lblBarCode_2nd.Text = "BarCode";
            this.lblBarCode_2nd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("돋움", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.Location = new System.Drawing.Point(413, 27);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 14);
            this.label19.TabIndex = 64;
            this.label19.Text = "바코드 : ";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft YaHei", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(828, 15);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(96, 30);
            this.label26.TabIndex = 62;
            this.label26.Text = "label26";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.61746F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.38254F));
            this.tableLayoutPanel4.Controls.Add(this.spCheckedNo_2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.spNotCheckedNo_2, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 267);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(962, 262);
            this.tableLayoutPanel4.TabIndex = 3;
            // 
            // spCheckedNo_2
            // 
            this.spCheckedNo_2.AccessibleDescription = "spCheckedNo_2, Sheet1, Row 0, Column 0, ";
            this.spCheckedNo_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spCheckedNo_2.FocusRenderer = defaultFocusIndicatorRenderer1;
            this.spCheckedNo_2.HorizontalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spCheckedNo_2.HorizontalScrollBar.Name = "";
            this.spCheckedNo_2.HorizontalScrollBar.Renderer = defaultScrollBarRenderer9;
            this.spCheckedNo_2.HorizontalScrollBar.TabIndex = 4;
            this.spCheckedNo_2.Location = new System.Drawing.Point(614, 3);
            this.spCheckedNo_2.Name = "spCheckedNo_2";
            this.spCheckedNo_2.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spCheckedNo_2_Sheet1});
            this.spCheckedNo_2.Size = new System.Drawing.Size(345, 256);
            this.spCheckedNo_2.Skin = FarPoint.Win.Spread.DefaultSpreadSkins.Classic;
            this.spCheckedNo_2.TabIndex = 3;
            this.spCheckedNo_2.VerticalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spCheckedNo_2.VerticalScrollBar.Name = "";
            this.spCheckedNo_2.VerticalScrollBar.Renderer = defaultScrollBarRenderer10;
            this.spCheckedNo_2.VerticalScrollBar.TabIndex = 5;
            this.spCheckedNo_2.VisualStyles = FarPoint.Win.VisualStyles.Off;
            // 
            // spCheckedNo_2_Sheet1
            // 
            this.spCheckedNo_2_Sheet1.Reset();
            this.spCheckedNo_2_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spCheckedNo_2_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spCheckedNo_2_Sheet1.ColumnCount = 12;
            this.spCheckedNo_2_Sheet1.RowHeader.ColumnCount = 0;
            this.spCheckedNo_2_Sheet1.ColumnFooter.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spCheckedNo_2_Sheet1.ColumnFooter.DefaultStyle.Parent = "HeaderDefault";
            this.spCheckedNo_2_Sheet1.ColumnFooterSheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spCheckedNo_2_Sheet1.ColumnFooterSheetCornerStyle.Parent = "CornerDefault";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "제조번호";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "지시번호";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "순번";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "Scale바코드";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "제품코드";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "원료명";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "지시량";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 7).Value = "사용량";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 8).Value = "Tank 선택";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 9).Value = "바코드";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 10).Value = "확인";
            this.spCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 11).Value = "등록일시";
            this.spCheckedNo_2_Sheet1.ColumnHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spCheckedNo_2_Sheet1.ColumnHeader.DefaultStyle.Parent = "HeaderDefault";
            this.spCheckedNo_2_Sheet1.Columns.Get(0).Label = "제조번호";
            this.spCheckedNo_2_Sheet1.Columns.Get(0).Width = 122F;
            this.spCheckedNo_2_Sheet1.Columns.Get(1).Label = "지시번호";
            this.spCheckedNo_2_Sheet1.Columns.Get(1).Width = 100F;
            this.spCheckedNo_2_Sheet1.Columns.Get(2).Label = "순번";
            this.spCheckedNo_2_Sheet1.Columns.Get(2).Width = 80F;
            this.spCheckedNo_2_Sheet1.Columns.Get(3).Label = "Scale바코드";
            this.spCheckedNo_2_Sheet1.Columns.Get(3).Width = 100F;
            this.spCheckedNo_2_Sheet1.Columns.Get(4).Label = "제품코드";
            this.spCheckedNo_2_Sheet1.Columns.Get(4).Width = 90F;
            this.spCheckedNo_2_Sheet1.Columns.Get(5).Label = "원료명";
            this.spCheckedNo_2_Sheet1.Columns.Get(5).Width = 250F;
            this.spCheckedNo_2_Sheet1.Columns.Get(6).Label = "지시량";
            this.spCheckedNo_2_Sheet1.Columns.Get(6).Width = 100F;
            this.spCheckedNo_2_Sheet1.Columns.Get(7).Label = "사용량";
            this.spCheckedNo_2_Sheet1.Columns.Get(7).Width = 100F;
            this.spCheckedNo_2_Sheet1.Columns.Get(8).Label = "Tank 선택";
            this.spCheckedNo_2_Sheet1.Columns.Get(8).Width = 100F;
            this.spCheckedNo_2_Sheet1.Columns.Get(9).Label = "바코드";
            this.spCheckedNo_2_Sheet1.Columns.Get(9).Width = 80F;
            this.spCheckedNo_2_Sheet1.Columns.Get(10).Label = "확인";
            this.spCheckedNo_2_Sheet1.Columns.Get(10).Width = 80F;
            this.spCheckedNo_2_Sheet1.Columns.Get(11).Label = "등록일시";
            this.spCheckedNo_2_Sheet1.Columns.Get(11).Width = 200F;
            this.spCheckedNo_2_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spCheckedNo_2_Sheet1.RowHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spCheckedNo_2_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderDefault";
            this.spCheckedNo_2_Sheet1.SheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spCheckedNo_2_Sheet1.SheetCornerStyle.Parent = "CornerDefault";
            this.spCheckedNo_2_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // spNotCheckedNo_2
            // 
            this.spNotCheckedNo_2.AccessibleDescription = "spNotCheckedNo_2, Sheet1, Row 0, Column 0, ";
            this.spNotCheckedNo_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spNotCheckedNo_2.FocusRenderer = defaultFocusIndicatorRenderer1;
            this.spNotCheckedNo_2.HorizontalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spNotCheckedNo_2.HorizontalScrollBar.Name = "";
            this.spNotCheckedNo_2.HorizontalScrollBar.Renderer = defaultScrollBarRenderer11;
            this.spNotCheckedNo_2.HorizontalScrollBar.TabIndex = 4;
            this.spNotCheckedNo_2.Location = new System.Drawing.Point(3, 3);
            this.spNotCheckedNo_2.Name = "spNotCheckedNo_2";
            this.spNotCheckedNo_2.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spNotCheckedNo_2_Sheet1});
            this.spNotCheckedNo_2.Size = new System.Drawing.Size(605, 256);
            this.spNotCheckedNo_2.Skin = FarPoint.Win.Spread.DefaultSpreadSkins.Classic;
            this.spNotCheckedNo_2.TabIndex = 2;
            this.spNotCheckedNo_2.VerticalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spNotCheckedNo_2.VerticalScrollBar.Name = "";
            this.spNotCheckedNo_2.VerticalScrollBar.Renderer = defaultScrollBarRenderer12;
            this.spNotCheckedNo_2.VerticalScrollBar.TabIndex = 5;
            this.spNotCheckedNo_2.VisualStyles = FarPoint.Win.VisualStyles.Off;
            // 
            // spNotCheckedNo_2_Sheet1
            // 
            this.spNotCheckedNo_2_Sheet1.Reset();
            this.spNotCheckedNo_2_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spNotCheckedNo_2_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spNotCheckedNo_2_Sheet1.ColumnCount = 12;
            this.spNotCheckedNo_2_Sheet1.RowHeader.ColumnCount = 0;
            this.spNotCheckedNo_2_Sheet1.ColumnFooter.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spNotCheckedNo_2_Sheet1.ColumnFooter.DefaultStyle.Parent = "HeaderDefault";
            this.spNotCheckedNo_2_Sheet1.ColumnFooterSheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spNotCheckedNo_2_Sheet1.ColumnFooterSheetCornerStyle.Parent = "CornerDefault";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "제조번호";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "지시번호";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "순번";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "Scale바코드";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "제품코드";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "원료명";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "지시량";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 7).Value = "사용량";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 8).Value = "Tank 선택";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 9).Value = "바코드";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 10).Value = "확인";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.Cells.Get(0, 11).Value = "등록일시";
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spNotCheckedNo_2_Sheet1.ColumnHeader.DefaultStyle.Parent = "HeaderDefault";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(0).Label = "제조번호";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(0).Width = 122F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(1).Label = "지시번호";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(1).Width = 100F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(2).Label = "순번";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(2).Width = 80F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(3).Label = "Scale바코드";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(3).Width = 100F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(4).Label = "제품코드";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(4).Width = 90F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(5).Label = "원료명";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(5).Width = 250F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(6).Label = "지시량";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(6).Width = 100F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(7).Label = "사용량";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(7).Width = 100F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(8).Label = "Tank 선택";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(8).Width = 100F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(9).Label = "바코드";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(9).Width = 80F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(10).Label = "확인";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(10).Width = 80F;
            this.spNotCheckedNo_2_Sheet1.Columns.Get(11).Label = "등록일시";
            this.spNotCheckedNo_2_Sheet1.Columns.Get(11).Width = 200F;
            this.spNotCheckedNo_2_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spNotCheckedNo_2_Sheet1.RowHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spNotCheckedNo_2_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderDefault";
            this.spNotCheckedNo_2_Sheet1.SheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spNotCheckedNo_2_Sheet1.SheetCornerStyle.Parent = "CornerDefault";
            this.spNotCheckedNo_2_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel6);
            this.tabPage3.Controls.Add(this.panel5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(974, 538);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "---";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.spSpread);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(974, 490);
            this.panel6.TabIndex = 1;
            // 
            // spSpread
            // 
            this.spSpread.AccessibleDescription = "shSpread1, Sheet1, Row 0, Column 0, ";
            this.spSpread.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spSpread.FocusRenderer = defaultFocusIndicatorRenderer1;
            this.spSpread.HorizontalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spSpread.HorizontalScrollBar.Name = "";
            this.spSpread.HorizontalScrollBar.Renderer = defaultScrollBarRenderer13;
            this.spSpread.HorizontalScrollBar.TabIndex = 4;
            this.spSpread.Location = new System.Drawing.Point(0, 0);
            this.spSpread.Name = "spSpread";
            this.spSpread.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.spSpread_Sheet1});
            this.spSpread.Size = new System.Drawing.Size(974, 490);
            this.spSpread.Skin = FarPoint.Win.Spread.DefaultSpreadSkins.Classic;
            this.spSpread.TabIndex = 0;
            this.spSpread.VerticalScrollBar.Buttons = new FarPoint.Win.Spread.FpScrollBarButtonCollection("BackwardLineButton,ThumbTrack,ForwardLineButton");
            this.spSpread.VerticalScrollBar.Name = "";
            this.spSpread.VerticalScrollBar.Renderer = defaultScrollBarRenderer14;
            this.spSpread.VerticalScrollBar.TabIndex = 5;
            this.spSpread.VisualStyles = FarPoint.Win.VisualStyles.Off;
            // 
            // spSpread_Sheet1
            // 
            this.spSpread_Sheet1.Reset();
            this.spSpread_Sheet1.SheetName = "Sheet1";
            // Formulas and custom names must be loaded with R1C1 reference style
            this.spSpread_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.R1C1;
            this.spSpread_Sheet1.ColumnCount = 8;
            this.spSpread_Sheet1.RowHeader.ColumnCount = 0;
            this.spSpread_Sheet1.ColumnFooter.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spSpread_Sheet1.ColumnFooter.DefaultStyle.Parent = "HeaderDefault";
            this.spSpread_Sheet1.ColumnFooterSheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spSpread_Sheet1.ColumnFooterSheetCornerStyle.Parent = "CornerDefault";
            this.spSpread_Sheet1.ColumnHeader.Cells.Get(0, 0).Value = "지시번호";
            this.spSpread_Sheet1.ColumnHeader.Cells.Get(0, 1).Value = "순번";
            this.spSpread_Sheet1.ColumnHeader.Cells.Get(0, 2).Value = "ScaleBarcode";
            this.spSpread_Sheet1.ColumnHeader.Cells.Get(0, 3).Value = "LineNumber";
            this.spSpread_Sheet1.ColumnHeader.Cells.Get(0, 4).Value = "MaterialCode";
            this.spSpread_Sheet1.ColumnHeader.Cells.Get(0, 5).Value = "MetrialName";
            this.spSpread_Sheet1.ColumnHeader.Cells.Get(0, 6).Value = "Capacity";
            this.spSpread_Sheet1.ColumnHeader.Cells.Get(0, 7).Value = "ScaleCapacity";
            this.spSpread_Sheet1.ColumnHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spSpread_Sheet1.ColumnHeader.DefaultStyle.Parent = "HeaderDefault";
            this.spSpread_Sheet1.Columns.Get(0).Label = "지시번호";
            this.spSpread_Sheet1.Columns.Get(0).Width = 131F;
            this.spSpread_Sheet1.Columns.Get(1).Label = "순번";
            this.spSpread_Sheet1.Columns.Get(1).Width = 86F;
            this.spSpread_Sheet1.Columns.Get(2).Label = "ScaleBarcode";
            this.spSpread_Sheet1.Columns.Get(2).Width = 126F;
            this.spSpread_Sheet1.Columns.Get(3).Label = "LineNumber";
            this.spSpread_Sheet1.Columns.Get(3).Width = 107F;
            this.spSpread_Sheet1.Columns.Get(4).Label = "MaterialCode";
            this.spSpread_Sheet1.Columns.Get(4).Width = 144F;
            this.spSpread_Sheet1.Columns.Get(5).Label = "MetrialName";
            this.spSpread_Sheet1.Columns.Get(5).Width = 181F;
            this.spSpread_Sheet1.Columns.Get(6).Label = "Capacity";
            this.spSpread_Sheet1.Columns.Get(6).Width = 75F;
            this.spSpread_Sheet1.Columns.Get(7).Label = "ScaleCapacity";
            this.spSpread_Sheet1.Columns.Get(7).Width = 144F;
            this.spSpread_Sheet1.RowHeader.Columns.Default.Resizable = false;
            this.spSpread_Sheet1.RowHeader.DefaultStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spSpread_Sheet1.RowHeader.DefaultStyle.Parent = "RowHeaderDefault";
            this.spSpread_Sheet1.SheetCornerStyle.NoteIndicatorColor = System.Drawing.Color.Red;
            this.spSpread_Sheet1.SheetCornerStyle.Parent = "CornerDefault";
            this.spSpread_Sheet1.ReferenceStyle = FarPoint.Win.Spread.Model.ReferenceStyle.A1;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblPWBarCode_2nd);
            this.panel5.Controls.Add(this.lblPWBarCode_1st);
            this.panel5.Controls.Add(this.lblETHANOLBarCode_2nd);
            this.panel5.Controls.Add(this.lblETHANOLBarCode_1st);
            this.panel5.Controls.Add(this.lblSILICONBarCode_2nd);
            this.panel5.Controls.Add(this.lblSILICONBarCode_1st);
            this.panel5.Controls.Add(this.lblLILYBarCode_2nd);
            this.panel5.Controls.Add(this.lblLILYBarCode_1st);
            this.panel5.Controls.Add(this.lbl13BGBarCode_2nd);
            this.panel5.Controls.Add(this.lbl13BGBarCode_1st);
            this.panel5.Controls.Add(this.lblGlycerineBarCode_2nd);
            this.panel5.Controls.Add(this.lblGlycerineBarCode_1st);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 490);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(974, 48);
            this.panel5.TabIndex = 0;
            // 
            // lblPWBarCode_2nd
            // 
            this.lblPWBarCode_2nd.AutoSize = true;
            this.lblPWBarCode_2nd.Location = new System.Drawing.Point(843, 26);
            this.lblPWBarCode_2nd.Name = "lblPWBarCode_2nd";
            this.lblPWBarCode_2nd.Size = new System.Drawing.Size(111, 12);
            this.lblPWBarCode_2nd.TabIndex = 79;
            this.lblPWBarCode_2nd.Text = "lblPWBarCode_2nd";
            // 
            // lblPWBarCode_1st
            // 
            this.lblPWBarCode_1st.AutoSize = true;
            this.lblPWBarCode_1st.Location = new System.Drawing.Point(843, 10);
            this.lblPWBarCode_1st.Name = "lblPWBarCode_1st";
            this.lblPWBarCode_1st.Size = new System.Drawing.Size(107, 12);
            this.lblPWBarCode_1st.TabIndex = 78;
            this.lblPWBarCode_1st.Text = "lblPWBarCode_1st";
            // 
            // lblETHANOLBarCode_2nd
            // 
            this.lblETHANOLBarCode_2nd.AutoSize = true;
            this.lblETHANOLBarCode_2nd.Location = new System.Drawing.Point(664, 26);
            this.lblETHANOLBarCode_2nd.Name = "lblETHANOLBarCode_2nd";
            this.lblETHANOLBarCode_2nd.Size = new System.Drawing.Size(150, 12);
            this.lblETHANOLBarCode_2nd.TabIndex = 77;
            this.lblETHANOLBarCode_2nd.Text = "lblETHANOLBarCode_2nd";
            // 
            // lblETHANOLBarCode_1st
            // 
            this.lblETHANOLBarCode_1st.AutoSize = true;
            this.lblETHANOLBarCode_1st.Location = new System.Drawing.Point(664, 10);
            this.lblETHANOLBarCode_1st.Name = "lblETHANOLBarCode_1st";
            this.lblETHANOLBarCode_1st.Size = new System.Drawing.Size(146, 12);
            this.lblETHANOLBarCode_1st.TabIndex = 76;
            this.lblETHANOLBarCode_1st.Text = "lblETHANOLBarCode_1st";
            // 
            // lblSILICONBarCode_2nd
            // 
            this.lblSILICONBarCode_2nd.AutoSize = true;
            this.lblSILICONBarCode_2nd.Location = new System.Drawing.Point(493, 26);
            this.lblSILICONBarCode_2nd.Name = "lblSILICONBarCode_2nd";
            this.lblSILICONBarCode_2nd.Size = new System.Drawing.Size(141, 12);
            this.lblSILICONBarCode_2nd.TabIndex = 75;
            this.lblSILICONBarCode_2nd.Text = "lblSILICONBarCode_2nd";
            // 
            // lblSILICONBarCode_1st
            // 
            this.lblSILICONBarCode_1st.AutoSize = true;
            this.lblSILICONBarCode_1st.Location = new System.Drawing.Point(493, 10);
            this.lblSILICONBarCode_1st.Name = "lblSILICONBarCode_1st";
            this.lblSILICONBarCode_1st.Size = new System.Drawing.Size(137, 12);
            this.lblSILICONBarCode_1st.TabIndex = 74;
            this.lblSILICONBarCode_1st.Text = "lblSILICONBarCode_1st";
            // 
            // lblLILYBarCode_2nd
            // 
            this.lblLILYBarCode_2nd.AutoSize = true;
            this.lblLILYBarCode_2nd.Location = new System.Drawing.Point(351, 26);
            this.lblLILYBarCode_2nd.Name = "lblLILYBarCode_2nd";
            this.lblLILYBarCode_2nd.Size = new System.Drawing.Size(118, 12);
            this.lblLILYBarCode_2nd.TabIndex = 73;
            this.lblLILYBarCode_2nd.Text = "lblLILYBarCode_2nd";
            // 
            // lblLILYBarCode_1st
            // 
            this.lblLILYBarCode_1st.AutoSize = true;
            this.lblLILYBarCode_1st.Location = new System.Drawing.Point(351, 10);
            this.lblLILYBarCode_1st.Name = "lblLILYBarCode_1st";
            this.lblLILYBarCode_1st.Size = new System.Drawing.Size(114, 12);
            this.lblLILYBarCode_1st.TabIndex = 72;
            this.lblLILYBarCode_1st.Text = "lblLILYBarCode_1st";
            // 
            // lbl13BGBarCode_2nd
            // 
            this.lbl13BGBarCode_2nd.AutoSize = true;
            this.lbl13BGBarCode_2nd.Location = new System.Drawing.Point(199, 26);
            this.lbl13BGBarCode_2nd.Name = "lbl13BGBarCode_2nd";
            this.lbl13BGBarCode_2nd.Size = new System.Drawing.Size(122, 12);
            this.lbl13BGBarCode_2nd.TabIndex = 71;
            this.lbl13BGBarCode_2nd.Text = "lbl13BGBarCode_2nd";
            // 
            // lbl13BGBarCode_1st
            // 
            this.lbl13BGBarCode_1st.AutoSize = true;
            this.lbl13BGBarCode_1st.Location = new System.Drawing.Point(199, 10);
            this.lbl13BGBarCode_1st.Name = "lbl13BGBarCode_1st";
            this.lbl13BGBarCode_1st.Size = new System.Drawing.Size(118, 12);
            this.lbl13BGBarCode_1st.TabIndex = 70;
            this.lbl13BGBarCode_1st.Text = "lbl13BGBarCode_1st";
            // 
            // lblGlycerineBarCode_2nd
            // 
            this.lblGlycerineBarCode_2nd.AutoSize = true;
            this.lblGlycerineBarCode_2nd.Location = new System.Drawing.Point(20, 26);
            this.lblGlycerineBarCode_2nd.Name = "lblGlycerineBarCode_2nd";
            this.lblGlycerineBarCode_2nd.Size = new System.Drawing.Size(147, 12);
            this.lblGlycerineBarCode_2nd.TabIndex = 69;
            this.lblGlycerineBarCode_2nd.Text = "lblGlycerineBarCode_2nd";
            // 
            // lblGlycerineBarCode_1st
            // 
            this.lblGlycerineBarCode_1st.AutoSize = true;
            this.lblGlycerineBarCode_1st.Location = new System.Drawing.Point(20, 10);
            this.lblGlycerineBarCode_1st.Name = "lblGlycerineBarCode_1st";
            this.lblGlycerineBarCode_1st.Size = new System.Drawing.Size(143, 12);
            this.lblGlycerineBarCode_1st.TabIndex = 68;
            this.lblGlycerineBarCode_1st.Text = "lblGlycerineBarCode_1st";
            // 
            // tabPage4
            // 
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(974, 538);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "---";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 608);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Main";
            this.Text = "Operation";
            this.Load += new System.EventHandler(this.Main_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spOrderNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spOrderNo_Sheet1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spCheckedNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spCheckedNo_Sheet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spNotCheckedNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spNotCheckedNo_Sheet1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spOrderNo_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spOrderNo_2_Sheet1)).EndInit();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spCheckedNo_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spCheckedNo_2_Sheet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spNotCheckedNo_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spNotCheckedNo_2_Sheet1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spSpread)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spSpread_Sheet1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
        internal System.Windows.Forms.Label lblManufactureName_1st;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.Label lblManufactureiTem_1st;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Button btnOrderNo_1;
        internal System.Windows.Forms.Label lblManufactureNo_1st;
        internal System.Windows.Forms.TextBox TextBox1;
        internal System.Windows.Forms.Label lblOrderNo_1st;
        internal System.Windows.Forms.Label label31;
        internal System.Windows.Forms.Label Label30;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private shSpread spOrderNo;
        private FarPoint.Win.Spread.SheetView spOrderNo_Sheet1;
        private System.Windows.Forms.Panel panel4;
        internal System.Windows.Forms.Label lblUnCheckedCNT;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.TextBox TextBox4;
        internal System.Windows.Forms.Button Button4;
        internal System.Windows.Forms.TextBox TextBox2;
        internal System.Windows.Forms.Button button2;
        internal System.Windows.Forms.Label lblBarCode_1st;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.Label Label10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private shSpread spCheckedNo;
        private FarPoint.Win.Spread.SheetView spCheckedNo_Sheet1;
        private shSpread spNotCheckedNo;
        private FarPoint.Win.Spread.SheetView spNotCheckedNo_Sheet1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        internal System.Windows.Forms.Label lblPWBarCode_2nd;
        internal System.Windows.Forms.Label lblPWBarCode_1st;
        internal System.Windows.Forms.Label lblETHANOLBarCode_2nd;
        internal System.Windows.Forms.Label lblETHANOLBarCode_1st;
        internal System.Windows.Forms.Label lblSILICONBarCode_2nd;
        internal System.Windows.Forms.Label lblSILICONBarCode_1st;
        internal System.Windows.Forms.Label lblLILYBarCode_2nd;
        internal System.Windows.Forms.Label lblLILYBarCode_1st;
        internal System.Windows.Forms.Label lbl13BGBarCode_2nd;
        internal System.Windows.Forms.Label lbl13BGBarCode_1st;
        internal System.Windows.Forms.Label lblGlycerineBarCode_2nd;
        internal System.Windows.Forms.Label lblGlycerineBarCode_1st;
        private shSpread spSpread;
        private FarPoint.Win.Spread.SheetView spSpread_Sheet1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel7;
        internal System.Windows.Forms.Label lblManufactureName_2nd;
        internal System.Windows.Forms.Label label4;
        internal System.Windows.Forms.Label lblManufactureiTem_2nd;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Button btnOrderNo_2;
        internal System.Windows.Forms.Label lblManufactureNo_2nd;
        internal System.Windows.Forms.TextBox TextBox6;
        internal System.Windows.Forms.Label lblOrderNo_2nd;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label label15;
        private shSpread spOrderNo_2;
        private FarPoint.Win.Spread.SheetView spOrderNo_2_Sheet1;
        private System.Windows.Forms.Panel panel8;
        internal System.Windows.Forms.Label lblUnCheckedCNT2;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.TextBox TextBox5;
        internal System.Windows.Forms.Button button5;
        internal System.Windows.Forms.TextBox TextBox3;
        internal System.Windows.Forms.Button button6;
        internal System.Windows.Forms.Label lblBarCode_2nd;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private shSpread spCheckedNo_2;
        private FarPoint.Win.Spread.SheetView spCheckedNo_2_Sheet1;
        private shSpread spNotCheckedNo_2;
        private FarPoint.Win.Spread.SheetView spNotCheckedNo_2_Sheet1;
    }
}

