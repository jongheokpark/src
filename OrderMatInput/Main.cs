﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FarPoint.Win.Spread;
using System.Diagnostics;
using System.Configuration;
using Core;

namespace OrderMatInput
{
    public partial class Main : Form
    {
        #region 전역변수
        private string OrderNo_1st = string.Empty;
        private string ManufactureNo_1st = string.Empty;
        private string ManufactureiTem_1st = string.Empty;
        private string ManufactureName_1st = string.Empty;
        private string OrderNo_2nd = string.Empty;
        private string ManufactureNo_2nd = string.Empty;
        private string ManufactureiTem_2nd = string.Empty;
        private string ManufactureName_2nd = string.Empty;
        private int INPUT_SEQNO = 0;
        private string GLYCERINE_MCODE = "6000386";
        private string _13BG_MCODE = "6007200";
        private string SILICON_MCODE = "6001164";
        private string PW_MCODE = "6001221";
        private string ETHANOL_MCODE = "6000374";
        private string LILY_MCODE = "6001697";
        private string MixerNo = "1000(7)";
        private string[] Glycerine_OrderQty = new string[1];
        private string[] _13BG_OrderQty = new string[1];
        private string[] Silicon_OrderQty = new string[1];
        private string[] Ethanol_OrderQty = new string[1];
        private string[] Lily_OrderQty = new string[1];
        private string[] PW_OrderQty = new string[1];
        private string ScanBarCode = string.Empty; // 스캔 바코드
        private StringBuilder TempScanBarcode = new StringBuilder();    // 임시 키보드값, 바코드위해사용 (Enter) 시 초기화

        QueryResult qr = new QueryResult();
        CommonCode common = new CommonCode();
        
        #endregion

        #region 생성자
        public Main()
        {
            InitializeComponent();
        }

        enum spOrderNoCol
        {
            WO_NO,
            PROD_NO,
            BULK_NAME,
            PROD_CODE,
            PROD_ROOM,
            PROD_NAME,
            PROD_QTY,
            PROD_DATE,
            USE_EQUIP,
            CHECK,
            USER
        }

        enum spNotCheckedNoCol
        {
            PROD_NO,
            WO_NO,
            SEQ,
            SCALE_BARCODE,
            PROD_CODE,
            PROD_NAME,
            ORDER_QTY,
            PROD_QTY,
            TANK_NAME,
            BARCODE,
            CHECK,
            CRT_DATE
        }

        enum spCheckedNoCol
        {
            PROD_NO,
            WO_NO,
            SEQ,
            SCALE_BARCODE,
            PROD_CODE,
            PROD_NAME,
            ORDER_QTY,
            PROD_QTY,
            TANK_NAME,
            BARCODE,
            CHECK,
            CRT_DATE
        }

        enum spSpreadCol
        {
            SEQ,
            WO_NO,
            SCALE_BARCODE,
            LINE_NUMBER,
            PROD_CODE,
            PROD_NAME,
            CAPA,
            SCALE_CAPA
        }
        #endregion

        #region form
        public void Main_Load(object sender, EventArgs e)
        {
            SetGrid(); //Grid 설정

            this.label2.Text = Program.cmx.GetTagVal("REPORT.MIXER_ID") == null ? label2.Text : Program.cmx.GetTagVal("REPORT.MIXER_ID").ToString();
            this.Label10.Text = string.Empty; //생산1 Barcode

            this.timer1.Enabled = true;
            this.TextBox1.Focus(); //생산1 지시번호
        }

        #endregion

        #region Event
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOrderNo_1_Click(object sender, EventArgs e)
        {
            //생산1 - 지시번호 클릭
            TextBox1.Focus();
            TextBox1.SelectionStart = 0;
            TextBox1.SelectionLength = TextBox1.Text.Length;
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            //생산1 - 투입탱크 클리어
            TextBoxClear(TextBox4);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            //생산1 - 원료바코드 클리어
            TextBoxClear(TextBox2);
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
            string ReportEnd_1 = Program.cmx.GetTagVal("REPORT.종료") == null ? "0" : Program.cmx.GetTagVal("REPORT.종료").ToString();
            string Glycerine = Program.cmx.GetTagVal("대량원료.GLYCERINE.WEIGHT_OK") == null ? "0" : Program.cmx.GetTagVal("대량원료.GLYCERINE.WEIGHT_OK").ToString();
            string _13BG = Program.cmx.GetTagVal("대량원료._13BG.WEIGHT_OK") == null ? "0" : Program.cmx.GetTagVal("대량원료._13BG.WEIGHT_OK").ToString();
            string Lily = Program.cmx.GetTagVal("대량원료.LILY.WEIGHT_OK") == null ? "0" : Program.cmx.GetTagVal("대량원료.LILY.WEIGHT_OK").ToString();
            string Silicon = Program.cmx.GetTagVal("대량원료.SILICON.WEIGHT_OK") == null ? "0" : Program.cmx.GetTagVal("대량원료.SILICON.WEIGHT_OK").ToString();
            string PW_Weight = Program.cmx.GetTagVal("대량원료.PW.WEIGHT_OK") == null ? "0" : Program.cmx.GetTagVal("대량원료.PW.WEIGHT_OK").ToString();

            if ("1".Equals(ReportEnd_1))
            {
                WorkEndDataBaseUp(TextBox1);
                Program.cmx.SetTagVal("REPORT.시작1", 0);
                Program.cmx.SetTagVal("REPORT.종료", 0);
            }
            else if ("1".Equals(Glycerine))
            {
                GetInstCompleteData(1);
                Program.cmx.SetTagVal("대량원료.GLYCERINE.WEIGHT_OK", 0);
            }
            else if ("1".Equals(_13BG))
            {
                GetInstCompleteData(2);
                Program.cmx.SetTagVal("대량원료._13BG.WEIGHT_OK", 0);
            }
            else if ("1".Equals(Lily))
            {
                GetInstCompleteData(3);
                Program.cmx.SetTagVal("대량원료.LILY.WEIGHT_OK", 0);
            }
            else if ("1".Equals(Silicon))
            {
                GetInstCompleteData(4);
                Program.cmx.SetTagVal("대량원료.SILICON.WEIGHT_OK", 0);
            }
            else if ("1".Equals(PW_Weight))
            {
                GetInstCompleteData(5);
                Program.cmx.SetTagVal("대량원료.PW.WEIGHT_OK", 0);
            }

            this.timer1.Enabled = true;
        }

        private void spOrderNo_Click(object sender, EventArgs e)
        {
            ListView1DataLoad();
        }

        private void TextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            //지시번호
            if (e.KeyCode == Keys.Enter)
            {
                MainData_1st();

                lblUnCheckedCNT.Text = spNotCheckedNo_Sheet1.RowCount.ToString();
                Program.cmx.SetTagVal("UNCHECK_MAT_CNT", lblUnCheckedCNT.Text);
            }
        }

        private void TextBox2_KeyDown(object sender, KeyEventArgs e)
        {
            //생산1 - 원료바코드 텍스트
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrEmpty(TextBox4.Text))
                {
                    MessageBox.Show("투입탱크가 지정되지 않았습니다..투입탱크를 지정해 주시기 바랍니다.!!!");
                    TextBox2.Text = "";
                    TextBox4.Focus();
                    TextBox4.SelectionStart = 0;
                    TextBox4.SelectionLength = TextBox4.Text.Length;
                }
                else
                {
                    lblBarCode_1st.Text = TextBox2.Text.Trim();
                    BarCodechecked_1st(1);

                    lblUnCheckedCNT.Text = spNotCheckedNo_Sheet1.RowCount.ToString();
                    Program.cmx.SetTagVal("UNCHECK_MAT_CNT", lblUnCheckedCNT.Text);
                }
            }
        }

        private void TextBox4_KeyDown(object sender, KeyEventArgs e)
        {
            //투입탱크 버튼(생산1)
            if (e.KeyCode == Keys.Enter)
            {
                string CodeData = TextBox4.Text.Trim();
                if (CodeData.Length != 5)
                {
                    MessageBox.Show(this, "투입탱크 바코드를 확인 하시기 바랍니다.!!!!");
                    TextBox4.Text = string.Empty;
                    TextBox4.Focus();
                }
                else
                {
                    TextBox2.Text = string.Empty;
                    TextBox2.Focus();
                }
            }
        }

        private void TextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //생산1 - 지시번호
            if (!common.ScanKeyPress(e.KeyChar, ref TempScanBarcode, ref TextBox1))
            {
                e.Handled = true;
                return;
            }
        }

        private void TextBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            //생산1 - 투입탱크
            if (!common.ScanKeyPress(e.KeyChar, ref TempScanBarcode, ref TextBox4))
            {
                e.Handled = true;
                return;
            }
        }

        private void TextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //생산1 - 원료바코드
            if (!common.ScanKeyPress(e.KeyChar, ref TempScanBarcode, ref TextBox2))
            {
                e.Handled = true;
                return;
            }
        }

        private void spNotCheckedNo_Click(object sender, EventArgs e)
        {
            TextBox2.Focus();
        }

        private void tabControl1_Click(object sender, EventArgs e)
        {
            if ("tabPage1".Equals(tabControl1.SelectedTab.Name))
            {
                if (string.IsNullOrEmpty(TextBox1.Text))
                {
                    TextBox1.Focus();
                }
                else
                {
                    TextBox2.Focus();
                    TextBox2.SelectionStart = 0;
                    TextBox2.SelectionLength = TextBox2.TextLength;
                }
            }
        }
        #endregion

        #region Method
        private void SetGrid()
        {
            /////////////tabpage1///////////////
            //지시번호 데이터 Sheet
            common.InitSheet(ref spOrderNo_Sheet1, 0, spOrderNo_Sheet1.ColumnCount);
            common.SetColHeader(spOrderNo_Sheet1, typeof(spOrderNoCol));

            //Check 안된 데이터 Sheet
            common.InitSheet(ref spNotCheckedNo_Sheet1, 0, spNotCheckedNo_Sheet1.ColumnCount);
            common.SetColHeader(spNotCheckedNo_Sheet1, typeof(spNotCheckedNoCol));

            //Check 된 데이터 Sheet
            common.InitSheet(ref spCheckedNo_Sheet1, 0, spCheckedNo_Sheet1.ColumnCount);
            common.SetColHeader(spCheckedNo_Sheet1, typeof(spCheckedNoCol));

            //TabPage 3
            common.InitSheet(ref spSpread_Sheet1, 0, spSpread_Sheet1.ColumnCount);
            common.SetColHeader(spSpread_Sheet1, typeof(spSpreadCol));


        }

        private void ListView1DataLoad()
        {
            SheetView sv = spOrderNo.ActiveSheet;
            BackColorClear(sv);

            if (sv.RowCount > 0)
            {
                OrderNo_1st = sv.GetText(sv.ActiveRowIndex, (int)spOrderNoCol.WO_NO);
                ManufactureNo_1st = sv.GetText(sv.ActiveRowIndex, (int)spOrderNoCol.PROD_NO);
                ManufactureiTem_1st = sv.GetText(sv.ActiveRowIndex, (int)spOrderNoCol.PROD_CODE);
                ManufactureName_1st = sv.GetText(sv.ActiveRowIndex, (int)spOrderNoCol.PROD_NAME);

                lblOrderNo_1st.Text = sv.GetText(sv.ActiveRowIndex, (int)spOrderNoCol.WO_NO);
                lblManufactureNo_1st.Text = sv.GetText(sv.ActiveRowIndex, (int)spOrderNoCol.PROD_NO);
                lblManufactureiTem_1st.Text = sv.GetText(sv.ActiveRowIndex, (int)spOrderNoCol.PROD_CODE);
                lblManufactureName_1st.Text = sv.GetText(sv.ActiveRowIndex, (int)spOrderNoCol.PROD_NAME);

                Program.cmx.SetTagVal("REPORT.생산1.지시번호", lblOrderNo_1st.Text);
                Program.cmx.SetTagVal("REPORT.생산1.제조번호", lblManufactureNo_1st.Text);
                Program.cmx.SetTagVal("REPORT.생산1.제조품목", lblManufactureiTem_1st.Text);
                Program.cmx.SetTagVal("REPORT.생산1.제조품명", lblManufactureName_1st.Text);
                Program.cmx.SetTagVal("REPORT.시작1", 1);

                MaterialFind_1st();

                TextBox2.Focus();
                TextBox2.SelectionStart = 0;
                TextBox2.SelectionLength = TextBox2.TextLength;
            }
        }

        private void BackColorClear(SheetView Sheet)
        {
            if (Sheet.RowCount > 0)
            {
                for (int i = 0; i < Sheet.RowCount; i++)
                {
                    Sheet.Rows[i].BackColor = Color.White;
                    Sheet.Rows[i].ForeColor = Color.Black;
                }
            }
        }

        private void BarCodeCheckedBackColorModify_1st(int SelectAs)
        {
            if (SelectAs == 1)
            {
                SheetView sv = spNotCheckedNo_Sheet1;
                for (int i = 0; i < sv.RowCount; i++)
                {
                    if ("Checked".Equals(sv.Cells[i, (int)spNotCheckedNoCol.CHECK].Text))
                    {
                        sv.Rows[i].BackColor = Color.Aqua;
                    }
                }
            }
        }

        private void TextBoxClear(TextBox textBox)
        {
            textBox.Text = string.Empty;
            textBox.Focus();
            textBox.SelectionStart = 0;
            textBox.SelectionLength = textBox.Text.Length;
        }
        private void WorkEndDataBaseUp(TextBox textBox)
        {
            try
            {
                //IF_Manufacturing -> EndDate 업데이트
                qr.UpdateData_IF_Manufactuing(textBox.Text);
            }
            catch(Exception e)
            {
                return;
            }
        }
        private void MaterialFind_1st()
        {
            string strQuery = string.Empty;
            string CKD = string.Empty;
            int row = 0;
            int row2 = 0;

            spCheckedNo_Sheet1.Rows.Clear();
            spNotCheckedNo_Sheet1.Rows.Clear();

            DataTable dt = new DataTable();
            qr.GetData_MaterialFind(ref dt, lblOrderNo_1st.Text);

            if (dt != null && dt.Rows.Count > 0)
            {
                SheetView sv = spCheckedNo_Sheet1;
                SheetView sv2 = spNotCheckedNo_Sheet1;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ("Checked".Equals(dt.Rows[i]["CHECKED"].ToString()))
                    {
                        sv.RowCount += 1;
                        sv.Cells[row, (int)spCheckedNoCol.PROD_NO].Text = dt.Rows[i]["ManufactureNo"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.WO_NO].Text = dt.Rows[i]["OrderNo"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.SEQ].Text = dt.Rows[i]["INPUT_SEQ"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.SCALE_BARCODE].Text = dt.Rows[i]["ScaleBarcode"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.PROD_CODE].Text = dt.Rows[i]["MaterialCode"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.PROD_NAME].Text = dt.Rows[i]["MaterialName"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.ORDER_QTY].Text = dt.Rows[i]["Capacity"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.PROD_QTY].Text = dt.Rows[i]["ScaleCapacity"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.TANK_NAME].Text = dt.Rows[i]["MixerNo"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.BARCODE].Text = dt.Rows[i]["BarCode"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.CHECK].Text = dt.Rows[i]["Checked"].ToString();
                        sv.Cells[row, (int)spCheckedNoCol.CRT_DATE].Text = dt.Rows[i]["RegistrationDate"].ToString();
                        sv.Rows[row].BackColor = Color.Aqua;
                        row++;
                    }
                    else
                    {
                        sv2.RowCount += 1;
                        sv2.Cells[row2, (int)spNotCheckedNoCol.PROD_NO].Text = dt.Rows[i]["ManufactureNo"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.WO_NO].Text = dt.Rows[i]["OrderNo"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.SEQ].Text = dt.Rows[i]["INPUT_SEQ"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.SCALE_BARCODE].Text = dt.Rows[i]["ScaleBarcode"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.PROD_CODE].Text = dt.Rows[i]["MaterialCode"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.PROD_NAME].Text = dt.Rows[i]["MaterialName"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.ORDER_QTY].Text = dt.Rows[i]["Capacity"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.PROD_QTY].Text = dt.Rows[i]["ScaleCapacity"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.TANK_NAME].Text = dt.Rows[i]["MixerNo"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.BARCODE].Text = dt.Rows[i]["BarCode"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.CHECK].Text = dt.Rows[i]["Checked"].ToString();
                        sv2.Cells[row2, (int)spNotCheckedNoCol.CRT_DATE].Text = dt.Rows[i]["RegistrationDate"].ToString();
                        row2++;
                    }
                }
            }
        }
        
        private void BarCodechecked_1st(int SelectAs)
        {
            SheetView sv = spNotCheckedNo_Sheet1;
            string ScaleBarcode = string.Empty;
            string InsertDate = string.Empty;
            string RDate = string.Empty;
            string useWeight = string.Empty;
            string MCode = string.Empty;
            double OrderQty = 0.0;
            int CountD = 0;

            if (SelectAs == 1)
            {
                int workSeqNumber = qr.GetWorkSeqNumber(lblOrderNo_1st.Text);

                for (int i = 0; i < sv.RowCount; i++)
                {
                    ScaleBarcode = sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text;

                    if (ScaleBarcode.Equals(lblBarCode_1st.Text) && sv.Cells[i, (int)spNotCheckedNoCol.CHECK].Text == "")
                    {
                        RDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        useWeight = sv.Cells[i, (int)spNotCheckedNoCol.PROD_QTY].Text; //사용량
                        MCode = sv.Cells[i, (int)spNotCheckedNoCol.PROD_CODE].Text; //제품코드
                        //지시량
                        OrderQty = double.Parse(sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text) > 0.0 ?
                                    double.Parse(sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text) / 1000 : 0.0;
                        INPUT_SEQNO = int.Parse(sv.Cells[i, (int)spNotCheckedNoCol.SEQ].Text); //순번

                        int succCnt = qr.UpdateData_IF_Scale(lblBarCode_1st.Text, TextBox4.Text, RDate, lblOrderNo_1st.Text,
                                                                useWeight, MCode, workSeqNumber.ToString());

                        if (succCnt == 1)
                        {
                            TextBox2.Text = "";
                            TextBox2.Focus();

                            if (GLYCERINE_MCODE.Equals(MCode))
                            {
                                lblGlycerineBarCode_1st.Text = sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text;
                                Program.cmx.SetTagVal("대량원료.GLYCERINE.설정값", OrderQty);
                                Program.cmx.SetTagVal("대량원료.GLYCERINE.투입값", 0);
                                Program.cmx.SetTagVal("대량원료.GLYCERINE.지시번호", sv.Cells[i, (int)spNotCheckedNoCol.WO_NO].Text);
                                Program.cmx.SetTagVal("대량원료.GLYCERINE.제조번호", sv.Cells[i, (int)spNotCheckedNoCol.PROD_NO].Text);
                                Program.cmx.SetTagVal("대량원료.GLYCERINE.제조품목", lblManufactureiTem_1st.Text);
                                Program.cmx.SetTagVal("대량원료.GLYCERINE.지시순번", sv.Cells[i, (int)spNotCheckedNoCol.SEQ].Text);
                                Program.cmx.SetTagVal("대량원료.GLYCERINE.GLYCERINE_BARCODE", sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text);
                                Glycerine_OrderQty[0] = sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text;
                            }
                            else if (_13BG_MCODE.Equals(MCode))
                            {
                                lbl13BGBarCode_1st.Text = sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text;
                                Program.cmx.SetTagVal("대량원료._13BG.설정값", OrderQty);
                                Program.cmx.SetTagVal("대량원료._13BG.투입값", 0);
                                Program.cmx.SetTagVal("대량원료._13BG.지시번호", sv.Cells[i, (int)spNotCheckedNoCol.WO_NO].Text);
                                Program.cmx.SetTagVal("대량원료._13BG.제조번호", sv.Cells[i, (int)spNotCheckedNoCol.PROD_NO].Text);
                                Program.cmx.SetTagVal("대량원료._13BG.제조품목", lblManufactureiTem_1st.Text);
                                Program.cmx.SetTagVal("대량원료._13BG.지시순번", sv.Cells[i, (int)spNotCheckedNoCol.SEQ].Text);
                                Program.cmx.SetTagVal("대량원료._13BG._13BG_BARCODE", sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text);
                                _13BG_OrderQty[0] = sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text;
                            }
                            else if (LILY_MCODE.Equals(MCode))
                            {
                                lblLILYBarCode_1st.Text = sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text;
                                Program.cmx.SetTagVal("대량원료.LILY.설정값", OrderQty);
                                Program.cmx.SetTagVal("대량원료.LILY.투입값", 0);
                                Program.cmx.SetTagVal("대량원료.LILY.지시번호", sv.Cells[i, (int)spNotCheckedNoCol.WO_NO].Text);
                                Program.cmx.SetTagVal("대량원료.LILY.제조번호", sv.Cells[i, (int)spNotCheckedNoCol.PROD_NO].Text);
                                Program.cmx.SetTagVal("대량원료.LILY.제조품목", lblManufactureiTem_1st.Text);
                                Program.cmx.SetTagVal("대량원료.LILY.지시순번", sv.Cells[i, (int)spNotCheckedNoCol.SEQ].Text);
                                Program.cmx.SetTagVal("대량원료.LILY.LILY_BARCODE", sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text);
                                Lily_OrderQty[0] = sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text;
                            }
                            else if (PW_MCODE.Equals(MCode))
                            {
                                lblPWBarCode_1st.Text = sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text;
                                Program.cmx.SetTagVal("대량원료.PW.설정값", OrderQty);
                                Program.cmx.SetTagVal("대량원료.PW.투입값", 0);
                                Program.cmx.SetTagVal("대량원료.PW.지시번호", sv.Cells[i, (int)spNotCheckedNoCol.WO_NO].Text);
                                Program.cmx.SetTagVal("대량원료.PW.제조번호", sv.Cells[i, (int)spNotCheckedNoCol.PROD_NO].Text);
                                Program.cmx.SetTagVal("대량원료.PW.제조품목", lblManufactureiTem_1st.Text);
                                Program.cmx.SetTagVal("대량원료.PW.지시순번", sv.Cells[i, (int)spNotCheckedNoCol.SEQ].Text);
                                Program.cmx.SetTagVal("대량원료.PW.정재수_BARCODE", sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text);
                                PW_OrderQty[0] = sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text;

                            }
                            else if (ETHANOL_MCODE.Equals(MCode))
                            {
                                lblETHANOLBarCode_1st.Text = sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text;
                                Program.cmx.SetTagVal("대량원료.ETHANOL.설정값", OrderQty);
                                Program.cmx.SetTagVal("대량원료.ETHANOL.투입값", 0);
                                Program.cmx.SetTagVal("대량원료.ETHANOL.지시번호", sv.Cells[i, (int)spNotCheckedNoCol.WO_NO].Text);
                                Program.cmx.SetTagVal("대량원료.ETHANOL.제조번호", sv.Cells[i, (int)spNotCheckedNoCol.PROD_NO].Text);
                                Program.cmx.SetTagVal("대량원료.ETHANOL.제조품목", lblManufactureiTem_1st.Text);
                                Program.cmx.SetTagVal("대량원료.ETHANOL.지시순번", sv.Cells[i, (int)spNotCheckedNoCol.SEQ].Text);
                                Program.cmx.SetTagVal("대량원료.ETHANOL.ETHANOL_BARCODE", sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text);
                                Ethanol_OrderQty[0] = sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text;
                            }
                            else if (SILICON_MCODE.Equals(MCode))
                            {
                                lblSILICONBarCode_1st.Text = sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text;
                                Program.cmx.SetTagVal("대량원료.SILICON.설정값", OrderQty);
                                Program.cmx.SetTagVal("대량원료.SILICON.투입값", 0);
                                Program.cmx.SetTagVal("대량원료.SILICON.지시번호", sv.Cells[i, (int)spNotCheckedNoCol.WO_NO].Text);
                                Program.cmx.SetTagVal("대량원료.SILICON.제조번호", sv.Cells[i, (int)spNotCheckedNoCol.PROD_NO].Text);
                                Program.cmx.SetTagVal("대량원료.SILICON.제조품목", lblManufactureiTem_1st.Text);
                                Program.cmx.SetTagVal("대량원료.SILICON.지시순번", sv.Cells[i, (int)spNotCheckedNoCol.SEQ].Text);
                                Program.cmx.SetTagVal("대량원료.SILICON.SILICON_BARCODE", sv.Cells[i, (int)spNotCheckedNoCol.SCALE_BARCODE].Text);
                                Silicon_OrderQty[0] = sv.Cells[i, (int)spNotCheckedNoCol.ORDER_QTY].Text;
                            }
                            workSeqNumber++;
                        }
                    }
                    CountD++;
                }

                if (CountD == 0)
                {
                    MessageBox.Show(this, "자료가 없습니다.!!!  확인 하시기 바랍니다.!!!");
                }
                MaterialFind_1st();
            }

            TextBox2.Text = "";
            TextBox2.Focus();
        }

        private void MainData_1st()
        {
            //텍스트박스에 글자가 없는 경우 return
            if (string.IsNullOrEmpty(TextBox1.Text)) return;

            spOrderNo_Sheet1.Rows.Clear();
            spNotCheckedNo_Sheet1.Rows.Clear();
            spCheckedNo_Sheet1.Rows.Clear();

            OrderNo_1st = string.Empty;
            ManufactureNo_1st = string.Empty;
            ManufactureiTem_1st = string.Empty;
            ManufactureName_1st = string.Empty;
            lblOrderNo_1st.Text = string.Empty;
            lblManufactureNo_1st.Text = string.Empty;
            lblManufactureiTem_1st.Text = string.Empty;
            lblManufactureName_1st.Text = string.Empty;

            DataTable dt = new DataTable();
            qr.GetData_IF_Manufactuing(ref dt, TextBox1.Text.Trim());
            SheetView sv = spOrderNo_Sheet1;
            string Person = string.Empty;

            if (dt != null && dt.Rows.Count > 0)
            {
                //sv.DataSource = dt;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sv.RowCount += 1;
                    sv.Cells[i, (int)spOrderNoCol.WO_NO].Text = dt.Rows[i]["OrderNo"].ToString();
                    sv.Cells[i, (int)spOrderNoCol.PROD_NO].Text = dt.Rows[i]["ManufactureNo"].ToString();
                    sv.Cells[i, (int)spOrderNoCol.BULK_NAME].Text = dt.Rows[i]["BulkName"].ToString();
                    sv.Cells[i, (int)spOrderNoCol.PROD_CODE].Text = dt.Rows[i]["ManufactureiTem"].ToString();
                    sv.Cells[i, (int)spOrderNoCol.PROD_ROOM].Text = dt.Rows[i]["ManufactureRoom"].ToString();
                    sv.Cells[i, (int)spOrderNoCol.PROD_NAME].Text = dt.Rows[i]["ManufactureName"].ToString();
                    sv.Cells[i, (int)spOrderNoCol.PROD_QTY].Text = dt.Rows[i]["ManufactureQTY"].ToString();
                    sv.Cells[i, (int)spOrderNoCol.PROD_DATE].Text = dt.Rows[i]["ManufactureDate"].ToString();
                    sv.Cells[i, (int)spOrderNoCol.USE_EQUIP].Text = dt.Rows[i]["MixerNo"].ToString();
                    sv.Cells[i, (int)spOrderNoCol.CHECK].Text = dt.Rows[i]["EndCheck"].ToString();
                    sv.Cells[i, (int)spOrderNoCol.USER].Text = dt.Rows[i]["ManufacturePerson"].ToString();
                }

                //왜 첫번째 row만 체크하는가?/////////////////////////////////////
                if (sv.Cells[0, (int)spOrderNoCol.CHECK].Text == "")
                {
                    string RDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    Person = Program.cmx.GetTagVal("작업자.작업자_N1").ToString();

                    int succCnt = qr.UpdateData_IF_Manufactuing(TextBox1.Text, RDate, Person, MixerNo);

                    if (succCnt == 1)
                    {
                        sv.Cells[0, (int)spOrderNoCol.PROD_DATE].Text = RDate;
                        sv.Cells[0, (int)spOrderNoCol.USE_EQUIP].Text = MixerNo;
                        sv.Cells[0, (int)spOrderNoCol.CHECK].Text = "Checked";
                        sv.Cells[0, (int)spOrderNoCol.USER].Text = Person;
                    }
                }
                //////////////////////////////////////////////////////////////////
                TextBox4.Text = string.Empty;
                ListView1DataLoad();
                TextBox4.Focus();
            }
            else
            {
                MessageBox.Show(this, "자료가 없습니다.!!!  확인 하시기 바랍니다.!!!");
                TextBox1.Focus();
                return;
            }
        }

        private void SettingReset()
        {
            Program.cmx.SetTagVal("대량원료.GLYCERINE.설정값", 0);
            Program.cmx.SetTagVal("대량원료._13BG.설정값", 0);
            Program.cmx.SetTagVal("대량원료.LILY.설정값", 0);
            Program.cmx.SetTagVal("대량원료.PW.설정값", 0);
            Program.cmx.SetTagVal("대량원료.ETHANOL.설정값", 0);
            Program.cmx.SetTagVal("대량원료.SILICON.설정값", 0);
        }

        private void BarcodeReset_1st()
        {
            Program.cmx.SetTagVal("REPORT.생산1.GLYCELIN_BARCODE", "");
            Program.cmx.SetTagVal("REPORT.생산1._13BG_BARCODE", "");
            Program.cmx.SetTagVal("REPORT.생산1.LILY_BARCODE", "");
            Program.cmx.SetTagVal("REPORT.생산1.정재수_BARCODE", "");
            Program.cmx.SetTagVal("REPORT.생산1.ETHANOL_BARCODE", "");
            Program.cmx.SetTagVal("REPORT.생산1.SILICON_BARCODE", "");
        }

        private void BarcodeReset_2nd()
        {
            Program.cmx.SetTagVal("REPORT.생산2.GLYCELIN_BARCODE", "");
            Program.cmx.SetTagVal("REPORT.생산2._13BG_BARCODE", "");
            Program.cmx.SetTagVal("REPORT.생산2.LILY_BARCODE", "");
            Program.cmx.SetTagVal("REPORT.생산2.정재수_BARCODE", "");
            Program.cmx.SetTagVal("REPORT.생산2.ETHANOL_BARCODE", "");
            Program.cmx.SetTagVal("REPORT.생산2.SILICON_BARCODE", "");
        }

        private void GetInstCompleteData(int POS)
        {
            string OrderNo = string.Empty;
            string OrderSeq = string.Empty;
            string ProdNo = string.Empty;
            string ProdCode = string.Empty;
            int ProdQty = 0;
            string MCode = string.Empty;
            string MQty = "0";
            string UseDate = string.Empty;
            string Status = "N";
            string LotNo = "0";
            string Unit = "G";
            string BarCode21 = string.Empty;

            if(POS == 1)
            {
                OrderNo = Convert.ToString(Program.cmx.GetTagVal("대량원료.GLYCERINE.지시번호"));
                OrderSeq = Convert.ToString(Program.cmx.GetTagVal("대량원료.GLYCERINE.지시순번"));
                ProdNo = Convert.ToString(Program.cmx.GetTagVal("대량원료.GLYCERINE.제조번호"));
                ProdCode = Convert.ToString(Program.cmx.GetTagVal("대량원료.GLYCERINE.제조품목"));
                ProdQty = Convert.ToInt32(Program.cmx.GetTagVal("대량원료.GLYCERINE.설정값"));
                MCode = GLYCERINE_MCODE;
                MQty = Convert.ToString(Program.cmx.GetTagVal("대량원료.GLYCERINE.투입값"));
                UseDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                BarCode21 = Convert.ToString(Program.cmx.GetTagVal("대량원료.GLYCERINE.GLYCERINE_BARCODE"));
            }
            else if (POS == 2)
            {
                OrderNo = Convert.ToString(Program.cmx.GetTagVal("대량원료._13BG.지시번호"));
                OrderSeq = Convert.ToString(Program.cmx.GetTagVal("대량원료._13BG.지시순번"));
                ProdNo = Convert.ToString(Program.cmx.GetTagVal("대량원료._13BG.제조번호"));
                ProdCode = Convert.ToString(Program.cmx.GetTagVal("대량원료._13BG.제조품목"));
                ProdQty = Convert.ToInt32(Program.cmx.GetTagVal("대량원료._13BG.설정값"));
                MCode = _13BG_MCODE;
                MQty = Convert.ToString(Program.cmx.GetTagVal("대량원료._13BG.투입값"));
                UseDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                BarCode21 = Convert.ToString(Program.cmx.GetTagVal("대량원료._13BG._13BG_BARCODE"));
            }
            else if (POS == 3)
            {
                OrderNo = Convert.ToString(Program.cmx.GetTagVal("대량원료.LILY.지시번호"));
                OrderSeq = Convert.ToString(Program.cmx.GetTagVal("대량원료.LILY.지시순번"));
                ProdNo = Convert.ToString(Program.cmx.GetTagVal("대량원료.LILY.제조번호"));
                ProdCode = Convert.ToString(Program.cmx.GetTagVal("대량원료.LILY.제조품목"));
                ProdQty = Convert.ToInt32(Program.cmx.GetTagVal("대량원료.LILY.설정값"));
                MCode = LILY_MCODE;
                MQty = Convert.ToString(Program.cmx.GetTagVal("대량원료.LILY.투입값"));
                UseDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                BarCode21 = Convert.ToString(Program.cmx.GetTagVal("대량원료.LILY.LILY_BARCODE"));
            }
            else if (POS == 4)
            {
                OrderNo = Convert.ToString(Program.cmx.GetTagVal("대량원료.SILICON.지시번호"));
                OrderSeq = Convert.ToString(Program.cmx.GetTagVal("대량원료.SILICON.지시순번"));
                ProdNo = Convert.ToString(Program.cmx.GetTagVal("대량원료.SILICON.제조번호"));
                ProdCode = Convert.ToString(Program.cmx.GetTagVal("대량원료.SILICON.제조품목"));
                ProdQty = Convert.ToInt32(Program.cmx.GetTagVal("대량원료.SILICON.설정값"));
                MCode = SILICON_MCODE;
                MQty = Convert.ToString(Program.cmx.GetTagVal("대량원료.SILICON.투입값"));
                UseDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                BarCode21 = Convert.ToString(Program.cmx.GetTagVal("대량원료.SILICON.SILICON_BARCODE"));
            }
            else if (POS == 5)
            {
                OrderNo = Convert.ToString(Program.cmx.GetTagVal("대량원료.PW.지시번호"));
                OrderSeq = Convert.ToString(Program.cmx.GetTagVal("대량원료.PW.지시순번"));
                ProdNo = Convert.ToString(Program.cmx.GetTagVal("대량원료.PW.제조번호"));
                ProdCode = Convert.ToString(Program.cmx.GetTagVal("대량원료.PW.제조품목"));
                ProdQty = Convert.ToInt32(Program.cmx.GetTagVal("대량원료.PW.설정값"));
                MCode = PW_MCODE;
                MQty = Convert.ToString(Program.cmx.GetTagVal("대량원료.PW.투입값"));
                UseDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                BarCode21 = Convert.ToString(Program.cmx.GetTagVal("대량원료.PW.정재수_BARCODE"));
            }

            qr.Insert_UPLOAD_PROD_OUT(OrderNo, OrderSeq, ProdNo, ProdCode, ProdQty, Unit, MCode, LotNo, MQty, UseDate, Status, BarCode21);
        }

        #endregion

        
    }
}
