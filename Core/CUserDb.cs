﻿using System.Windows.Forms;
using System.Data.Common;

namespace Core
{
    /// <summary>
    /// DB 
    /// </summary>
    public class CUserDb : CBaseDb
    {
        internal static bool OffSetting = false;

        /// <summary>
        /// DB 생성자
        /// </summary>
        public CUserDb()
            : base(ref QueryResult.ConDB)
        {

        }

        public CUserDb(bool bBind)
            : base(ref QueryResult.ConDB, bBind)
        {
        }

        public CUserDb(ref DbConnection pCon)
            : base(ref pCon)
        {
        }

        public CUserDb(ref DbConnection pCon, bool bBind)
            : base(ref pCon, bBind)
        {
        }

        /// <summary>
        /// Error 메세지
        /// </summary>
        internal override void ShowErrMsg(bool bMsgBox)
        {
            string strMsg = string.Empty;

            switch (ErrKind)
            {
                case DB_LOCK:
                    strMsg = "잠시 후 다시 사용하세요. (DB Lock.)";
                    break;
                case DB_DUP:
                    strMsg = "이미 등록된 자료입니다. (DB 중복)";
                    break;
                case DB_EXIST_CHILD:
                    strMsg = "하위 데이터가 존재합니다. 전산실에 문의 바랍니다.";
                    break;
                default:
                    strMsg = ErrMsg;
                    break;
            }

            if (bMsgBox && strMsg != "")
            {
                MessageBox.Show(strMsg, "DB Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
