﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using System.Windows.Forms;
using System.Configuration;

namespace Core
{
    public class Service
    {
        internal const int DB_ERR = -1;           /// DB 에러 
        internal const string CRLF = "\r\n";      /// 줄바꿈

        DbConnection pConObj;
        DbTransaction DbTran;
        DbProviderFactory Factory; // DB Factory (DB DbProviderFactories 클래스)
        DbCommand comMain;
        DbDataAdapter daMain;
        string strErrMsg = string.Empty;

        private string Sql = string.Empty;
        private int SelCnt = 0;

        private void Connect()
        {
            Factory = DbProviderFactories.GetFactory("System.Data.SqlClient");

            pConObj = Factory.CreateConnection();
            pConObj.ConnectionString = ConfigurationSettings.AppSettings["ConnectString_CosmaxMas"]; // ConnectString_CosmaxMas;
            pConObj.Open();
            DbTran = pConObj.BeginTransaction();

            comMain = Factory.CreateCommand();
            daMain = Factory.CreateDataAdapter();

            comMain.Connection = pConObj;
            comMain.CommandType = CommandType.Text;
            comMain.Transaction = DbTran;
            daMain.SelectCommand = comMain;
        }

        public string GetSeqKeyForWrkStep(string wrkStep, string prodNo, string compId)
        {
            Connect();

            DataTable dt = new DataTable();

            Sql = "";
            Sql += "  SELECT SEQKEY \n";
            Sql += "    FROM T_RD_ROUTING_OPER_NEW_R A \n";
            Sql += "   WHERE WRKSTEP = '" + wrkStep + "' \n";
            Sql += "     AND ROUTNO = '" + prodNo + "' \n";
            Sql += "     AND COMPID = '" + compId + "' \n";

            try
            {
                comMain.CommandText = Sql;
                comMain.CommandType = CommandType.Text;
                daMain.Fill(dt);
            }
            catch (DbException DbErr)
            {
                DbTran.Rollback();
                MessageBox.Show(DbErr.Message);
                return null;
            }
            catch (Exception AppErr)
            {
                DbTran.Rollback();
                MessageBox.Show(AppErr.Message);
                return null;
            }

            DbTran.Commit();
            return dt.Rows[0]["SEQKEY"].ToString();
        }

        public DataTable GetMaterialData(string wrkStep, string prodNo, string compId, string seqKey)
        {
            Connect();

            DataTable dt = new DataTable();

            Sql = "";
            Sql += "  SELECT A.SPCOMP \n";
            Sql += "    FROM T_RD_ROUTING_OPER_NEW_ITEM_R A, (SELECT SEQKEY, ROUTNO, COMPID FROM T_RD_ROUTING_OPER_NEW_R A \n";
            Sql += "                                           WHERE WRKSTEP > '" + wrkStep + "' \n";
            Sql += "                                             AND ROUTNO = '" + prodNo + "' \n";
            Sql += "                                             AND COMPID = '" + compId + "' \n";
            Sql += "                                             AND OPRCOD = 'PT_%' \n";
            Sql += "                                             AND SEQKEY = '" + seqKey + "') B \n";
            Sql += "   WHERE SUBSTRING(A.SEQKEY,0 , 5) IN (B.SEQKEY) \n";
            Sql += "     AND A.ROUTNO = B.ROUTNO      \n";
            Sql += "     AND A.COMPID = B.COMPID      \n";
            Sql += "ORDER BY A.SEQKEY                 \n";

            try
            {
                comMain.CommandText = Sql;
                comMain.CommandType = CommandType.Text;
                daMain.Fill(dt);
            }
            catch (DbException DbErr)
            {
                DbTran.Rollback();
                MessageBox.Show(DbErr.Message);
                return null;
            }
            catch (Exception AppErr)
            {
                DbTran.Rollback();
                MessageBox.Show(AppErr.Message);
                return null;
            }

            DbTran.Commit();
            return dt;
        }
        
        public int UpdateData_IF_Scale(string BarCode, string MixerNo, string regDate, string orderNo, string useWeight, string MCode)
        {
            Connect();

            DataTable dt = new DataTable(); ;

            Sql = "";
            Sql += "  UPDATE IF_SCALE \n";
            Sql += "     SET BARCODE = '" + BarCode + "', " + "CHECKED = 'Checked' \n";
            Sql += "         , MIXERNO = '" + MixerNo + "', " + "REGISTRATIONDATE = '" + regDate + "' \n";
            Sql += "   WHERE ORDERNO = '" + orderNo + "' AND SCALEBARCODE = '" + BarCode + "' AND SCALECAPACITY = '" + useWeight + "'";
            Sql += "     AND MATERIALCODE = '" + MCode + "'";

            try
            {
                comMain.CommandText = Sql;
                comMain.CommandType = CommandType.Text;
                SelCnt = comMain.ExecuteNonQuery();
            }
            catch (DbException DbErr)
            {
                DbTran.Rollback();
                MessageBox.Show(DbErr.Message);
                return 0;
            }
            catch (Exception AppErr)
            {
                DbTran.Rollback();
                MessageBox.Show(AppErr.Message);
                return 0;
            }

            DbTran.Commit();
            return SelCnt;
        }
    }
}
