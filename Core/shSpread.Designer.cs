﻿namespace Core
{
    partial class shSpread
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ctmsMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiSorting = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiMultiSorting = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiFiltering = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiColumnSeq = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiColumnSeqSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiColumnSeqInit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiColumnWidth = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiColumnWidthSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiColumnWidthInit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPDF = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiHtml = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiXml = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiSelectionUnitCell = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSelectionUnitRow = new System.Windows.Forms.ToolStripMenuItem();
            this.spData = new FarPoint.Win.Spread.FpSpread();
            this.ssData = new FarPoint.Win.Spread.SheetView();
            this.ctmsMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // ctmsMenu
            // 
            this.ctmsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSorting,
            this.tsmiMultiSorting,
            this.tsmiFiltering,
            this.tsmiColumnSeq,
            this.tsmiColumnWidth,
            this.tsSeparator,
            this.tsmiExcel,
            this.tsmiPDF,
            this.tsmiHtml,
            this.tsmiXml,
            this.tsmiPrint,
            this.toolStripSeparator1,
            this.tsmiSelectionUnitCell,
            this.tsmiSelectionUnitRow});
            this.ctmsMenu.Name = "ctmsMenu1";
            this.ctmsMenu.Size = new System.Drawing.Size(183, 280);
            this.ctmsMenu.Opening += new System.ComponentModel.CancelEventHandler(this.ctmsMenu_Opening);
            // 
            // tsmiSorting
            // 
            this.tsmiSorting.CheckOnClick = true;
            this.tsmiSorting.Name = "tsmiSorting";
            this.tsmiSorting.Size = new System.Drawing.Size(182, 22);
            this.tsmiSorting.Text = "정렬";
            this.tsmiSorting.Click += new System.EventHandler(this.tsmiSorting_Click);
            // 
            // tsmiMultiSorting
            // 
            //this.tsmiMultiSorting.Name = "tsmiMultiSorting";
            //this.tsmiMultiSorting.Size = new System.Drawing.Size(182, 22);
            //this.tsmiMultiSorting.Text = "다중정렬";
            //this.tsmiMultiSorting.Click += new System.EventHandler(this.tsmiMultiSort_Click);
            // 
            // tsmiFiltering
            // 
            this.tsmiFiltering.CheckOnClick = true;
            this.tsmiFiltering.Name = "tsmiFiltering";
            this.tsmiFiltering.Size = new System.Drawing.Size(182, 22);
            this.tsmiFiltering.Text = "필터";
            this.tsmiFiltering.Click += new System.EventHandler(this.tsmiFiltering_Click);
            // 
            // tsmiColumnSeq
            // 
            this.tsmiColumnSeq.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiColumnSeqSave,
            this.tsmiColumnSeqInit});
            this.tsmiColumnSeq.Name = "tsmiColumnSeq";
            this.tsmiColumnSeq.Size = new System.Drawing.Size(182, 22);
            this.tsmiColumnSeq.Text = "컬럼 순서";
            this.tsmiColumnSeq.Visible = false;
            // 
            // tsmiColumnSeqSave
            // 
            //this.tsmiColumnSeqSave.Name = "tsmiColumnSeqSave";
            //this.tsmiColumnSeqSave.Size = new System.Drawing.Size(110, 22);
            //this.tsmiColumnSeqSave.Text = "저장";
            //this.tsmiColumnSeqSave.Click += new System.EventHandler(this.tsmiColumnSeqSave_Click);
            // 
            // tsmiColumnSeqInit
            // 
            //this.tsmiColumnSeqInit.Name = "tsmiColumnSeqInit";
            //this.tsmiColumnSeqInit.Size = new System.Drawing.Size(110, 22);
            //this.tsmiColumnSeqInit.Text = "초기화";
            //this.tsmiColumnSeqInit.Click += new System.EventHandler(this.tsmiColumnSeqInit_Click);
            // 
            // tsmiColumnWidth
            // 
            this.tsmiColumnWidth.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiColumnWidthSave,
            this.tsmiColumnWidthInit});
            this.tsmiColumnWidth.Name = "tsmiColumnWidth";
            this.tsmiColumnWidth.Size = new System.Drawing.Size(182, 22);
            this.tsmiColumnWidth.Text = "컬럼 넓이";
            this.tsmiColumnWidth.Visible = false;
            // 
            // tsmiColumnWidthSave
            // 
            //this.tsmiColumnWidthSave.Name = "tsmiColumnWidthSave";
            //this.tsmiColumnWidthSave.Size = new System.Drawing.Size(110, 22);
            //this.tsmiColumnWidthSave.Text = "저장";
            //this.tsmiColumnWidthSave.Click += new System.EventHandler(this.tsmiColumnWidthSave_Click);
            // 
            // tsmiColumnWidthInit
            // 
            //this.tsmiColumnWidthInit.Name = "tsmiColumnWidthInit";
            //this.tsmiColumnWidthInit.Size = new System.Drawing.Size(110, 22);
            //this.tsmiColumnWidthInit.Text = "초기화";
            //this.tsmiColumnWidthInit.Click += new System.EventHandler(this.tsmiColumnWidthInit_Click);
            // 
            // tsSeparator
            // 
            this.tsSeparator.Name = "tsSeparator";
            this.tsSeparator.Size = new System.Drawing.Size(179, 6);
            // 
            // tsmiExcel
            // 
            this.tsmiExcel.Name = "tsmiExcel";
            this.tsmiExcel.Size = new System.Drawing.Size(182, 22);
            this.tsmiExcel.Text = "엑셀 저장";
            this.tsmiExcel.Click += new System.EventHandler(this.tsmiExcel_Click);
            // 
            // tsmiPDF
            // 
            this.tsmiPDF.Name = "tsmiPDF";
            this.tsmiPDF.Size = new System.Drawing.Size(182, 22);
            this.tsmiPDF.Text = "PDF 저장";
            this.tsmiPDF.Click += new System.EventHandler(this.tsmiPDF_Click);
            // 
            // tsmiHtml
            // 
            this.tsmiHtml.Name = "tsmiHtml";
            this.tsmiHtml.Size = new System.Drawing.Size(182, 22);
            this.tsmiHtml.Text = "HTML 저장";
            this.tsmiHtml.Click += new System.EventHandler(this.tsmiHtml_Click);
            // 
            // tsmiXml
            // 
            this.tsmiXml.Name = "tsmiXml";
            this.tsmiXml.Size = new System.Drawing.Size(182, 22);
            this.tsmiXml.Text = "XML 저장";
            this.tsmiXml.Click += new System.EventHandler(this.tsmiXml_Click);
            // 
            // tsmiPrint
            // 
            this.tsmiPrint.Name = "tsmiPrint";
            this.tsmiPrint.Size = new System.Drawing.Size(182, 22);
            this.tsmiPrint.Text = "인쇄";
            this.tsmiPrint.Click += new System.EventHandler(this.tsmiPrint_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(179, 6);
            // 
            // tsmiSelectionUnitCell
            // 
            this.tsmiSelectionUnitCell.CheckOnClick = true;
            this.tsmiSelectionUnitCell.Name = "tsmiSelectionUnitCell";
            this.tsmiSelectionUnitCell.Size = new System.Drawing.Size(182, 22);
            this.tsmiSelectionUnitCell.Text = "셀단위 데이터 선택";
            this.tsmiSelectionUnitCell.Click += new System.EventHandler(this.tsmiSelectionUnitCell_Click);
            // 
            // tsmiSelectionUnitRow
            // 
            this.tsmiSelectionUnitRow.CheckOnClick = true;
            this.tsmiSelectionUnitRow.Name = "tsmiSelectionUnitRow";
            this.tsmiSelectionUnitRow.Size = new System.Drawing.Size(182, 22);
            this.tsmiSelectionUnitRow.Text = "행 단위 데이터 선택";
            this.tsmiSelectionUnitRow.Click += new System.EventHandler(this.tsmiSelectionUnitRow_Click);
            // 
            // spData
            // 
            this.spData.AccessibleDescription = "";
            this.spData.Location = new System.Drawing.Point(0, 0);
            this.spData.Name = "spData";
            this.spData.Sheets.AddRange(new FarPoint.Win.Spread.SheetView[] {
            this.ssData});
            this.spData.Size = new System.Drawing.Size(200, 100);
            this.spData.TabIndex = 0;
            // 
            // ssData
            // 
            this.ssData.Reset();
            this.ssData.SheetName = "Sheet1";
            // 
            // shSpread
            // 
            this.ContextMenuStrip = this.ctmsMenu;
            this.ctmsMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ContextMenuStrip ctmsMenu;
        internal System.Windows.Forms.ToolStripMenuItem tsmiExcel;
        internal System.Windows.Forms.ToolStripMenuItem tsmiPDF;
        internal System.Windows.Forms.ToolStripMenuItem tsmiXml;
        internal System.Windows.Forms.ToolStripMenuItem tsmiHtml;
        internal System.Windows.Forms.ToolStripMenuItem tsmiPrint;
        private System.Windows.Forms.ToolStripMenuItem tsmiSorting;
        private System.Windows.Forms.ToolStripSeparator tsSeparator;
        private System.Windows.Forms.ToolStripMenuItem tsmiFiltering;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem tsmiSelectionUnitCell;
        private System.Windows.Forms.ToolStripMenuItem tsmiSelectionUnitRow;
        private System.Windows.Forms.ToolStripMenuItem tsmiMultiSorting;
        private FarPoint.Win.Spread.FpSpread spData;
        private FarPoint.Win.Spread.SheetView ssData;
        private System.Windows.Forms.ToolStripMenuItem tsmiColumnSeq;
        private System.Windows.Forms.ToolStripMenuItem tsmiColumnWidth;
        private System.Windows.Forms.ToolStripMenuItem tsmiColumnSeqSave;
        private System.Windows.Forms.ToolStripMenuItem tsmiColumnSeqInit;
        private System.Windows.Forms.ToolStripMenuItem tsmiColumnWidthSave;
        private System.Windows.Forms.ToolStripMenuItem tsmiColumnWidthInit;

    }
}
