﻿using System;
using System.Data.Common;
using System.Configuration;
using System.Windows.Forms;
using System.Data;

namespace Core
{
    public class QueryResult
    {
        internal const int DB_ERR = -1;           /// DB 에러 
        internal const string CRLF = "\r\n";      /// 줄바꿈
        public static System.Data.Common.DbConnection ConDB;            /// DB Connection 객체
        public CUserDb Bdb;
        private string Sql = string.Empty;
        private int SelCnt = 0;
        private string RetMsg = string.Empty;
        CommonCode commonFunc = new CommonCode();

        public QueryResult()
        {
            DBLogin();
        }
        private void DBLogin()
        {
            try
            {
                // DB 접속
                if (QueryResult.ConDB == null)
                {
                    DBLogIn(ref QueryResult.ConDB);
                }

                Bdb = new CUserDb();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "로그인 에러");
            }
        }
        /// <summary>
        /// DB Login
        /// </summary>
        private bool DBLogIn(ref DbConnection pConObj)
        {
            string ConnectString = string.Empty; //ConnectString
            DbProviderFactory Factory; // DB Factory (DB DbProviderFactories 클래스)

            try
            {
                if ("DEV".Equals(commonFunc.Status))
                {
                    ConnectString = ConfigurationSettings.AppSettings["ConnectString_DEV"];
                }
                else
                {
                    ConnectString = ConfigurationSettings.AppSettings["ConnectString_REAL"];
                }

                Factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
                pConObj = Factory.CreateConnection();
                pConObj.ConnectionString = ConnectString;
                pConObj.Open();

                return true;
            }
            catch (Exception AppErr)
            {
                MessageBox.Show(AppErr.ToString(), "DB Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                pConObj = null;
                return false;
            }
        }
        #region Common

        public int UpdateData_IF_Scale(string BarCode, string MixerNo, string regDate, string orderNo, string useWeight, string MCode, string lineNumber)
        {
            Sql = "";
            Sql += CRLF + "  UPDATE IF_SCALE \n";
            Sql += CRLF + "     SET BARCODE = '" + BarCode + "', " + "CHECKED = 'Checked' \n";
            Sql += CRLF + "         , MIXERNO = '" + MixerNo + "', REGISTRATIONDATE = '" + regDate + "', LINENUMBER = '" + lineNumber + "' \n";
            Sql += CRLF + "   WHERE ORDERNO = '" + orderNo + "' AND SCALEBARCODE = '" + BarCode + "' AND SCALECAPACITY = '" + useWeight + "'";
            Sql += CRLF + "     AND MATERIALCODE = '" + MCode + "'";

            SelCnt = Bdb.ExcuteNonQry(Sql);
            return SelCnt;
        }

        public int UpdateData_IF_Manufactuing(string orderNo)
        {
            string EDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            Sql = "";
            Sql += "  UPDATE IF_Manufacturing \n";
            Sql += "     SET EndDate = '" + EDate + "' \n";
            Sql += "   WHERE ORDERNO = '" + orderNo + "'";

            SelCnt = Bdb.ExcuteNonQry(Sql);
            return SelCnt;
        }

        public int UpdateData_IF_Manufacturing(string startTime, string prodOrderNo)
        {
            Sql = "";
            //if (ConfigurationSettings.AppSettings["Server"] == "MAS_TEST")
            //{
            //    Sql += CRLF + " UPDATE PMS.dbo.IF_Manufacturing                                       ";
            //}
            //else
            //{
                Sql += CRLF + " UPDATE IF_Manufacturing                                       ";
            //}
            Sql += CRLF + " SET StartTime = '" + startTime + "'                         ";
            Sql += CRLF + " WHERE OrderNo = '" + prodOrderNo + "'               ";
            SelCnt = Bdb.ExcuteNonQry(Sql);
            return SelCnt;
        }

        public int UpdateData_IF_Manufactuing(string orderNo, string RDate, string Person, string MixerNo)
        {
            Sql = "";
            Sql += CRLF + "  UPDATE IF_Manufacturing                            ";
            Sql += CRLF + "     SET ManufactureDate = '" + RDate + "', StartTime = '" + RDate + "', EndCheck = 'Checked' ";
            Sql += CRLF + "         ,ManufacturePerson = '" + Person + "', MixerNo = '" + MixerNo + "' ";
            Sql += CRLF + "   WHERE OrderNo = '" + orderNo + "'";

            SelCnt = Bdb.ExcuteNonQry(Sql);
            return SelCnt;
        }

        public int GetData_IF_Manufactuing(ref DataTable dt, string orderNo)
        {
            Sql = "";
            //Sql += "  SELECT * FROM IF_Manufacturing \n";
            Sql += CRLF + "  SELECT OrderNo                             ";
            Sql += CRLF + "         , ManufactureNo                     ";
            Sql += CRLF + "         , BulkCode                          ";
            Sql += CRLF + "         , BulkName                          ";
            Sql += CRLF + "         , ManufactureiTem                   ";
            Sql += CRLF + "         , ManufactureRoom                   ";
            Sql += CRLF + "         , ManufactureName                   ";
            Sql += CRLF + "         , ManufactureQTY                    ";
            Sql += CRLF + "         , ManufactureDate                   ";
            Sql += CRLF + "         , StartTime                         ";
            Sql += CRLF + "         , MixerNo                           ";
            Sql += CRLF + "         , EndCheck                          ";
            Sql += CRLF + "         , ManufacturePerson                 ";
            Sql += CRLF + "    FROM IF_Manufacturing                    ";
            Sql += CRLF + "   WHERE ORDERNO = '" + orderNo + "'         ";
            Sql += CRLF + "     AND CRT_TYPE = 'SAVE' AND D_MARK = 'i'  ";

            SelCnt = Bdb.ExcuteQry(ref dt, Sql);
            return SelCnt;
        }
        public int Insert_UPLOAD_PROD_OUT(string orderNo, string orderSeq, string prodNo, string prodCode, int prodQty,
                                            string unit, string mCode, string lotNo, string mQty, string useDate,
                                            string status, string barcode21)
        {
            Sql = "";
            Sql += "  INSERT INTO UPLOAD_PROD_OUT(WO_NO, INPUT_SEQ, PROD_LOT_NO, PROD_ITEM_CODE, PROD_ORDER_QTY, PROD_ITEM_UNIT, \n";
            Sql += "                               ITEM_CODE, LOT_NO, USE_QTY, ITEM_UNIT, ERP_SEND_DATE, ERP_RECV_ST, SCAN_BARCORD) \n";
            Sql += "                       VALUES('" + orderNo + "' \n";
            Sql += "                             ,'" + orderSeq + "' \n";
            Sql += "                             ,'" + prodNo + "' \n";
            Sql += "                             ,'" + prodCode + "' \n";
            Sql += "                             ,'" + prodQty + "' \n";
            Sql += "                             ,'" + unit + "' \n";
            Sql += "                             ,'" + mCode + "' \n";
            Sql += "                             ,'" + lotNo + "' \n";
            Sql += "                             ,'" + mQty + "' \n";
            Sql += "                             ,'" + useDate + "' \n";
            Sql += "                             ,'" + status + "' \n";
            Sql += "                             ,'" + barcode21 + "' \n";

            SelCnt = Bdb.ExcuteNonQry(Sql);
            return SelCnt;
        }
        #endregion

        #region ProdStart
        public int GetData_Worker(ref DataTable dt, string sCompId)
        {
            Sql = "";
            Sql += CRLF + " SELECT WORKER_INFO_SEQ      ";
            Sql += CRLF + "      , WORKER               ";
            Sql += CRLF + " FROM T_WORKER_INFO          ";
            Sql += CRLF + " WHERE USE_YN = 'Y'          ";
            Sql += CRLF + "   AND COMPID = '" + sCompId + "'";

            SelCnt = Bdb.ExcuteQry(ref dt, Sql);
            return SelCnt;
        }
        
        public int GetData_RTGETC(ref DataTable dt, string compId, string itemCode, string version)
        {
            Sql = "";
            Sql += CRLF + " SELECT RTGETC                                                        ";
            Sql += CRLF + " FROM T_RD_ROUTING_OPER_NEW_R                                         ";
            Sql += CRLF + " WHERE COMPID = '" + compId + "'                                       ";
            Sql += CRLF + " AND ROUTNO = '" + itemCode + "'                                       ";
            if(!string.IsNullOrEmpty(version))
            {
                Sql += CRLF + " AND VERSION = " + version + "                                       ";
            }

            SelCnt = Bdb.ExcuteQry(ref dt, Sql);
            return SelCnt;
        }

        public int InsertData_T_FACT_UPTIME(string compId, string woNo, string prodNo, string itemCode, string itemName, string tankId, string startTime)
        {
            Sql = "";
            Sql += CRLF + " INSERT INTO T_FACT_UPTIME(                 ";
            Sql += CRLF + "   COMPID                                   ";
            Sql += CRLF + " , WO_NO                                    ";
            Sql += CRLF + " , PROD_NO                                  ";
            Sql += CRLF + " , ITEM_CODE                                ";
            Sql += CRLF + " , ITEM_NAME                                ";
            Sql += CRLF + " , MIXER_ID                                 ";
            Sql += CRLF + " , START_DATE)                              ";
            Sql += CRLF + " VALUES(                                    ";
            Sql += CRLF + "   '" + compId + "'                                  ";
            Sql += CRLF + " , '" + woNo + "'                                    ";
            Sql += CRLF + " , '" + prodNo + "'                                  ";
            Sql += CRLF + " , '" + itemCode + "'                                ";
            Sql += CRLF + " , '" + itemName + "'                                ";
            Sql += CRLF + " , '" + tankId + "'     ";
            Sql += CRLF + " , '" + startTime + "')                              ";
            SelCnt = Bdb.ExcuteNonQry(Sql);
            return SelCnt;
        }

        public int GetCount_IF_RD_ROUTING_OPER_NEW_R(string compId, string routNo, string version)
        {
            Sql = "";
            Sql += CRLF + " SELECT COUNT(*) AS CNT                        ";
            Sql += CRLF + " FROM IF_RD_ROUTING_OPER_NEW_R                                                      ";
            Sql += CRLF + " WHERE COMPID = '" + compId + "'                                                    ";
            Sql += CRLF + " AND ROUTNO = '" + routNo + "'                                                   ";
            Sql += CRLF + " AND VERSION = '" + version + "'                                                   ";
            SelCnt = Bdb.ExcuteQry(Sql);
            return SelCnt;
        }

        public bool InsertData_L2LIF0025_WEB_responseIF_RD_ROUTING_OPER_NEW_R2(string[][] SAPDATA, string pCompId, string pRoutNo, string pVersion)
        {
            Bdb.BeginTrans();

            // 인터페이스 테이블(IF_RD_ROUTING_OPER_NEW_R)에 INSERT
            foreach (string[] sapData in SAPDATA)
            {
                Sql = "";
                Sql += CRLF + " INSERT INTO IF_RD_ROUTING_OPER_NEW_R(                        ";
                Sql += CRLF + "   COMPID                                                     ";
                Sql += CRLF + "   , ROUTNO                                                   ";
                Sql += CRLF + "   , VERSION                                                  ";
                Sql += CRLF + "   , SEQKEY                                                   ";
                Sql += CRLF + "   , SPCOMP                                                   ";
                Sql += CRLF + "   , NODTYP                                                   ";
                Sql += CRLF + "   , NODEID                                                   ";
                Sql += CRLF + "   , OPRCOD                                                   ";
                Sql += CRLF + "   , OPTIND                                                   ";
                Sql += CRLF + "   , OPRUOT                                                   ";
                Sql += CRLF + "   , OPTIME                                                   ";
                Sql += CRLF + "   , OPTEMP                                                   ";
                Sql += CRLF + "   , OPRPMH                                                   ";
                Sql += CRLF + "   , OPRPMH_R                                                 ";
                Sql += CRLF + "   , OPRPMP                                                   ";
                Sql += CRLF + "   , OPRPMS                                                   ";
                Sql += CRLF + "   , OPRPMV                                                   ";
                Sql += CRLF + "   , WTEMP                                                    ";
                Sql += CRLF + "   , WRPMD                                                    ";
                Sql += CRLF + "   , OTEMP                                                    ";
                Sql += CRLF + "   , ORPMP                                                    ";
                Sql += CRLF + "   , OPSINS                                                   ";
                Sql += CRLF + "   , OPSINSDSC                                                ";
                Sql += CRLF + "   , RTGETC                                                   ";
                Sql += CRLF + "   , ADDEMP                                                   ";
                Sql += CRLF + "   , ADDTIM                                                   ";
                Sql += CRLF + "   , LOGEMP                                                   ";
                Sql += CRLF + "   , LOGTIM                                                   ";
                Sql += CRLF + "   , PO_SEND_TIME                                             ";
                Sql += CRLF + "   , PROCYN                                                   ";
                Sql += CRLF + "   , PROCTIM)                                                 ";
                Sql += CRLF + " VALUES(                                                      ";
                Sql += CRLF + "     '" + pCompId + "'                                        ";
                Sql += CRLF + "   , '" + pRoutNo + "'                                        ";
                Sql += CRLF + "   , " + pVersion + "                                         ";
                Sql += CRLF + "   , '" + sapData[0] + "'                                     ";
                Sql += CRLF + "   , '" + sapData[1] + "'                                     ";
                Sql += CRLF + "   , '" + sapData[2] + "'                                     ";
                Sql += CRLF + "   , '" + sapData[3] + "'                                     ";
                Sql += CRLF + "   , '" + sapData[4] + "'                                     ";
                Sql += CRLF + "   , '" + sapData[5] + "'                                     ";
                Sql += CRLF + "   , '" + sapData[6] + "'                                     ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[7])) ? "0" : sapData[7]) + "                                 ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[8])) ? "0" : sapData[8]) + "                                 ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[9])) ? "0" : sapData[9]) + "                                 ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[10])) ? "0" : sapData[10]) + "                               ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[11])) ? "0" : sapData[11]) + "                                 ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[12])) ? "0" : sapData[12]) + "                                 ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[13])) ? "0" : sapData[13]) + "                                 ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[14])) ? "0" : sapData[14]) + "                                  ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[15])) ? "0" : sapData[15]) + "                                  ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[16])) ? "0" : sapData[16]) + "                                  ";
                Sql += CRLF + "   , " + ((string.IsNullOrEmpty(sapData[17])) ? "0" : sapData[17]) + "                                  ";
                Sql += CRLF + "   , '" + sapData[18] + "'                                 ";
                Sql += CRLF + "   , '" + sapData[19] + "'                              ";
                Sql += CRLF + "   , '" + sapData[20] + "'                                 ";
                Sql += CRLF + "   , '" + sapData[21] + "'                                 ";
                Sql += CRLF + "   , '" + sapData[22] + "'                                 ";
                Sql += CRLF + "   , '" + sapData[23] + "'                                 ";
                Sql += CRLF + "   , '" + sapData[24] + "'                                 ";
                Sql += CRLF + "   , GETDATE()                                ";
                Sql += CRLF + "   , 'N'                                ";
                Sql += CRLF + "   , GETDATE())                                ";
                SelCnt = Bdb.ExcuteNonQry(Sql);
                if (SelCnt != 1)
                {
                    Bdb.Rollback(true);
                    return false;
                }
            }

            // 본테이블에 INSERT

            // T_RD_ROUTING_OPER_NEW_R : 회사, 품목 같은게 있는 경우에는 기존걸 NEW_YN = 'N'으로 UDPATE
            Sql = "";
            Sql += CRLF + " UPDATE T_RD_ROUTING_OPER_NEW_R                        ";
            Sql += CRLF + " SET NEW_YN = 'N'                                                      ";
            Sql += CRLF + " WHERE COMPID = '" + pCompId + "'                                                    ";
            Sql += CRLF + " AND ROUTNO = '" + pRoutNo + "'                                                   ";
            SelCnt = Bdb.ExcuteNonQry(Sql);
            if (SelCnt == DB_ERR)
            {
                Bdb.Rollback(true);
                return false;
            }

            // T_RD_ROUTING_OPER_NEW_R : INSERT
            Sql = "";
            Sql += CRLF + " INSERT INTO T_RD_ROUTING_OPER_NEW_R(                        ";
            Sql += CRLF + " COMPID                                                      ";
            Sql += CRLF + " , ROUTNO                                                    ";
            Sql += CRLF + " , VERSION                                                   ";
            Sql += CRLF + " , SEQKEY                                                    ";
            Sql += CRLF + " , NODTYP                                                    ";
            Sql += CRLF + " , NODEID                                                    ";
            Sql += CRLF + " , OPRCOD                                                    ";
            Sql += CRLF + " , OPTIND                                                    ";
            Sql += CRLF + " , OPRUOT                                                    ";
            Sql += CRLF + " , OPTIME                                                    ";
            Sql += CRLF + " , OPTEMP                                                    ";
            Sql += CRLF + " , OPRPMH                                                    ";
            Sql += CRLF + " , OPRPMH_R                                                  ";
            Sql += CRLF + " , OPRPMP                                                    ";
            Sql += CRLF + " , OPRPMS                                                    ";
            Sql += CRLF + " , OPRPMV                                                    ";
            Sql += CRLF + " , WTEMP                                                     ";
            Sql += CRLF + " , WRPMD                                                     ";
            Sql += CRLF + " , OTEMP                                                     ";
            Sql += CRLF + " , ORPMP                                                     ";
            Sql += CRLF + " , OPSINS                                                    ";
            Sql += CRLF + " , OPSINSDSC                                                 ";
            Sql += CRLF + " , RTGETC                                                    ";
            Sql += CRLF + " , NEW_YN                                                    ";
            Sql += CRLF + " , ADDEMP                                                    ";
            Sql += CRLF + " , ADDTIM                                                    ";
            Sql += CRLF + " , LOGEMP                                                    ";
            Sql += CRLF + " , LOGTIM                                                    ";
            Sql += CRLF + " , WRKSTEP                                                    ";
            Sql += CRLF + " )                                                           ";
            Sql += CRLF + " SELECT                                                      ";
            Sql += CRLF + " COMPID                                                      ";
            Sql += CRLF + " , ROUTNO                                                    ";
            Sql += CRLF + " , VERSION                                                   ";
            Sql += CRLF + " , SEQKEY                                                    ";
            Sql += CRLF + " , NODTYP                                                    ";
            Sql += CRLF + " , NODEID                                                    ";
            Sql += CRLF + " , OPRCOD                                                    ";
            Sql += CRLF + " , OPTIND                                                    ";
            Sql += CRLF + " , OPRUOT                                                    ";
            Sql += CRLF + " , OPTIME                                                    ";
            Sql += CRLF + " , OPTEMP                                                    ";
            Sql += CRLF + " , OPRPMH                                                    ";
            Sql += CRLF + " , OPRPMH_R                                                  ";
            Sql += CRLF + " , OPRPMP                                                    ";
            Sql += CRLF + " , OPRPMS                                                    ";
            Sql += CRLF + " , OPRPMV                                                    ";
            Sql += CRLF + " , WTEMP                                                     ";
            Sql += CRLF + " , WRPMD                                                     ";
            Sql += CRLF + " , OTEMP                                                     ";
            Sql += CRLF + " , ORPMP                                                     ";
            Sql += CRLF + " , OPSINS                                                    ";
            Sql += CRLF + " , OPSINSDSC                                                 ";
            Sql += CRLF + " , RTGETC                                                    ";
            Sql += CRLF + " , 'Y'                                                       ";
            Sql += CRLF + " , ADDEMP                                                    ";
            Sql += CRLF + " , ADDTIM                                                    ";
            Sql += CRLF + " , LOGEMP                                                    ";
            Sql += CRLF + " , LOGTIM                                                    ";
            Sql += CRLF + " , ROW_NUMBER() OVER(ORDER BY SEQKEY)                                                    ";
            Sql += CRLF + " FROM IF_RD_ROUTING_OPER_NEW_R                               ";
            Sql += CRLF + " WHERE PROCYN = 'N'                                          ";
            Sql += CRLF + " AND NODTYP = 1                                          ";
            Sql += CRLF + " ORDER BY SEQKEY ASC                                 ";
            SelCnt = Bdb.ExcuteNonQry(Sql);
            if (SelCnt == DB_ERR)
            {
                Bdb.Rollback(true);
                return false;
            }

            // T_RD_ROUTING_OPER_NEW_ITEM_R : 회사, 품목, 버전이 같은게 있는 경우에는 DELETE
            Sql = "";
            Sql += CRLF + " DELETE T_RD_ROUTING_OPER_NEW_ITEM_R                                                      ";
            Sql += CRLF + " WHERE COMPID = '" + pCompId + "'                                                    ";
            Sql += CRLF + " AND ROUTNO = '" + pRoutNo + "'                                                   ";
            Sql += CRLF + " AND VERSION = " + pVersion + "                                                    ";
            SelCnt = Bdb.ExcuteNonQry(Sql);
            if (SelCnt == DB_ERR)
            {
                Bdb.Rollback(true);
                return false;
            }

            // T_RD_ROUTING_OPER_NEW_ITEM_R : INSERT
            Sql = "";
            Sql += CRLF + " INSERT INTO T_RD_ROUTING_OPER_NEW_ITEM_R(                        ";
            Sql += CRLF + " COMPID                                                      ";
            Sql += CRLF + " , ROUTNO                                                    ";
            Sql += CRLF + " , VERSION                                                   ";
            Sql += CRLF + " , SEQKEY                                                    ";
            Sql += CRLF + " , SPCOMP                                                    ";
            Sql += CRLF + " , ADDEMP                                                    ";
            Sql += CRLF + " , ADDTIM                                                    ";
            Sql += CRLF + " , LOGEMP                                                    ";
            Sql += CRLF + " , LOGTIM                                                    ";
            Sql += CRLF + " , NODEID                                                    ";
            Sql += CRLF + " )                                                           ";
            Sql += CRLF + " SELECT                                                      ";
            Sql += CRLF + " COMPID                                                      ";
            Sql += CRLF + " , ROUTNO                                                    ";
            Sql += CRLF + " , VERSION                                                   ";
            Sql += CRLF + " , SEQKEY                                                    ";
            Sql += CRLF + " , SPCOMP                                                    ";
            Sql += CRLF + " , ADDEMP                                                    ";
            Sql += CRLF + " , ADDTIM                                                    ";
            Sql += CRLF + " , LOGEMP                                                    ";
            Sql += CRLF + " , LOGTIM                                                    ";
            Sql += CRLF + " , NODEID                                                    ";
            Sql += CRLF + " FROM IF_RD_ROUTING_OPER_NEW_R                               ";
            Sql += CRLF + " WHERE PROCYN = 'N'                                          ";
            Sql += CRLF + " AND NODTYP = 2                                          ";
            Sql += CRLF + " ORDER BY SEQKEY ASC                                 ";
            SelCnt = Bdb.ExcuteNonQry(Sql);
            if (SelCnt == DB_ERR)
            {
                Bdb.Rollback(true);
                return false;
            }

            Sql = "";
            Sql += CRLF + " UPDATE IF_RD_ROUTING_OPER_NEW_R                        ";
            Sql += CRLF + " SET PROCYN = 'Y'                                                      ";
            Sql += CRLF + " , PROCTIM = GETDATE()                                                    ";
            Sql += CRLF + " WHERE PROCYN = 'N'                                                   ";
            SelCnt = Bdb.ExcuteNonQry(Sql);
            if (SelCnt != SAPDATA.Length)
            {
                Bdb.Rollback(true);
                return false;
            }

            Bdb.Commit();
            return true;
        }
        #endregion

        #region MatInput
        public int GetDataForWrkStep(ref DataTable dt, string wrkStep, string prodNo, string compId)
        {
            Sql = "";
            Sql += CRLF + "  SELECT SEQKEY, OPRCOD, OPTIME, OPTEMP, OPRPMH, OPRPMH_R, OPRPMP, OPRPMS, OPRPMV, WTEMP, WRPMD, OTEMP, ORPMP, OPSINS, OPSINSDSC \n";
            Sql += CRLF + "    FROM T_RD_ROUTING_OPER_NEW_R A \n";
            Sql += CRLF + "   WHERE WRKSTEP = '" + wrkStep + "' \n";
            Sql += CRLF + "     AND ROUTNO = '" + prodNo + "' \n";
            Sql += CRLF + "     AND COMPID = '" + compId + "' \n";

            SelCnt = Bdb.ExcuteQry(ref dt, Sql);
            return SelCnt;
        }

        public int GetMaterialData(ref DataTable dt, string orderNo, string wrkStep, string prodNo, string compId, string seqKey, string normalCase = "NORMAL")
        {
            if (seqKey == null || seqKey == "")
            {
                seqKey = "00";
            }
            Sql = "";
            Sql += "  SELECT AA.INPUT_SEQ, BB.SPCOMP, AA.MaterialName, AA.ScaleCapacity, AA.ScaleBarCode, AA.Checked, AA.LineNumber  \n";
            Sql += "    FROM (SELECT IFS.INPUT_SEQ, IFS.MaterialCode, IFS.MaterialName, IFS.ScaleCapacity, IFS.ScaleBarCode, IFS.Checked, IFS.LineNumber  \n";
            Sql += "            FROM IF_Scale IFS                                                                \n";
            Sql += "           WHERE 1=1 \n";
            Sql += "             AND IFS.OrderNo = '" + orderNo + "' \n";
            Sql += "             ) AA, \n";
            Sql += "         (SELECT T.SPCOMP, CONVERT(NUMERIC(10), T.NODEID) AS NODEID \n";
            Sql += "            FROM T_RD_ROUTING_OPER_NEW_ITEM_R T \n";
            Sql += "           WHERE 1=1 \n";
            if ("WATER".Equals(normalCase))
            {
                Sql += "         AND T.SEQKEY = '0101' \n";
            }
            else if("PROCPROGRESS".Equals(normalCase))
            {

            }
            else
            {
                Sql += "         AND T.SEQKEY LIKE '" + seqKey.Substring(0, 2) + "%' \n";
                Sql += "         AND T.SEQKEY NOT IN ('0101') \n";
            }
            Sql += "             AND T.ROUTNO = '" + prodNo + "'          \n";
            Sql += "             AND T.COMPID = '" + compId + "') BB      \n";
            Sql += "   WHERE 1=1                                        \n";
            Sql += "     AND AA.MaterialCode = BB.SPCOMP        \n";
            Sql += "     AND AA.INPUT_SEQ = BB.NODEID           \n";
            if ("PROCPROGRESS".Equals(normalCase))
            {
                Sql += "ORDER BY CONVERT(INT, AA.LineNumber)      \n";
            }
            else
            {
                Sql += "ORDER BY AA.INPUT_SEQ                       \n";
            }

            SelCnt = Bdb.ExcuteQry(ref dt, Sql);
            return SelCnt;
        }

        public int GetWorkSeqNumber(string orderNo)
        {
            DataTable dt = new DataTable();

            Sql = "";
            Sql += CRLF + "  SELECT CASE WHEN MAX(LineNumber) IS NULL  OR LEN(MAX(LineNumber)) = 0 THEN '1' ELSE MAX(CONVERT(INT, LineNumber)) + 1 END LineNumber \n";
            Sql += CRLF + "    FROM IF_SCALE WHERE ORDERNO = '" + orderNo + "' ";

            SelCnt = Bdb.ExcuteQry(ref dt, Sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                return int.Parse(dt.Rows[0]["LineNumber"].ToString());
            }
            else
            {
                return 1;
            }
        }

        public int ChkTankId(string tankId, string sCode, string partCode)
        {
            DataTable dt = new DataTable();
            string compId = ConfigurationSettings.AppSettings["CompId"];

            Sql = "";
            Sql += CRLF + "  SELECT TANKID ";
            Sql += CRLF + "    FROM T_TANK_INFO ";
            Sql += CRLF + "   WHERE COMPID = '" + compId + "' ";
            Sql += CRLF + "     AND TANKID = '" + tankId + "' ";
            Sql += CRLF + "     AND S_CODE = '" + partCode + "' ";

            SelCnt = Bdb.ExcuteQry(ref dt, Sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region ProcProgress
        public int GetComment(ref DataTable dtComment, string RoutNo, string Version)
        {
            string CompId = ConfigurationSettings.AppSettings["CompId"];

            Sql = "";
            Sql += CRLF + " SELECT RTGETC                           ";
            Sql += CRLF + " FROM T_RD_ROUTING_OPER_NEW_R            ";
            Sql += CRLF + " WHERE COMPID = '"+ CompId + "'          ";
            Sql += CRLF + " AND ROUTNO = '"+ RoutNo + "'          ";
            if(!string.IsNullOrEmpty(Version))
            {
                Sql += CRLF + " AND VERSION = " + Version + "          ";
            }

            SelCnt = Bdb.ExcuteQry(ref dtComment, Sql);
            return SelCnt;
        }

        public int GetAlarm(ref DataTable dt, string compId, string woNo, string prodNo, string itemCode, string mixerId, string startDate)
        {
            Sql = "";
            Sql += CRLF + " SELECT CONVERT(CHAR(10), OCCU_DATE, 111)                     ";
            Sql += CRLF + "  + ' ' + CONVERT(CHAR(8), OCCU_DATE, 108)                    ";
            Sql += CRLF + "  + ' ' + ALARM_CONT AS ALARM_TEXT                            ";
            Sql += CRLF + "  FROM T_FACT_ALARM_INFO                                      ";
            Sql += CRLF + "  WHERE COMPID = '" + compId + "'                             ";
            Sql += CRLF + "  AND WO_NO = '" + woNo +"'                                   ";
            Sql += CRLF + "  AND OCCU_DATE >= '" + startDate + "'                        ";
            SelCnt = Bdb.ExcuteQry(ref dt, Sql);
            return SelCnt;
        }
        #endregion

        #region OrderMatInput
        public int GetData_MaterialFind(ref DataTable dt, string OrderNo)
        {
            Sql = "";
            Sql += "  SELECT ManufactureNo, OrderNo, INPUT_SEQ, ScaleBarcode, MaterialCode, MaterialName, Capacity \n";
            Sql += "         ,ScaleCapacity, MixerNo, BarCode, Checked, RegistrationDate \n";
            Sql += "    FROM IF_Scale A \n";
            Sql += "   WHERE A.ORDERNO = '" + OrderNo + "' \n";
            Sql += "ORDER BY A.CHECKED DESC, INPUT_SEQ";

            SelCnt = Bdb.ExcuteQry(ref dt, Sql);
            return SelCnt;
        }

        #endregion

        #region ProcChk

        public int GetNextStepName(ref DataTable dt, string sCompId, string sCode)
        {
            Sql = "";
            Sql += "  SELECT OPRDSC FROM T_RD_OPER_NEW_MASTER \n";
            Sql += "   WHERE COMPID = '" + sCompId + "' \n";
            Sql += "     AND OPRCOD = '" + sCode +   "' \n";

            SelCnt = Bdb.ExcuteQry(ref dt, Sql);
            return SelCnt;
        }
        #endregion

        #region ProdEnd

        public bool ProdEnd(string endTime, string orderNo)
        {
            Bdb.BeginTrans();

            Sql = "";
            Sql += CRLF + " UPDATE IF_Manufacturing                                     ";
            Sql += CRLF + " SET EndTime = '" + endTime + "'                         ";
            Sql += CRLF + " WHERE OrderNo = '" + orderNo + "'               ";

            SelCnt = Bdb.ExcuteNonQry(Sql);
            if (SelCnt != 1)
            {
                Bdb.Rollback(true);
                return false;
            }

            Sql = "";
            Sql += CRLF + " UPDATE T_FACT_UPTIME                                     ";
            Sql += CRLF + " SET END_DATE = '" + endTime + "'                         ";
            Sql += CRLF + " WHERE WO_NO = '" + orderNo + "'               ";

            SelCnt = Bdb.ExcuteNonQry(Sql);
            if (SelCnt < 0)
            {
                Bdb.Rollback(true);
                return false;
            }

            Bdb.Commit();
            return true;
        }
        #endregion

        #region CauseInputPopup
        public int GetCuaseData(ref DataTable dt, string sCode)
        {
            Sql = "";
            Sql += "  SELECT S_CODE_NAME \n";
            Sql += "    FROM T_CODE_COMM A \n";
            Sql += "   WHERE A.M_CODE = '" + sCode + "' \n";
            Sql += "ORDER BY A.SORDER";

            SelCnt = Bdb.ExcuteQry(ref dt, Sql);
            return SelCnt;
        }
        #endregion

        public int SetCauseData(string compId, string wo_no, string tankId, string UserName, string mCode, string sCode, string routNo, string Version, string wrkStep)
        {
            string sDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            Sql = "";
            Sql += CRLF + " INSERT INTO T_AUTO_STOP_INFO(         ";
            Sql += CRLF + "   COMPID                              ";
            Sql += CRLF + " , WO_NO                               ";
            Sql += CRLF + " , TANKID                              ";
            Sql += CRLF + " , WORKER_SEQ                          ";
            Sql += CRLF + " , M_CODE                              ";
            Sql += CRLF + " , S_CODE                              ";
            Sql += CRLF + " , ROUNTNO                              ";
            Sql += CRLF + " , VERSION                             ";
            Sql += CRLF + " , WRKSTEP                             ";
            Sql += CRLF + " , ADDEMP                              ";
            Sql += CRLF + " , ADDTIM                              ";
            Sql += CRLF + " , LOGEMP                              ";
            Sql += CRLF + " , LOGTIM)                             ";
            Sql += CRLF + " VALUES(                               ";
            Sql += CRLF + "   '" + compId + "'                    ";
            Sql += CRLF + " , '" + wo_no + "'                     ";
            Sql += CRLF + " , '" + tankId + "'                    ";
            Sql += CRLF + " , (SELECT WORKER_INFO_SEQ FROM T_WORKER_INFO WHERE WORKER = '" + UserName + "')  ";
            Sql += CRLF + " , '" + mCode + "'               ";
            Sql += CRLF + " , '" + sCode + "'               ";
            Sql += CRLF + " , '" + routNo + "'              ";
            Sql += CRLF + " , '" + Version + "'             ";
            Sql += CRLF + " , '" + wrkStep + "'             ";
            Sql += CRLF + " , '" + UserName + "'            ";
            Sql += CRLF + " , '" + sDate + "'               ";
            Sql += CRLF + " , '" + UserName + "'            ";
            Sql += CRLF + " , '" + sDate + "')              ";
            SelCnt = Bdb.ExcuteNonQry(Sql);
            return SelCnt;
        }
        public bool TestStart(string orderNo)
        {
            Bdb.BeginTrans();

            Sql = "";
            Sql += CRLF + " UPDATE IF_Manufacturing SET STARTTIME = NULL, ENDTIME = NULL, EndCheck = null ";
            if (!string.IsNullOrEmpty(orderNo))
            {
                Sql += CRLF + " WHERE OrderNo = '" + orderNo + "'               ";
            }

            SelCnt = Bdb.ExcuteNonQry(Sql);
            if (SelCnt < 0)
            {
                Bdb.Rollback(true);
                return false;
            }

            Bdb.Commit();
            return true;
        }
    }
}
