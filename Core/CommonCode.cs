﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarPoint.Win.Spread;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Configuration;

namespace Core
{
    public class CommonCode : Form
    {
        private string _WrkStep = string.Empty;
        private string _WrkName = string.Empty;
        private string _PartTypeName = string.Empty;

        public string progID;
        object bird;

        //수상, 유상 - 시간, 온도, RPM 변수
        private string _OpTime = "0";
        private string _OpTemp = "0";
        private string _OpRpmH = "0";
        private string _OpRpmH_R = "0";
        private string _OpRpmP = "0";
        private string _OpRpmS = "0";
        private string _OpVacu = "0";
        private string _wTemp = "0";
        private string _wRpmD = "0";
        private string _oTemp = "0";
        private string _oRpmP = "0";
        //DEV - MAS_TEST, REAL - MAS, SAEHIM - 회사서버
        public string Status = "REAL";
        #region 생성자
        public CommonCode()
        {
            //InitializeComponent();
        }

        public string WrkStep
        {
            get
            {
                return _WrkStep;
            }
            set
            {
                _WrkStep = value;
            }
        }

        public string WrkName
        {
            get
            {
                return _WrkName;
            }
            set
            {
                _WrkName = value;
            }
        }

        public string PartTypeName
        {
            get
            {
                return _PartTypeName;
            }
            set
            {
                _PartTypeName = value;
            }
        }
        public string OpTime
        {
            get
            {
                return _OpTime;
            }
            set
            {
                _OpTime = value;
            }
        }
        public string OpTemp
        {
            get
            {
                return _OpTemp;
            }
            set
            {
                _OpTemp = value;
            }
        }
        public string OpRpmH
        {
            get
            {
                return _OpRpmH;
            }
            set
            {
                _OpRpmH = value;
            }
        }
        public string OpRpmH_R
        {
            get
            {
                return _OpRpmH_R;
            }
            set
            {
                _OpRpmH_R = value;
            }
        }
        public string OpRpmP
        {
            get
            {
                return _OpRpmP;
            }
            set
            {
                _OpRpmP = value;
            }
        }
        public string OpRpmS
        {
            get
            {
                return _OpRpmS;
            }
            set
            {
                _OpRpmS = value;
            }
        }
        public string OpVacu
        {
            get
            {
                return _OpVacu;
            }
            set
            {
                _OpVacu = value;
            }
        }
        public string wTemp
        {
            get
            {
                return _wTemp;
            }
            set
            {
                _wTemp = value;
            }
        }
        public string wRpmD
        {
            get
            {
                return _wRpmD;
            }
            set
            {
                _wRpmD = value;
            }
        }
        public string oTemp
        {
            get
            {
                return _oTemp;
            }
            set
            {
                _oTemp = value;
            }
        }
        public string oRpmP
        {
            get
            {
                return _oRpmP;
            }
            set
            {
                _oRpmP = value;
            }
        }
        #endregion
        #region Sheet
        /// <summary>
        /// 시트 설정
        /// </summary>
        public void InitSheet(ref FarPoint.Win.Spread.SheetView pssData)
        {
            InitSheet(ref pssData, 0, 0, 30, OperationMode.Normal);
        }

        /// <summary>
        /// 시트 설정 Row, Column
        /// </summary>
        public void InitSheet(ref FarPoint.Win.Spread.SheetView pssData, int pRowCnt, int pColCnt)
        {
            InitSheet(ref pssData, pRowCnt, pColCnt, 30, OperationMode.Normal);
        }

        /// <summary>
        /// 시트 설정 Row, Column, HeaderHeight
        /// </summary>
        public void InitSheet(ref FarPoint.Win.Spread.SheetView pssData, int pRowCnt, int pColCnt, int pHeaderHeight)
        {
            InitSheet(ref pssData, pRowCnt, pColCnt, pHeaderHeight, OperationMode.Normal);
        }

        /// <summary>
        /// 시트 설정
        /// </summary>
        public void InitSheet(ref FarPoint.Win.Spread.SheetView pssData, int pRowCnt, int pColCnt, int pHeaderHeight, OperationMode pOperationMode)
        {
            pssData.RowCount = pRowCnt;
            pssData.Columns.Count = pColCnt;
            pssData.ColumnHeader.Rows[0].Height = pHeaderHeight;
            pssData.DataAutoHeadings = false;
            pssData.DataAutoSizeColumns = false;
            pssData.DataAutoCellTypes = false;

            // OperationMode
            pssData.OperationMode = pOperationMode;

            // Selection 선택을 Default Row 단위로
            pssData.SelectionUnit = FarPoint.Win.Spread.Model.SelectionUnit.Row;
            pssData.GrayAreaBackColor = System.Drawing.Color.GhostWhite;
            pssData.SelectionStyle = SelectionStyles.SelectionColors;
            //pssData.SelectionBackColor = System.Drawing.Color.LightCyan;
            pssData.SelectionBackColor = System.Drawing.Color.PowderBlue;

            // Edit 모드시 데이터 선택된 상태로 보여짐.
            pssData.FpSpread.EditModeReplace = true;

        }

        /// <summary>
        /// 스프레드시트 시트 지정 컬럼헤더 설정
        /// </summary>
        /// <param name="pMatchDataField">Data Filed Name를 가지고 컬럼을 찾는 옶션, 미리 Binding 된 DataSource에서 DataField를 찾아서 컬럼 속성 값 정의 시 사용</param>
        /// <param name="pssData"></param>
        /// <param name="pRowIdx"></param>
        /// <param name="pColumnTag"></param>
        /// <param name="pText"></param>
        /// <param name="pWidth"></param>
        /// <param name="pHorizontalAlignment"></param>
        /// <param name="pVerticalAlignment"></param>
        /// <param name="pHidden"></param>
        public void SetColHeader(FarPoint.Win.Spread.SheetView pssData, Type pColumn, CellHorizontalAlignment pHorizontalAlignment = CellHorizontalAlignment.Center, CellVerticalAlignment pVerticalAlignment = CellVerticalAlignment.Center)
        {
            // 컬럼의 헤더문자열 지정
            var columnValues = Enum.GetValues(pColumn);

            for (int i = 0; i < pssData.ColumnCount; i++)
            {
                pssData.Columns[i].Tag = columnValues.GetValue(i).ToString();
                pssData.Columns[i].DataField = columnValues.GetValue(i).ToString();
                //컬럼의 폭지정
                //원료명의 경우 왼쪽 정렬
                if ("PROD_NAME".Equals(pssData.Columns[i].Tag) || "MAT_NAME".Equals(pssData.Columns[i].Tag))
                {
                    pssData.Columns[i].HorizontalAlignment = CellHorizontalAlignment.Left;
                }
                else
                {
                    pssData.Columns[i].HorizontalAlignment = pHorizontalAlignment;
                }
                pssData.Columns[i].VerticalAlignment = pVerticalAlignment;
                //pssData.Columns[i].BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
                //pssData.Columns[i].BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(215)))), ((int)(((byte)(215)))), ((int)(((byte)(215)))));
                // Sheet Column Lock
                pssData.Columns[i].Locked = true;
            }

            //컬럼의 폭지정
            //pssData.SetColumnWidth(ColIdx, pWidth);
            //pssData.DataAutoSizeColumns = true;
            // 컬럼숨김 여부
            //pssData.Columns[ColIdx].Visible = pHidden;
        }
        #endregion
        #region 바코드스캔
        public bool ScanKeyPress(char KeyChar, ref StringBuilder TempScanBarcode, ref TextBox ScanTextBox)
        {
            //************************** 키보드 처리****************************
            // 입력 키값을 가지고 값을 처리
            // TempScanBarcode 값에 넣었다가 엔터키를 입력받으면 초기화
            int KeyValue = (int)KeyChar;

            // 32(스페이스), 13(엔터), 8(백스페이스), 9(탭) // 229(마우스)는 KeyUp에만 발생 
            if (KeyValue < 32 && KeyValue != 13 && KeyValue != 8 && KeyValue != 9) return false;

            // 바코드 PREFIX (^) 이면 
            if (KeyValue == 94) return false;

            // 엔터키가 아니면
            if (KeyValue != 13)
            {
                // 백스페이스이고 TempSanBarcode에 데이터가 있으면 1문자씩 지운다.
                // 아니면 데이터를 변수에 입력한다.
                if (KeyValue == 8)
                {
                    if (TempScanBarcode.Length > 0)
                    {
                        TempScanBarcode.Length = TempScanBarcode.Length - 1;
                    }
                }
                else
                {
                    TempScanBarcode.Append(KeyChar);
                }
                ScanTextBox.Text = TempScanBarcode.ToString();
                return false;
            }
            if (TempScanBarcode.Length < 1) return false;

            TempScanBarcode.Clear(); //스캔한 바코드 정보 클리어
            return true;
            // *****************************************************************
        }
        #endregion

        public int AddWrkStep(string wrkStep)
        {
            int addWrkStep = 0;

            if (!string.IsNullOrEmpty(wrkStep))
            {
                addWrkStep = int.Parse(wrkStep) + 1;
            }
            return addWrkStep;
        }

        private bool isCimonXRun()
        {
            Process[] viewProcess = Process.GetProcessesByName("CimonX");

            if (viewProcess != null && viewProcess.Length == 1)
            {
                return true;
            }
            else return false;
        }

        public void CimonXConnection(Object state, EventArgs eventArgs)
        {
            //정상 연결이 된 경우
            if (bird != null && isCimonXRun()) return;

            // CimonX가 중간에 종료된 경우
            if (bird != null && !isCimonXRun())
            {
                Marshal.ReleaseComObject(bird);
                bird = null;
                return;
            }

            // 초기 OLE 연결 설정
            if (bird == null && isCimonXRun())
            {
                progID = "CimonX.Document";

                var type = Type.GetTypeFromProgID(progID);
                if (type == null)
                {
                    throw new Exception("Invalid ProgID.");
                }

                var obj = Activator.CreateInstance(type);
                IntPtr pIUnk = Marshal.GetIUnknownForObject(obj);
                IntPtr ppv;
                Guid IID_IDispatch = new Guid("{00020400-0000-0000-C000-000000000046}");
                Int32 result = Marshal.QueryInterface(pIUnk, ref IID_IDispatch, out ppv);
                if (result < 0)
                { throw new Exception("Invalid QueryInterface."); }
                else
                {
                    bird = Marshal.GetObjectForIUnknown(ppv);
                }

                return;
            }

            bird = null;
        }

        public void Delay(int ms)
        {
            DateTime dateTimeNow = DateTime.Now;
            TimeSpan duration = new TimeSpan(0, 0, 0, 0, ms);
            DateTime dateTimeAdd = dateTimeNow.Add(duration);

            while(dateTimeAdd >= dateTimeNow)
            {
                Application.DoEvents();
                dateTimeNow = DateTime.Now;
            }

            return;
        }
    }
}
