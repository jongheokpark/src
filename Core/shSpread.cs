﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FarPoint.Win.Spread;

namespace Core
{
    public partial class shSpread : FpSpread
    {
        // 문서 종류
        public enum DocType : int
        {
            Excel = 1,
            PDF = 2,
            Html = 3,
            Xml = 4,
            Print = 5
        }

        private bool _UseSorting = true;
        private bool _UseFiltering = true;
        private string _CompanyName = "";
        private string _SearchInfo = string.Empty;
        private bool _IsMultiSort = true;


        public shSpread()
        {
            InitializeComponent();
        }

        [Browsable(true)]
        [DefaultValue(true)]
        public bool UseSorting
        {
            get { return this._UseSorting; }
            set
            {
                this._UseSorting = value;
                this.tsmiSorting.Enabled = value;
            }
        }

        [Browsable(true)]
        [DefaultValue(true)]
        public bool UseFiltering
        {
            get { return this._UseFiltering; }
            set
            {
                this._UseFiltering = value;
                this.tsmiFiltering.Enabled = value;
            }
        }

        /// <summary>
        /// 그리드 프린트 시 회사명 출력
        /// </summary>
        [Browsable(true)]
        [DefaultValue("")]
        public string CompanyNameText
        {
            get { return this._CompanyName; }
            set
            {
                this._CompanyName = value;
            }
        }

        /// <summary>
        /// 프린트 시 조회 조건 출력
        /// </summary>
        [Browsable(true)]
        [DefaultValue("")]
        public string SearchInfoText
        {
            get { return this._SearchInfo; }
            set
            {
                this._SearchInfo = value;
            }
        }


        // 정렬
        private void tsmiSorting_Click(object sender, System.EventArgs e)
        {
            if (this.tsmiSorting.Checked)
            {
                this.ActiveSheet.Columns[0, this.ActiveSheet.Columns.Count - 1].AllowAutoSort = true;
            }
            else
            {
                this.ActiveSheet.Columns[0, this.ActiveSheet.Columns.Count - 1].ResetSortIndicator();
                this.ActiveSheet.Columns[0, this.ActiveSheet.Columns.Count - 1].AllowAutoSort = false;
            }
        }

        // Multi Sort용 그리드 초기화
        // Hwang
        private void SetGrid()
        {
            //CommonCode.InitSheet(ref ssData);
            //// 컬럼의 헤더문자열 지정
            //ssData.Columns.Add(0, 5);
            //ssData.Columns[0].Label = "";
            //ssData.Columns[0].Tag = "COLUMN_IDX";
            //ssData.Columns[0].DataField = "COLUMN_IDX";

            //ssData.Columns[1].Label = "";
            //ssData.Columns[1].Tag = "COLUMN_NAME";
            //ssData.Columns[1].DataField = "COLUMN_NAME";

            //ssData.Columns[2].Label = "";
            //ssData.Columns[2].Tag = "DISPLAY_NAME";
            //ssData.Columns[2].DataField = "DISPLAY_NAME";

            //ssData.Columns[3].Label = "";
            //ssData.Columns[3].Tag = "DESCRIPTION";
            //ssData.Columns[3].DataField = "DESCRIPTION";

            //ssData.Columns[4].Label = "";
            //ssData.Columns[4].Tag = "SORT";
            //ssData.Columns[4].DataField = "SORT";

            //_IsMultiSort = false;

        }

        // 필터
        private void tsmiFiltering_Click(object sender, System.EventArgs e)
        {
            this.ActiveSheet.Columns[0, this.ActiveSheet.Columns.Count - 1].AllowAutoFilter = (this.tsmiFiltering.Checked) ? true : false;
        }

        // 엑셀 저장
        private void tsmiExcel_Click(object sender, EventArgs e)
        {
            Export(DocType.Excel);
        }

        // PDF 저장
        private void tsmiPDF_Click(object sender, EventArgs e)
        {
            Export(DocType.PDF);
        }

        // HTML 저장
        private void tsmiHtml_Click(object sender, EventArgs e)
        {
            Export(DocType.Html);
        }

        // XML 저장
        private void tsmiXml_Click(object sender, EventArgs e)
        {
            Export(DocType.Xml);
        }

        // 인쇄
        private void tsmiPrint_Click(object sender, EventArgs e)
        {
            Export(DocType.Print);
        }

        // 내보내기
        private void Export(DocType pDocType)
        {
            SaveFileDialog sfdFile = new SaveFileDialog();

            // Hidden 컬럼을 삭제하기 위해 시트의 복사본을 만든다.
            FpSpread Fp = new FpSpread();
            for (int i = 0; i < this.Sheets.Count; i++)
            {
                Fp.Sheets.Add(this.Sheets[i].Clone());
            }

            // Hidden 컬럼 삭제
            for (int i = 0; i < Fp.Sheets.Count; i++)
            {
                for (int j = Fp.Sheets[i].ColumnCount - 1; j < 0; j--)
                {
                    if (Fp.Sheets[i].Columns[j].Visible == false)
                    {
                        Fp.Sheets[i].Columns[j].Remove();
                    }
                }
            }

            // ***프린터 인쇄 설정 정보를 생성한다. ***************
            PrintInfo Pi = new PrintInfo();
            Pi.PrintType = PrintType.All;

            //SmartPrintRulesCollection Rules = new SmartPrintRulesCollection();
            //Rules.Add(new BestFitColumnRule(ResetOption.Current));
            //Rules.Add(new LandscapeRule(ResetOption.Current));
            //Rules.Add(new ScaleRule(ResetOption.None, 1, 0.2F, 0.05F));
            //Pi.SmartPrintRules = Rules;

            // 여백지정
            Pi.Margin = new PrintMargin(10, 30, 10, 30, 10, 10);

            //Pi.UseSmartPrint = true;
            Pi.UseSmartPrint = false;
            Pi.ShowGrid = true;
            Pi.ShowBorder = true;
            Pi.ShowShadows = true;
            Pi.ShowColor = true;
            Pi.UseMax = false;
            // 인쇄폭을 자동으로  지정
            //Pi.BestFitCols = true;

            // 사용자 정의 프린터 옵션 폼
            //frmPrintOption PrintOption = new frmPrintOption();
            //if (pDocType == DocType.Print)
            //{
            //    Pi.Orientation = FarPoint.Win.Spread.PrintOrientation.Landscape;
            //    PrintOption.ShowDialog();
            //    if (PrintOption.DialogResult == DialogResult.Cancel) return;
            //    // 가로 출력 여부
            //    if (!PrintOption.LandscapeYn)
            //    {
            //        Pi.Orientation = FarPoint.Win.Spread.PrintOrientation.Portrait;
            //    }
            //    // 컬럼 최소화 여부
            //    if (PrintOption.BestFitColumnYn)
            //    {
            //        Pi.BestFitCols = true;
            //    }
            //    // 축소설정
            //    Pi.ZoomFactor = (float)Convert.ToDecimal(PrintOption.PageSize);
            //}


            // ***************************************************

            try
            {
                // DOCTYPE에 따라 SaveFileDialog에 나타나는 확장자를 바꿔준다.
                // 인쇄의 경우 다른 것과 다르게 순수 인쇄 만 한다.
                switch (pDocType)
                {
                    case DocType.Excel:
                        sfdFile.Filter = "EXCEL 97 - 2003 통합문서 (*.xls)|*.xls|EXCEL 2007 통합문서 (*.xlsx)|*.xlsx";
                        break;
                    case DocType.PDF:
                        sfdFile.Filter = "PDF Files (*.pdf)|*.pdf";
                        break;
                    case DocType.Xml:
                        sfdFile.Filter = "XML 데이터 (*.xml)|*.xml";
                        break;
                    case DocType.Html:
                        sfdFile.Filter = "웹 페이지 (*.html)|*.html";
                        break;
                    //case DocType.Print:
                    //    Pi.ShowPrintDialog = true;
                    //    Pi.Preview = true;

                    //    Pi.PrintToPdf = false;
                    //    Pi.PdfFileName = "";

                    //    Pi.Header = "/c/fz\"12\"/fb1" + Form.ActiveForm.Text + "/fb0/fz\"10\"/n/l/n" + "/n" + "/l" + "/r" + "Page : /p / /pc";

                    //    // 조회조건 추가
                    //    if (_SearchInfo != string.Empty)
                    //        Pi.Header += "/n" + "/l/fz\"10\"/fb0" + this._SearchInfo + "/n";

                    //    Pi.Footer = "인쇄자 : " + CCmApp.UserName + "    " + "인쇄일시 : " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + "/l/r" + _CompanyName;

                    //    Fp.ActiveSheet.PrintInfo = Pi;
                    //    Fp.PrintSheet(Fp.ActiveSheet);

                    //    return;
                    default:
                        return;
                }

                sfdFile.FilterIndex = 1;
                sfdFile.RestoreDirectory = true;
                if (sfdFile.ShowDialog() != DialogResult.OK) return;

                // 확장자 가져오기
                string extension = System.IO.Path.GetExtension(sfdFile.FileName);
                extension = extension.ToLower();

                // ************************* 엑셀, HTML, XML등은 Sheet 컬럼헤더 추가하여 조회조건 넣기 ******************************************
                if (extension != ".pdf")
                {
                    Fp.ActiveSheet.ColumnHeader.RowCount += 3;

                    // ColumnHeader 재설정
                    for (int i = 1; i < Fp.ActiveSheet.ColumnCount; i++)
                    {
                        Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 1, i].Text = Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 4, i].Text;
                    }
                    // 화면 제목 추가
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 4, 1].Text = Form.ActiveForm.Text;
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 4, 1].ColumnSpan = Fp.ActiveSheet.ColumnCount - 1;
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 4, 1].HorizontalAlignment = CellHorizontalAlignment.Center;
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 4, 1].BackColor = System.Drawing.Color.White;

                    // 줄바꿈
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 3, 1].Text = "";
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 3, 1].ColumnSpan = Fp.ActiveSheet.ColumnCount - 1;
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 3, 1].BackColor = System.Drawing.Color.White;

                    // 조회 조건 추가
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 2, 1].Text = this._SearchInfo;
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 2, 1].ColumnSpan = Fp.ActiveSheet.ColumnCount - 1;
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 2, 1].HorizontalAlignment = CellHorizontalAlignment.Left;
                    Fp.ActiveSheet.ColumnHeader.Cells[Fp.ActiveSheet.ColumnHeader.RowCount - 2, 1].BackColor = System.Drawing.Color.White;
                }
                // *******************************************************************************************************************************

                // 확장자에 따라서 실제 데이터를 저장
                switch (extension)
                {
                    case ".xls":
                        Fp.SaveExcel(sfdFile.FileName, FarPoint.Win.Spread.Model.IncludeHeaders.ColumnHeadersCustomOnly);
                        break;
                    case ".xlsx":
                        Fp.SaveExcel(sfdFile.FileName, FarPoint.Excel.ExcelSaveFlags.UseOOXMLFormat);
                        break;
                    //case ".pdf":
                    //    // PDF의 경우 시트단위
                    //    Pi.ShowPrintDialog = false;
                    //    Pi.Preview = false;
                    //    Pi.PrintToPdf = true;
                    //    Pi.PdfFileName = sfdFile.FileName;

                    //    Pi.Header = "/c/fz\"12\"/fb1" + Form.ActiveForm.Text + "/fb0/fz\"10\"/n/l/n" + "/n" + "/l" + "/r" + "Page : /p / /pc";
                    //    if (_SearchInfo != string.Empty)
                    //        Pi.Header += "/n" + "/l/fz\"10\"/fb0" + this._SearchInfo + "/n";
                    //    Pi.Footer = "인쇄자 : " + CCmApp.UserName + "    " + "인쇄일시 : " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + "/l/r" + _CompanyName;
                    //    Fp.ActiveSheet.PrintInfo = Pi;
                    //    Fp.PrintSheet(Fp.ActiveSheet);
                    //    //Application.DoEvents();
                    //    break;
                    case ".xml":
                        Fp.Save(sfdFile.FileName, true);
                        break;
                    case ".html":
                        Fp.ActiveSheet.SaveHtml(sfdFile.FileName);
                        break;
                    default:
                        return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// Column Index
        /// </summary>
        /// <param name="pColumnName">Column Tag</param>
        /// <returns>Int Column Index</returns>
        public Int32? ColIndex(String pColumnName)
        {
            try
            {
                return this.ActiveSheet.Columns[pColumnName].Index;
            }
            catch (Exception ex)
            {
                MessageBox.Show(pColumnName + "\r\n" + ex.ToString());
                //return -1; - 1이 맞는 것 같음, jsp
                return null;
            }
        }

        /// <summary>
        /// Get Cell Value
        /// </summary>
        /// <param name="pRowIndex">Row Index</param>
        /// <param name="pColumnName">Column Tag</param>
        /// <returns>Object Cell Value</returns>
        public Object GetValue(Int32 pRowIndex, String pColumnName)
        {
            if (ColIndex(pColumnName) == null) return null;
            if (this.ActiveSheet.RowCount <= 0) return null;
            Int32 ColIdx = Convert.ToInt32(ColIndex(pColumnName));
            return this.ActiveSheet.Cells[pRowIndex, ColIdx].Value;
        }

        /// <summary>
        /// Get Cell Value. Convert To Striing
        /// </summary>
        /// <param name="pRowIndex">Row Index</param>
        /// <param name="pColumnName">Column Tag</param>
        /// <returns>String Cell Value</returns>
        public String GetText(Int32 pRowIndex, String pColumnName)
        {
            if (ColIndex(pColumnName) == null) return null;
            if (this.ActiveSheet.RowCount <= 0) return null;
            Int32 ColIdx = Convert.ToInt32(ColIndex(pColumnName));
            return this.ActiveSheet.Cells[pRowIndex, ColIdx].Text;
        }

        /// <summary>
        /// Set Cell Value
        /// </summary>
        /// <param name="pRowIndex">Row Index</param>
        /// <param name="pColumnName">Column Tag</param>
        /// <param name="pValue">Object Value</param>
        /// <returns>Bool true/false</returns>
        public Boolean SetVelue(Int32 pRowIndex, String pColumnName, Object pValue)
        {
            if (ColIndex(pColumnName) == null) return false;
            if (this.ActiveSheet.RowCount <= 0) return false;
            Int32 ColIdx = Convert.ToInt32(ColIndex(pColumnName));
            this.ActiveSheet.Cells[pRowIndex, ColIdx].Value = pValue;
            return true;
        }

        // Cell 단위 데이터 선택
        private void tsmiSelectionUnitCell_Click(object sender, EventArgs e)
        {
            this.ActiveSheet.SelectionUnit = FarPoint.Win.Spread.Model.SelectionUnit.Cell;
        }

        // Row 단위 데이터 선택
        private void tsmiSelectionUnitRow_Click(object sender, EventArgs e)
        {
            this.ActiveSheet.SelectionUnit = FarPoint.Win.Spread.Model.SelectionUnit.Row;
        }

        // 초기 오픈 시 Init
        private void ctmsMenu_Opening(object sender, CancelEventArgs e)
        {
            // Row/Cell 단위 선택 설정값을 표시
            if (this.ActiveSheet.SelectionUnit == FarPoint.Win.Spread.Model.SelectionUnit.Row)
            {
                tsmiSelectionUnitRow.Checked = true;
                tsmiSelectionUnitCell.Checked = false;
            }
            else
            {
                tsmiSelectionUnitRow.Checked = false;
                tsmiSelectionUnitCell.Checked = true;
            }

        }

        private bool ExtendLastColProp = false; // 마지막 컬럼 길이 자동 조절 여부
        private float OldLastColWidth = 0.0f;   // 마지막 컬럼 원래 길이
        private int LastColIdx = 0;             // 마지막 컬럼 인덱스

        // 마지막 컬럼 길이 자동 조절 여부
        [Browsable(false)]
        [DefaultValue(false)]
        public Boolean ExtendLastCol
        {
            get
            {
                return this.ExtendLastColProp;
            }
            set
            {
                this.ExtendLastColProp = value;
                if (this.ExtendLastColProp == true)
                {
                    if (this.ActiveSheet == null) return;
                    for (int i = this.ActiveSheet.ColumnCount - 1; i >= 0; i--)
                    {
                        if (this.ActiveSheet.Columns[i].Visible == false) continue;

                        this.OldLastColWidth = this.ActiveSheet.Columns[i].Width;
                        this.LastColIdx = i;
                        break;

                    }
                    ExtendLastColAction();
                }
                else
                {
                    if (this.ActiveSheet.ColumnCount < 1) return;
                    if (this.LastColIdx == 0 && this.OldLastColWidth == 0) return;
                    this.ActiveSheet.Columns[LastColIdx].Width = this.OldLastColWidth;
                }
            }
        }

        private void ExtendLastColAction()
        {
            if (this.ActiveSheet == null) return;
            if (this.ActiveSheet.ColumnCount < 1) return;

            float MaxWidth = this.Width;
            float MinusWidth = 0.0f;

            // Spread Border Width
            MinusWidth += 2 * SystemInformation.Border3DSize.Width;

            // RowHeader Width
            if (this.ActiveSheet.RowHeader.ColumnCount > 0)
            {
                for (int i = 0; i < this.ActiveSheet.RowHeader.ColumnCount; i++)
                {
                    if (this.ActiveSheet.RowHeader.Columns[i].Visible == true)
                    {
                        MinusWidth += this.ActiveSheet.RowHeader.Columns[i].Width;
                    }
                }
            }

            // Columns Width
            for (int i = 0; i < this.ActiveSheet.ColumnCount; i++)
            {
                if (this.ActiveSheet.Columns[i].Visible == true)
                {
                    MinusWidth += this.ActiveSheet.Columns[i].Width;
                }
            }

            // VerticalScroll Bar Width 빼기
            if (!this.VerticalScrollBar.Size.IsEmpty)
            {
                MinusWidth += SystemInformation.VerticalScrollBarWidth;
            }

            if (MaxWidth > (MinusWidth - this.ActiveSheet.Columns[LastColIdx].Width) + this.OldLastColWidth)
            {
                this.HorizontalScrollBar.Size = System.Drawing.Size.Empty;
                // Set last column to the remaining width of spreadsheet 
                this.ActiveSheet.Columns[LastColIdx].Width = MaxWidth - (MinusWidth - this.ActiveSheet.Columns[LastColIdx].Width);
            }
            else
            {
                this.ActiveSheet.Columns[LastColIdx].Width = this.OldLastColWidth;
            }

        }


    }

}
