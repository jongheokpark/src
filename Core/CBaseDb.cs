﻿using System;
using System.Data.Common;
using System.Data;
using System.Windows.Forms;

namespace Core
{
    /// <summary>
    ///  DB
    /// </summary>
    public class CBaseDb
    {
        /// <summary>
        /// 외부에서 할당해주는 Connection 개체 참조(Connection이 하나 일 경우)
        /// </summary>
        internal DbConnection conMain;

        // DB Factory (DB DbProviderFactories 클래스)
        private DbProviderFactory Factory;

        // 자체 생성되는 DB 객체들
        private DbTransaction trnMain;
        private DbCommand comMain;
        private DbDataAdapter daMain;
        public DataTable dtMain = new DataTable("Default");

        // 바인딩 객체에 사용할지 여부
        // 바인딩 객체일 경우 Reset을 하면 안됨.(계속 연결된 상태, DataSource에 따라 작동)
        private bool bBindingType;

        // DB 에러
        protected const int DB_ERR = -1;
        // DB 에러중 DB Lock
        protected const int DB_LOCK = -2;
        // DB 에러중 중복 데이타
        protected const int DB_DUP = -3;
        // DB 에러중 자식 레코드 존재
        protected const int DB_EXIST_CHILD = -4;

        // DB Error Message
        private string strErrMsg = "";
        internal string ErrMsg
        {
            get { return strErrMsg; }
            set { strErrMsg = value; }
        }

        // DB Error 종류
        private int nErrKind = 0;
        internal int ErrKind
        {
            get { return nErrKind; }
        }


        /// <summary>
        /// 자체 Connection 객체 사용
        /// 외부에서 New 생성 후 Connection 객체를 Open하고 Init을 호출한다.
        /// 종료시 comMain.Close를 반드시 호출
        /// 외부에서 Connection 객체 정의 (pc cliient 처럼 , 하나를 쓸 경우)
        /// </summary>
        public CBaseDb(ref DbConnection conObj)
        {
            bBindingType = false;
            conMain = conObj;

            //DB init
            Init();
        }

        public CBaseDb(ref DbConnection conObj, bool bBind)
        {
            bBindingType = bBind;
            conMain = conObj;

            //DB init
            Init();
        }


        /// <summary>
        /// DB init
        /// </summary>
        internal void Init()
        {
            Factory = DbProviderFactories.GetFactory(conMain.GetType().Namespace);
            comMain = Factory.CreateCommand();
            daMain = Factory.CreateDataAdapter();

            comMain.Connection = conMain;
            comMain.CommandType = CommandType.Text;
            daMain.SelectCommand = comMain;
        }

        /// <summary>
        /// DB Error Message
        /// 프로젝트 별로 메세지를 표시하는 방법을 패생 클래스에서 오버라이드 해서 사용한다.
        /// </summary>
        internal virtual void ShowErrMsg(bool bMsgBox)
        {
            if (bMsgBox)
            {
                MessageBox.Show(ErrMsg, "DB Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// 쿼리 실행 For Select..  
        /// Parameter: bMsgBox (메세지 박스 표시 여부)
        ///            bRerutnErr (에러 발생시, 에러를 Return할 지 여부)
        /// Return: 성공 - 쿼리한 레코드 수 (양의 정수)
        ///         실패 - DB_ERR(-1):  일반 DB Err
        ///         실패 - DB_LOCK(-2): DB Lock
        ///         실패 - DB_DUP(-3):  데이타 중복
        /// </summary>
        internal int ExcuteQry(string strQry)
        {
            return ExcuteQry(strQry, true, false);
        }

        internal int ExcuteQry(string strQry, bool bMsgBox)
        {
            return ExcuteQry(strQry, bMsgBox, false);
        }

        internal int ExcuteQry(string strQry, bool bMsgBox, bool bReturnErr)
        {

            strErrMsg = "";
            nErrKind = 0;

            try
            {
                // 바인딩 객체일 경우 연결유지, DATA만 클리어
                if (bBindingType)
                {
                    dtMain.Clear();
                }
                else
                {
                    dtMain.Reset();
                }
                comMain.CommandText = strQry;
                comMain.CommandType = CommandType.Text;
#if DEBUG
                Console.WriteLine(@"----------
" + comMain.CommandText);
#endif
                return daMain.Fill(dtMain);
            }
            catch (DbException DbErr)
            {
                if (bReturnErr)
                {
                    throw DbErr;
                }
                else
                {
                    strErrMsg = DbErr.Message;
                    nErrKind = 0;
                    ShowErrMsg(bMsgBox);
                }
                return DB_ERR;
            }
            catch (Exception AppErr)
            {
                if (bReturnErr)
                {
                    throw AppErr;
                }
                else
                {
                    strErrMsg = AppErr.Message;
                    nErrKind = 0;
                }
                ShowErrMsg(bMsgBox);
                return DB_ERR;
            }

        }

        /// <summary>
        ///  쿼리 실행 For Select.., 2개 이상 쿼리를 할 경우 datatable을 별도로 바인딩 한다.  
        ///  Parameter: bRerutnErr (에러 발생시, 에러를 Return할 지 여부)
        ///  Return: 성공 - 쿼리한 레코드 수 (양의 정수)
        ///          실패 - DB_ERR(-1):  일반 DB Err
        ///          실패 - DB_LOCK(-2): DB Lock
        ///          실패 - DB_DUP(-3):  데이타 중복
        /// </summary>
        internal int ExcuteQry(ref DataTable dtOther, string strQry)
        {
            return ExcuteQry(ref dtOther, strQry, true, false);
        }

        internal int ExcuteQry(ref DataTable dtOther, string strQry, bool bMsgBox)
        {
            return ExcuteQry(ref dtOther, strQry, bMsgBox, false);
        }

        internal int ExcuteQry(ref DataTable dtOther, string strQry, bool bMsgBox, bool bReturnErr)
        {
            strErrMsg = "";
            nErrKind = 0;
            try
            {
                // 바인딩 객체일 경우 연결유지, DATA만 클리어
                if (bBindingType)
                {
                    dtOther.Clear();
                }
                else
                {
                    dtOther.Reset();
                }

                comMain.CommandText = strQry;
                comMain.CommandType = CommandType.Text;
#if DEBUG
                Console.WriteLine(@"----------
" + comMain.CommandText);
#endif
                return daMain.Fill(dtOther);
            }
            catch (DbException DbErr)
            {
                if (bReturnErr)
                {
                    throw DbErr;
                }
                else
                {
                    strErrMsg = DbErr.Message;
                    nErrKind = 0;
                    ShowErrMsg(bMsgBox);
                }
                return DB_ERR;
            }
            catch (Exception AppErr)
            {
                if (bReturnErr)
                {
                    throw AppErr;
                }
                else
                {
                    strErrMsg = AppErr.Message;
                    nErrKind = 0;
                }
                ShowErrMsg(bMsgBox);
                return DB_ERR;
            }

        }

        /// <summary>
        /// None Query For insert, update, ...
        /// Parameter: bRerutnErr (에러 발생시, 에러를 Return할 지 여부)
        /// Return: 성공 - 반영된 레코드 수 (양의 정수)
        ///         실패 - DB_ERR(-1):  일반 DB Err
        ///         실패 - DB_LOCK(-2): DB Lock
        ///         실패 - DB_DUP(-3):  데이타 중복
        /// </summary>
        internal int ExcuteNonQry(string strQry)
        {
            return ExcuteNonQry(strQry, true, false);
        }

        internal int ExcuteNonQry(string strQry, bool bMsgBox)
        {
            return ExcuteNonQry(strQry, bMsgBox, false);
        }

        internal int ExcuteNonQry(string strQry, bool bMsgBox, bool bReturnErr)
        {
            strErrMsg = "";
            nErrKind = 0;
            try
            {
                comMain.CommandText = strQry;
                comMain.CommandType = CommandType.Text;
#if DEBUG
                Console.WriteLine(@"----------
" + comMain.CommandText);
#endif
                return comMain.ExecuteNonQuery();
            }
            catch (DbException DbErr)
            {
                if (bReturnErr)
                {
                    throw DbErr;
                }
                else
                {
                    strErrMsg = DbErr.Message;
                    nErrKind = 0;
                    if (DbErr.ErrorCode == -2147217873)
                    {
                        nErrKind = DB_DUP;
                    }
                    ShowErrMsg(bMsgBox);
                }
                return DB_ERR;
            }
            catch (Exception AppErr)
            {
                if (bReturnErr)
                {
                    throw AppErr;
                }
                else
                {
                    strErrMsg = AppErr.Message;
                    nErrKind = 0;
                }
                ShowErrMsg(bMsgBox);
                return DB_ERR;
            }

        }

        /// <summary>
        /// Transction 객체 할당
        /// </summary>
        internal void BeginTrans()
        {

            try
            {
                trnMain = conMain.BeginTransaction(IsolationLevel.ReadCommitted);
                comMain.Transaction = trnMain;
            }
            catch (Exception AppErr)
            {
                throw AppErr;
            }
        }

        /// <summary>
        ///  RollBack()
        /// </summary>
        internal void Rollback()
        {
            try
            {
                trnMain.Rollback();
                ShowErrMsg(true);
            }
            catch
            {

            }
        }


        /// <summary>
        ///트랜잭션 단위일 경우 나중에 메세지 보여주기
        /// </summary>
        internal void Rollback(bool bMsgBox)
        {
            try
            {
                trnMain.Rollback();
                ShowErrMsg(bMsgBox);
            }
            catch
            {

            }
        }

        /// <summary>
        /// Commit()
        /// </summary>
        internal void Commit()
        {
            try
            {
                trnMain.Commit();
            }
            catch
            {

            }
        }

    }
}
