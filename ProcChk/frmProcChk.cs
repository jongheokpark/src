﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;
using System.Configuration;
using System.Diagnostics;
using System.Threading;

namespace ProcChk
{
    public partial class frmProcChk : Form
    {
        #region 전역변수
        private string wrkCode = string.Empty;
        private string sFactory = ConfigurationSettings.AppSettings["CompId"];
        private string SeqKey = string.Empty;
        CommonCode commonFunc = new CommonCode();
        DataTable dt = new DataTable();
        QueryResult qr = new QueryResult();
        string preTempCode = string.Empty;
        #endregion

        public frmProcChk()
        {
            InitializeComponent();
        }

        public void frmProcChk_Load(object sender, EventArgs e)
        {
            SetControl();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            string TempStep = commonFunc.AddWrkStep(wrkCode).ToString();
            string TempCode = string.Empty;
            string nextStep = string.Empty;
            string chkMHB = string.Empty;

            qr.GetDataForWrkStep(ref dt, TempStep, lblItemCode.Text, sFactory);
            if (dt != null && dt.Rows.Count > 0)
            {
                //수상, 유상 정지 신호
                //CHK_O_PRT_T의 경우는 예외처리
                if ("CHK_W_PRT".Equals(preTempCode))
                {
                    //수상 파트 확인 시 수상운전 정지
                    Program.cmx.SetTagVal("WATER.AGITATOR_OFF", "1");
                    Program.cmx.SetTagVal("WATER.AGITATOR_ON", "0");

                    //온도, 시간, RPM 클리어
                    //Program.cmx.SetTagVal("WATER.HOT_TEMP_SET", "0");
                    //Program.cmx.SetTagVal("WATER.RPM_SET", "0");
                    //Program.cmx.SetTagVal("WATER.RUN_TIME_MIN_SET", "0");

                    commonFunc.Delay(300);
                }
                else if ("CHK_O_PRT".Equals(preTempCode))
                {
                    Program.cmx.SetTagVal("OIL.AGITATOR_OFF", "1");
                    Program.cmx.SetTagVal("OIL.AGITATOR_ON", "0");

                    //온도, 시간, RPM 클리어
                    //Program.cmx.SetTagVal("OIL.HOT_TEMP_SET", "0");
                    //Program.cmx.SetTagVal("OIL.RPM_SET", "0");
                    //Program.cmx.SetTagVal("OIL.RUN_TIME_MIN_SET", "0");

                    commonFunc.Delay(300);
                }
                //유상 이송 후 역방향 정지 
                else if ("CHK_O_PRT_M_T".Equals(preTempCode))
                {
                    //호모교반(역방향) 종료 메시지 전송 필요
                    Program.cmx.SetTagVal("MAIN.RECIPE_START", "0");

                    commonFunc.Delay(300);
                }

                TempCode = dt.Rows[0]["OPRCOD"].ToString();
                if (TempCode.StartsWith("PT_"))
                {
                    Program.cmx.SetTagVal("CMD.STEP", TempStep);
                    this.Hide();
                    Process.Start(Application.StartupPath + "/MatInput.exe");
                    this.Close();
                    return;
                }
                else if (TempCode.StartsWith("CHK"))
                {
                    Program.cmx.SetTagVal("CMD.STEP", TempStep);
                    this.Hide();
                    Process.Start(Application.StartupPath + "/ProcChk.exe");
                    this.Close();
                    return;
                }


                //메인 장비 세팅 값 때문에 다음 Step전 변수에 입력
                commonFunc.OpTime = double.Parse(dt.Rows[0]["OPTIME"].ToString()).ToString();
                commonFunc.OpTemp = double.Parse(dt.Rows[0]["OPTEMP"].ToString()).ToString();
                commonFunc.OpRpmH = double.Parse(dt.Rows[0]["OPRPMH"].ToString()).ToString();
                commonFunc.OpRpmH_R = double.Parse(dt.Rows[0]["OPRPMH_R"].ToString()).ToString();
                commonFunc.OpRpmP = double.Parse(dt.Rows[0]["OPRPMP"].ToString()).ToString();
                commonFunc.OpRpmS = double.Parse(dt.Rows[0]["OPRPMS"].ToString()).ToString();
                commonFunc.wTemp = double.Parse(dt.Rows[0]["WTEMP"].ToString()).ToString();
                commonFunc.wRpmD = double.Parse(dt.Rows[0]["WRPMD"].ToString()).ToString();
                commonFunc.oTemp = double.Parse(dt.Rows[0]["OTEMP"].ToString()).ToString();
                commonFunc.oRpmP = double.Parse(dt.Rows[0]["ORPMP"].ToString()).ToString();

                if (TempCode.StartsWith("M") && !"MHB".Equals(TempCode))
                {
                    nextStep = commonFunc.AddWrkStep(TempStep).ToString();
                    qr.GetDataForWrkStep(ref dt, nextStep, lblItemCode.Text, sFactory);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string nextStepCode = dt.Rows[0]["OPRCOD"].ToString();
                        TempCode += "," + nextStepCode;

                        while (nextStepCode.StartsWith("M"))
                        {
                            commonFunc.OpTime += "," + double.Parse(dt.Rows[0]["OPTIME"].ToString()).ToString();
                            commonFunc.OpTemp += "," + double.Parse(dt.Rows[0]["OPTEMP"].ToString()).ToString();
                            commonFunc.OpRpmH += "," + double.Parse(dt.Rows[0]["OPRPMH"].ToString()).ToString();
                            commonFunc.OpRpmH_R += "," + double.Parse(dt.Rows[0]["OPRPMH_R"].ToString()).ToString();
                            commonFunc.OpRpmP += "," + double.Parse(dt.Rows[0]["OPRPMP"].ToString()).ToString();
                            commonFunc.OpRpmS += "," + double.Parse(dt.Rows[0]["OPRPMS"].ToString()).ToString();
                            commonFunc.wTemp += "," + double.Parse(dt.Rows[0]["WTEMP"].ToString()).ToString();
                            commonFunc.wRpmD += "," + double.Parse(dt.Rows[0]["WRPMD"].ToString()).ToString();
                            commonFunc.oTemp += "," + double.Parse(dt.Rows[0]["OTEMP"].ToString()).ToString();
                            commonFunc.oRpmP += "," + double.Parse(dt.Rows[0]["ORPMP"].ToString()).ToString();

                            nextStep = commonFunc.AddWrkStep(nextStep).ToString();
                            qr.GetDataForWrkStep(ref dt, nextStep, lblItemCode.Text, sFactory);

                            if (dt != null && dt.Rows.Count > 0)
                            {
                                if ("MHB".Equals(nextStepCode))
                                {
                                    chkMHB = dt.Rows[0]["OPRCOD"].ToString();
                                    break;
                                }
                                else
                                {
                                    nextStepCode = dt.Rows[0]["OPRCOD"].ToString();
                                    TempCode += "," + nextStepCode;
                                }
                            }
                            else
                            {
                                //메인장비의 연속동작 후 종료하는 케이스
                                TempCode += ",CHK";
                                Program.cmx.SetTagVal("REPORT.종료", 1);
                                nextStepCode = string.Empty;
                            }
                        }
                    }
                    else
                    {
                        //단일작업으로 종료되는 케이스
                        TempCode += ",CHK";
                        Program.cmx.SetTagVal("REPORT.종료", 1);
                    }
                }

                //CMD 세팅
                Program.cmx.SetTagVal("CMD.OPTIME", commonFunc.OpTime);
                Program.cmx.SetTagVal("CMD.OPTEMP", commonFunc.OpTemp);
                Program.cmx.SetTagVal("CMD.OPRPMH", commonFunc.OpRpmH);
                Program.cmx.SetTagVal("CMD.OPRPMH_R", commonFunc.OpRpmH_R);
                Program.cmx.SetTagVal("CMD.OPRPMP", commonFunc.OpRpmP);
                Program.cmx.SetTagVal("CMD.OPRPMS", commonFunc.OpRpmS);
                Program.cmx.SetTagVal("CMD.OPRPMV", commonFunc.OpVacu);
                Program.cmx.SetTagVal("CMD.WTEMP", commonFunc.wTemp);
                Program.cmx.SetTagVal("CMD.WRPMD", commonFunc.wRpmD);
                Program.cmx.SetTagVal("CMD.OTEMP", commonFunc.oTemp);
                Program.cmx.SetTagVal("CMD.ORPMP", commonFunc.oRpmP);

                //Thread.Sleep(200);
                //세팅값이 설정되어야 메인머신 운전이 가능하다고 해서 세팅 후에 Step, 현재 코드 세팅
                Program.cmx.SetTagVal("CMD.STEP", TempStep);
                Program.cmx.SetTagVal("CMD.OPRCOD", TempCode);

                //다음 Step OPRCOD
                //작업 후에 팝업 시 Code에 ","가 들어가도록 되어 있으므로 oprcod2에는 데이터 입력하지 않음
                if (!TempCode.Contains(","))
                {
                    TempStep = commonFunc.AddWrkStep(TempStep).ToString();
                    qr.GetDataForWrkStep(ref dt, TempStep, lblItemCode.Text, sFactory);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        Program.cmx.SetTagVal("CMD.OPRCOD2", dt.Rows[0]["OPRCOD"].ToString());
                    }
                }
                else if (TempCode.Contains(",MHB"))
                {
                    string gbnCode = string.Empty;
                    int count = TempCode.Count(o => o == ',');
                    for(int i =0; i< count; i++)
                    {
                        gbnCode += ",";
                    }
                    Program.cmx.SetTagVal("CMD.OPRCOD2", gbnCode + chkMHB);
                }
            }
            this.Close();
        }

        private void SetControl()
        {
            //상단부분 데이터 세팅
            // CIMON에서 GetTagVal해서 읽어온 데이터로 제조지시 정보 표시
            wrkCode = Program.cmx.GetTagVal("CMD.STEP") == null ? "1" : Program.cmx.GetTagVal("CMD.STEP").ToString();
            lblOrderNo.Text = Program.cmx.GetTagVal("REPORT.생산1.지시번호") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.지시번호").ToString();
            lblItemCode.Text = Program.cmx.GetTagVal("REPORT.생산1.제조품목") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조품목").ToString();
            string closeCode = Program.cmx.GetTagVal("REPORT.종료") == null ? "0" : Program.cmx.GetTagVal("REPORT.종료").ToString();

            string sStatus = Program.cmx.GetTagVal("CMD.처리") == null ? "" : Program.cmx.GetTagVal("CMD.처리").ToString();
            //CHK 항목 가져오기
            qr.GetDataForWrkStep(ref dt, wrkCode, lblItemCode.Text, sFactory);
            if (dt != null && dt.Rows.Count > 0)
            {
                //이전 Code 저장 변수
                preTempCode = dt.Rows[0]["OPRCOD"].ToString();

                //현재 CMD가 OPRCOD와 동일하고 동작이 완료된 상태의 경우 다음 Step의 데이터 가져오기
                //해당 코드 현재 미사용될 수도?
                if (sStatus.Contains(dt.Rows[0]["OPRCOD"].ToString()) && sStatus.Contains("동작 완료"))
                {
                    wrkCode = commonFunc.AddWrkStep(wrkCode).ToString();
                    qr.GetDataForWrkStep(ref dt, wrkCode, lblItemCode.Text, sFactory);
                }
                //////

                string[] vMsg = dt.Rows[0]["OPSINS"].ToString().Split('\n');
                for (int i = 0; i < vMsg.Count(); i++)
                {
                    if (!string.IsNullOrEmpty(vMsg[i]))
                    {
                        chkMsgBox.Items.Add(vMsg[i]);
                    }
                }

                //다음 스텝 가져오기
                string sNextStep = commonFunc.AddWrkStep(wrkCode).ToString();
                qr.GetDataForWrkStep(ref dt, sNextStep, lblItemCode.Text, sFactory);
                if (dt != null && dt.Rows.Count > 0)
                {
                    string oprCod = dt.Rows[0]["OPRCOD"].ToString();

                    txtNextStep.Text = oprCod + Environment.NewLine;
                    qr.GetNextStepName(ref dt, sFactory, oprCod);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        txtNextStep.Text += dt.Rows[0]["OPRDSC"].ToString();
                    }
                }
            }
            else
            {
                if ("1".Equals(closeCode))
                {
                    chkMsgBox.Items.Add("모든 제조 작업이 종료되었습니다.");
                }
            }
        }

        private void chkMsgBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (chkMsgBox.CheckedItems.Count == chkMsgBox.Items.Count)
            {
                btnOK.Enabled = true;
            }
            else
            {
                btnOK.Enabled = false;
            }
        }
    }
}
