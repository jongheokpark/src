﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.Common;

namespace ProdEnd
{
    class CMyFunc
    {
        internal static int ExcuteQry(ref string RetMsg, ref System.Data.DataTable RetTable, string pSql)
        {
            DbConnection pConObj;
            DbTransaction DbTran;
            DbProviderFactory Factory; // DB Factory (DB DbProviderFactories 클래스)
            DbCommand comMain;
            DbDataAdapter daMain;
            string strErrMsg = string.Empty;
            int resultCnt = 0;

            Factory = DbProviderFactories.GetFactory("System.Data.SqlClient");

            pConObj = Factory.CreateConnection();
            pConObj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnectString"];
            pConObj.Open();
            DbTran = pConObj.BeginTransaction();

            comMain = Factory.CreateCommand();
            daMain = Factory.CreateDataAdapter();

            comMain.Connection = pConObj;
            comMain.CommandType = CommandType.Text;
            comMain.Transaction = DbTran;
            daMain.SelectCommand = comMain;

            try
            {
                comMain.CommandText = pSql;
                comMain.CommandType = CommandType.Text;
                resultCnt = daMain.Fill(RetTable);
            }
            catch (DbException DbErr)
            {
                DbTran.Rollback();
                MessageBox.Show(DbErr.Message);
                return resultCnt;
            }
            catch (Exception AppErr)
            {
                DbTran.Rollback();
                MessageBox.Show(AppErr.Message);
                return resultCnt;
            }

            DbTran.Commit();
            return resultCnt;
        }

        internal static int ExcuteNonQry(ref string RetMsg, string pSql)
        {

            DbConnection pConObj;
            DbTransaction DbTran;
            DbProviderFactory Factory; // DB Factory (DB DbProviderFactories 클래스)
            DbCommand comMain;
            DbDataAdapter daMain;
            string strErrMsg = string.Empty;
            int resultCnt = 0;

            Factory = DbProviderFactories.GetFactory("System.Data.SqlClient");

            pConObj = Factory.CreateConnection();
            pConObj.ConnectionString = System.Configuration.ConfigurationSettings.AppSettings["ConnectString"];
            pConObj.Open();
            DbTran = pConObj.BeginTransaction();

            comMain = Factory.CreateCommand();
            daMain = Factory.CreateDataAdapter();

            comMain.Connection = pConObj;
            comMain.CommandType = CommandType.Text;
            comMain.Transaction = DbTran;
            daMain.SelectCommand = comMain;

            try
            {
                comMain.CommandText = pSql;
                comMain.CommandType = CommandType.Text;
                resultCnt = comMain.ExecuteNonQuery();
            }
            catch (DbException DbErr)
            {
                DbTran.Rollback();
                MessageBox.Show(DbErr.Message);
                return resultCnt;
            }
            catch (Exception AppErr)
            {
                DbTran.Rollback();
                MessageBox.Show(AppErr.Message);
                return resultCnt;
            }

            DbTran.Commit();
            return resultCnt;
        }
    }
}
