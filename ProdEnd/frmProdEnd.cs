﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core;
using System.Configuration;

namespace ProdEnd
{
    public partial class frmProdEnd : Form
    {
        internal const int DB_ERR = -1;           /// DB 에러 
        internal const string CRLF = "\r\n";      /// 줄바꿈

        private string Sql = string.Empty;
        private int SelCnt = 0;
        private string RetMsg = string.Empty;
        public string wrkCode = string.Empty;
        CommonCode commonFunc = new CommonCode();
        QueryResult qr = new QueryResult();
        private string sFactory = ConfigurationSettings.AppSettings["CompId"];

        public frmProdEnd()
        {
            InitializeComponent();
        }

        private void clbPrecautions_SelectedIndexChanged(object sender, EventArgs e)
        {
            //주의사항 항목이 모두 Checked 되야 ‘생산종료‘ 버튼이 활성화
            if (clbPrecautions.CheckedItems.Count == clbPrecautions.Items.Count)
            {
                btnProdEnd.Enabled = true;
            }
            else
            {
                btnProdEnd.Enabled = false;
            }
        }

        private void btnProdEnd_Click(object sender, EventArgs e)
        {
            string TempStep = commonFunc.AddWrkStep(wrkCode).ToString();
            string TempCode = string.Empty;

            DataTable dt = new DataTable();
            qr.GetDataForWrkStep(ref dt, TempStep, lblItemCode.Text, sFactory);
            if (dt != null && dt.Rows.Count > 0)
            {
                MessageBox.Show("다음 진행 작업이 남아있습니다.");
                return;
            }

            // 생산종료 일시 DB 적재
            string endTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            if(!qr.ProdEnd(endTime, lblOrderNo.Text))
            {
                MessageBox.Show("생산종료 일시 DB 적재 실패");
                return;
            }

            // Tag
            Program.cmx.SetTagVal("CMD.OPRCOD", "Z");

            this.Close();
        }

        public void frmProdEnd_Load(object sender, EventArgs e)
        {
            SetControl();
        }

        private void SetControl()
        {
            //상단부분 데이터 세팅
            wrkCode = Program.cmx.GetTagVal("CMD.STEP") == null ? "1" : Program.cmx.GetTagVal("CMD.STEP").ToString();
            lblOrderNo.Text = Program.cmx.GetTagVal("REPORT.생산1.지시번호") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.지시번호").ToString();
            lblVersion.Text = Program.cmx.GetTagVal("REPORT.VER") == null ? "" : Program.cmx.GetTagVal("REPORT.VER").ToString();
            lblProdNo.Text = Program.cmx.GetTagVal("REPORT.생산1.제조번호") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조번호").ToString();
            lblTankID.Text = "";
            lblItemCode.Text = Program.cmx.GetTagVal("REPORT.생산1.제조품목") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조품목").ToString();
            lblItemName.Text = Program.cmx.GetTagVal("REPORT.생산1.제조품명") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조품명").ToString();
            lblProdOrderQty.Text = Program.cmx.GetTagVal("REPORT.생산1.제조량") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.제조량").ToString();
            lblProdStartDate.Text = Program.cmx.GetTagVal("REPORT.생산1.시작일시2") == null ? "" : Program.cmx.GetTagVal("REPORT.생산1.시작일시2").ToString();
            lblWorker.Text = Program.cmx.GetTagVal("작업자.작업자_N1") == null ? "" : Program.cmx.GetTagVal("작업자.작업자_N1").ToString();

            // 생산 종료 시 작업자 확인사항은 고정
            clbPrecautions.Items.Clear();
            clbPrecautions.Items.Add("잔량 원료 여부 확인");
            clbPrecautions.Items.Add("즉점도");
            clbPrecautions.Items.Add("즉경도");
            clbPrecautions.Items.Add("pH");
            clbPrecautions.Items.Add("향취");
            clbPrecautions.Items.Add("성상");
        }
    }
}
